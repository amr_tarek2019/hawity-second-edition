<?php
return[
    'member'=>'عضو',
    'memberstable'=>'جدول الأعضاء',
    'membersdata'=>'بيانات الأعضاء',
    'addnew'=>'اضف جديد',
    'name'=>'اسم',
    'email'=>'البريد الإلكتروني',
    'createdat'=>'أنشئت في',
    'updatedat'=>'تم التحديث في',
    'actions'=>'أفعال',
    'name'=>'اسم',
    'email'=>'البريد الإلكتروني',
    'userstatusselect'=>'حدد حالة المستخدم',
    'statusselect'=>'حدد الوضع',
    'password'=>'كلمه السر',
];