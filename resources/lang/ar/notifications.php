<?php
return[
    'notificationsdata'=>'بيانات الاشعارات',
    'notificationsdatatable'=>'جدول الاشعارات',
    'title'=>'عنوان',
    'notification'=>'اشعارات',
    'icon'=>'أيقونة',
    'createdat'=>'أنشئت في',
    'actions'=>'عمل',
    'submit'=>'تأكيد',
    'addnew'=>'اضف جديد',
    'selectuser'=>'اختار المستخدمين'
];
