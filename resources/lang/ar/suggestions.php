<?php
return[
    'suggestions'=>'اقتراحات',
    'suggestionsdatatable'=>'جدول بيانات الاقتراحات',
    'suggestionsdata'=>'جدول الاقتراحات',
    'username'=>'اسم المستخدم',
    'useremail'=>'البريد الالكتروني للمستخدم',
    'title'=>'عنوان',
    'suggestion'=>'اقتراح',
    'createdat'=>'أنشئت في',
    'actions'=>'أفعال',
    'show'=>'عرض',
    'delete'=>'حذف',
    'showmessage'=>'عرض الرسالة',
    'back'=>'رجوع'
];