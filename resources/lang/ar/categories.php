<?php
return[
    'categoriesdatatable'=>'جدول بيانات الفئات',
    'namearabic'=>'الاسم بالعربيه',
    'nameenglish'=>'الاسم بالانجليزيه',
    'companiesorders'=>'طلبات الشركات',
    'image'=>'صورة',
    'createdat'=>'أنشئت في',
    'updatedat'=>'تم التحديث في',
    'actions'=>'أفعال',
    'edit'=>'تعديل',
    'delete'=>'حذف',
    'submit'=>'تأكيد',
    'uploadfile'=>'رفع ملف',
    'editcategory'=>'تحرير الفئة',
    'category'=>'الفئة',
    'addcategory'=>'اضف فئة',
    'categoriesdata'=>'بيانات الفئات'
];
