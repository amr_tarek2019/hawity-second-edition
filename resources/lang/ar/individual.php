<?php
return[
    'individualtable'=>'جدول الافراد',
    'name'=>'الاسم',
    'email'=>'البريد الإلكتروني',
    'latitude'=>'خط العرض',
    'longitude'=>'خط الطول',
    'createdat'=>'أنشئت في',
    'updatedat'=>'تم التحديث في',
    'actions'=>'أفعال',
    'show'=>'عرض',
    'delete'=>'حذف',
    'showindividualuser'=>'عرض المستخدم الفردي',
    'phone'=>'هاتف',
    'city'=>'مدينة',
    'back'=>'رجوع',
];
