<?php
return[
    'categoriesdatatable'=>'categories data table',
    'namearabic'=>'arabic name',
    'nameenglish'=>'english name',
    'companiesorders'=>'companies orders',
    'image'=>'image',
    'createdat'=>'created at',
    'updatedat'=>'updated at',
    'actions'=>'actions',
    'edit'=>'edit',
    'delete'=>'delete',
    'submit'=>'submit',
    'uploadfile'=>'upload file',
    'editcategory'=>'edit category',
    'category'=>'category',
    'addcategory'=>'add category',
    'categoriesdata'=>'categories data'

];
