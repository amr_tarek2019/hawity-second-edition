<?php
return[
    'containerdatatable'=>'container data table',
    'containersdata'=>'containers data',
    'addnew'=>'add new',
    'arabicname'=>'arabic name',
    'englishname'=>'english name',
    'company'=>'company',
    'price'=>'price',
    'distance'=>'distance',
    'image'=>'image',
    'createdat'=>'created at',
    'updatedat'=>'updated at',
    'actions'=>'actions',
    'companyselect'=>'company select',
    'uploadfile'=>'upload file',
    'textenglish'=>'detalis english',
    'textarabic'=>'detalis arabic',
    'submit'=>'submit',
    'edit'=>'edit',
    'delete'=>'delete',
    'editcontainer'=>'edit container'
];
