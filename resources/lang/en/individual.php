<?php
return[
    'individualtable'=>'individual table',
    'name'=>'name',
    'email'=>'email',
    'latitude'=>'latitude',
    'longitude'=>'longitude',
    'createdat'=>'created at',
    'updatedat'=>'updated at',
    'actions'=>'actions',
    'show'=>'show',
    'delete'=>'delete',
    'showindividualuser'=>'show individual user',
    'phone'=>'phone',
    'city'=>'city',
    'back'=>'back',
];
