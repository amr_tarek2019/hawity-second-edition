<?php
return[
    'submitted'=>'this account submitted before',
    'logged'=>'user loged in successfully',
    'check'=>'check your data',
    'code'=>'code sent successfully',
    'verify'=>'your account verified',
    'codenotfound'=>'code activation not found',
    'passwordchangedsuccess'=>'new password changed successfully',
    'somethingwentwrong'=>'something went wrong',
    'getdataprofile'=>'get data of user successfully',
    'profiledatachanged'=>'Profile data changed successfully',
    'suggestion'=>'Suggestion sent successfully',
    'register'=>'user registered successfully',
    'phone'=>'phone not found',
    'deleted'=>'deletion done successfully',
    'reservation'=>'Reservation request sent successfully',
    'empty'=>'reported that recycle is fulled',
    'Order successfully deleted'=>'Order successfully deleted'


];