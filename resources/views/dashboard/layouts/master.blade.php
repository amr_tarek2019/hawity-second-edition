<!DOCTYPE html>
@if(request()->segment(1)=='en')
    <html lang="en" dir="ltr">
    @else
        <html lang="ar" dir="rtl">
        @endif
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="endless admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, endless admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="{{ asset('assets/coming/assets/img/favicons/Group 1188.png') }}" type="image/x-icon">
    <link rel="shortcut icon" href="{{ asset('assets/coming/assets/img/favicons/Group 1188.png') }}" type="image/x-icon">
    <title>{{trans('dashboard.hawity')}}</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/css/fontawesome.css') }}">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/css/icofont.css') }}">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/css/themify.css') }}">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/css/flag-icon.css') }}">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/css/feather-icon.css') }}">
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/css/chartist.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/css/prism.css') }}">
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/css/bootstrap.css') }}">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/css/style.css') }}">
    <link id="color" rel="stylesheet" href="{{ asset('assets/dashboard/css/light-1.css') }}" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/css/responsive.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/css/datatables.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/css/select2.css') }}">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

</head>
        @if(request()->segment(1)=='ar')
            <body main-theme-layout="rtl">

            @else
                <body>

                @endif
<!-- Loader starts-->
<div class="loader-wrapper">
    <div class="loader bg-white">
        <div class="whirly-loader"> </div>
    </div>
</div>
<!-- Loader ends-->
<!-- page-wrapper Start-->
                @if(request()->segment(1)=='en')
                    <div class="page-wrapper">
                        @else
                            <div class="page-wrapper" dir="rtl">
                            @endif
    <!-- Page Header Start-->
@include('dashboard.partials.navbar')
    <!-- Page Header Ends                              -->
    <!-- Page Body Start-->
                                @if(request()->segment(1)=='en')
                                    <div class="page-body-wrapper">
                                        @else
                                            <div class="page-body-wrapper" dir="rtl">
                                            @endif
        <!-- Page Sidebar Start-->
    @include('dashboard.partials.sidebar')
        <!-- Page Sidebar Ends-->
        <!-- Right sidebar Start-->
        <!-- Right sidebar Ends-->
       @yield('content')
        <!-- footer start-->
        @include('dashboard.partials.footer')

    </div>
</div>

<!-- latest jquery-->
<script src="{{ asset('assets/dashboard/js/jquery-3.2.1.min.js') }}"></script>
<!-- Bootstrap js-->
<script src="{{ asset('assets/dashboard/js/bootstrap/popper.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/bootstrap/bootstrap.js') }}"></script>
<!-- feather icon js-->
<script src="{{ asset('assets/dashboard/js/icons/feather-icon/feather.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/icons/feather-icon/feather-icon.js') }}"></script>
<!-- Sidebar jquery-->
<script src="{{ asset('assets/dashboard/js/sidebar-menu.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/config.js') }}"></script>
<!-- Plugins JS start-->
<script src="{{ asset('assets/dashboard/js/chart/chartist/chartist.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/chart/knob/knob.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/chart/knob/knob-chart.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/prism/prism.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/clipboard/clipboard.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/counter/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/counter/jquery.counterup.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/counter/counter-custom.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/custom-card/custom-card.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/dashboard/default.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/typeahead/handlebars.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/typeahead/typeahead.bundle.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/typeahead/typeahead.custom.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/chat-menu.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/height-equal.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/tooltip-init.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/typeahead-search/handlebars.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/typeahead-search/typeahead-custom.js') }}"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="{{ asset('assets/dashboard/js/script.js') }}"></script>
<!-- Plugin used-->
<script src="{{ asset('assets/dashboard/js/datatable/datatables/jquery.dataTables.min.js') }}"></script>

<script src="{{ asset('assets/dashboard/js/datatable/datatables/datatable.custom.js') }}"></script>
<!-- Plugins JS start-->
<script src="{{ asset('assets/dashboard/js/chart/morris-chart/raphael.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/chart/morris-chart/morris.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/chart/morris-chart/prettify.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/chart/chartjs/chart.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/chart/knob/knob.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/chart/knob/knob-chart.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/prism/prism.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/clipboard/clipboard.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/counter/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/counter/jquery.counterup.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/counter/counter-custom.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/custom-card/custom-card.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/owlcarousel/owl.carousel.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/dashboard/dashboard-ecommerce/chart.custom.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/dashboard/dashboard-ecommerce/morris-script.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/dashboard/dashboard-ecommerce/owl-carousel.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/typeahead/handlebars.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/typeahead/typeahead.bundle.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/typeahead/typeahead.custom.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/chat-menu.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/height-equal.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/tooltip-init.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/typeahead-search/handlebars.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/typeahead-search/typeahead-custom.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/select2/select2-custom.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/typeahead/handlebars.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/typeahead/typeahead.bundle.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/typeahead/typeahead.custom.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/chat-menu.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/tooltip-init.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/typeahead-search/handlebars.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/typeahead-search/typeahead-custom.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/script.js') }}"></script>
<!-- Plugins JS Ends-->
<script src="{{ asset('assets/dashboard/js/datatable/datatable-extension/pdfmake.min.js') }}"></script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5bd443a85b400d47"></script>

<script>
    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>


@yield('myjsfile')
</body>
</html>