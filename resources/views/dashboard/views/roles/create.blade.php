@extends('dashboard.layouts.master')

@section('content')


    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('permissions.Role Information')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item active">{{trans('permissions.roles')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('permissions.ADD ROLE')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <form class="form-horizontal" action="{{ route('roles.store') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="col">

                                        <div class="form-group">
                                            <label for="name">{{trans('admin.name')}}</label>
                                            <input type="text" placeholder="{{trans('admin.name')}}" id="name" name="name" class="form-control" required>
                                        </div>


                                        <div class="form-group">


                                            <div class="checkbox checkbox-dark m-squar">
                                                <input id="inline-sqr-1" value="1" name="permissions[]" type="checkbox">
                                                <label class="mt-0" for="inline-sqr-1">{{trans('dashboard.users')}}</label>
                                            </div>

                                            <div class="checkbox checkbox-dark m-squar">
                                                <input id="inline-sqr-2" value="2" name="permissions[]" type="checkbox">
                                                <label class="mt-0" for="inline-sqr-2">{{trans('dashboard.admin')}}</label>
                                            </div>

                                            <div class="checkbox checkbox-dark m-squar">
                                                <input id="inline-sqr-3" value="3" name="permissions[]" type="checkbox">
                                                <label class="mt-0" for="inline-sqr-3">{{trans('dashboard.company')}}</label>
                                            </div>

                                            <div class="checkbox checkbox-dark m-squar">
                                                <input id="inline-sqr-4" value="4" name="permissions[]" type="checkbox">
                                                <label class="mt-0" for="inline-sqr-4">{{trans('dashboard.individual')}}</label>
                                            </div>

                                            <div class="checkbox checkbox-dark m-squar">
                                                <input id="inline-sqr-5" value="5" name="permissions[]" type="checkbox">
                                                <label class="mt-0" for="inline-sqr-5">{{trans('dashboard.categories')}}</label>
                                            </div>

                                            <div class="checkbox checkbox-dark m-squar">
                                                <input id="inline-sqr-6" value="6" name="permissions[]" type="checkbox">
                                                <label class="mt-0" for="inline-sqr-6">{{trans('dashboard.company')}}</label>
                                            </div>

                                            <div class="checkbox checkbox-dark m-squar">
                                                <input id="inline-sqr-7" value="7" name="permissions[]" type="checkbox">
                                                <label class="mt-0" for="inline-sqr-7">{{trans('dashboard.containers')}}</label>
                                            </div>

                                            <div class="checkbox checkbox-dark m-squar">
                                                <input id="inline-sqr-8" value="8" name="permissions[]" type="checkbox">
                                                <label class="mt-0" for="inline-sqr-8">{{trans('dashboard.booking')}}</label>
                                            </div>

                                            <div class="checkbox checkbox-dark m-squar">
                                                <input id="inline-sqr-9" value="9" name="permissions[]" type="checkbox">
                                                <label class="mt-0" for="inline-sqr-9">{{trans('dashboard.settings')}}</label>
                                            </div>

                                            <div class="checkbox checkbox-dark m-squar">
                                                <input id="inline-sqr-10" value="10" name="permissions[]" type="checkbox">
                                                <label class="mt-0" for="inline-sqr-10">{{trans('dashboard.suggestions')}}</label>
                                            </div>



                                            <div class="checkbox checkbox-dark m-squar">
                                                <input id="inline-sqr-11" value="11" name="permissions[]" type="checkbox">
                                                <label class="mt-0" for="inline-sqr-11">{{trans('dashboard.notifications')}}</label>
                                            </div>




                                        </div>
                                        <button class="btn btn-primary" type="submit" data-original-title="" title="">{{trans('admin.submit')}}</button>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
@endsection
