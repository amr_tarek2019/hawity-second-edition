
@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('permissions.Roles DataTable')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item active">{{trans('permissions.Roles DataTable')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <!-- Zero Configuration  Starts-->
                <div class="col-sm-12">
                    <a href="{{ route('roles.create')}}" class="btn btn-primary">{{trans('member.addnew')}}</a>
                    @include('dashboard.partials.msg')
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('permissions.ROLES DATA')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{trans('user.name')}}</th>
                                        <th>{{trans('reservation.actions')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($roles as $key => $role)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$role->name}}</td>
                                            <td>
                                                <a href="{{ route('roles.edit',encrypt($role->id))}}" class="btn btn-info btn-sm"><i class="material-icons">{{trans('reservation.edit')}}</i></a>

                                                <form id="delete-form-{{ $role->id }}" action="{{ route('roles.destroy',$role->id) }}" style="display: none;" method="POST">
                                                    @csrf
                                                </form>
                                                <button type="button" class="btn btn-danger btn-sm" onclick="if(confirm('{{trans('admin.deletemsg')}}')){
                                                    event.preventDefault();
                                                    document.getElementById('delete-form-{{ $role->id }}').submit();
                                                    }else {
                                                    event.preventDefault();
                                                    }"><i class="material-icons">{{trans('reservation.delete')}}</i></button>
                                            </td>

                                        </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Zero Configuration  Ends-->
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>





@endsection


