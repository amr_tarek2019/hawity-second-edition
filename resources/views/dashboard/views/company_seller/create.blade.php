@extends('dashboard.layouts.master')
@section('content')
<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <div class="page-header-left">
                        <h3>{{trans('companysellers.addnew')}}</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('dashboard')}}" data-original-title="" title=""><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg></a></li>
                            <li class="breadcrumb-item">{{trans('dashboard.dashboard')}}</li>
                            <li class="breadcrumb-item active">{{trans('companysellers.addnew')}}</li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('dashboard.partials.msg')

                <div class="card">
                    <div class="card-header">
                        <h5>{{trans('companysellers.addnew')}}</h5>
                    </div>
                    <form class="form theme-form" method="POST" action="{{ route('company.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">{{trans('companysellers.name')}}</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" type="text" name="name" placeholder="Name">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">{{trans('companysellers.companyarabicname')}}</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" name="company_name_A" placeholder="Company Name Arabic" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">{{trans('companysellers.companyenglishname')}}</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" name="company_name_E" placeholder="Company Name English" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect9">{{trans('companysellers.categoryselect')}}</label>
                                        <select class="form-control digits" name="category">
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}">{{ $category->name_E }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-form-label">{{trans('companysellers.textenglish')}}</label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control" name="text_E" rows="5" cols="5" placeholder="Text English"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-form-label">{{trans('companysellers.textarabic')}}</label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control" name="text_A" rows="5" cols="5" placeholder="Text Arabic"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">{{trans('companysellers.email')}}</label>
                                        <input class="form-control" type="email" name="email" placeholder="name@example.com">
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">{{trans('companysellers.phone')}}</label>
                                        <div class="col-sm-9">
                                            <input class="form-control m-input digits" name="phone" type="tel" placeholder="91-(999)-999-999">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">{{trans('companysellers.address')}}</label>
                                        <div class="col-sm-9">
                                            <input class="form-control m-input digits" name="address" type="text" placeholder="Address">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">{{trans('companysellers.lat')}}</label>
                                        <div class="col-sm-9">
                                            <input class="form-control m-input digits" name="latitude" type="text" placeholder="latitude">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">{{trans('companysellers.lng')}}</label>
                                        <div class="col-sm-9">
                                            <input class="form-control m-input digits" name="longitude" type="text" placeholder="longitude">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">{{trans('companysellers.commercialregister')}}</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" type="number" name="commercial_register" placeholder="Commercial Register">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">{{trans('companysellers.taxrecord')}}</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" type="number" name="tax_record" placeholder="Tax Record">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">{{trans('companysellers.uploadicon')}}</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" type="file" name="icon" data-original-title="" title="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="col-sm-9 offset-sm-3">
                                <button class="btn btn-primary" type="submit">{{trans('companysellers.submit')}}</button>
                                <a href="{{route('company.index')}}" class="btn btn-light">
                                    {{trans('companysellers.cancel')}}
                                </a>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
</div>
@endsection