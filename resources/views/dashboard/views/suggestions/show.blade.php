@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('suggestions.showmessage')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}" data-original-title="" title=""><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg></a></li>
                                <li class="breadcrumb-item">{{trans('dashboard.dashboard')}}</li>
                                <li class="breadcrumb-item active">{{trans('suggestions.showmessage')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">

                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                          <div class="col-md-9 mb-3">
                                            <label for="validationTooltip01">{{trans('suggestions.username')}}</label>
                                            <input class="form-control" id="title" type="text" value="{{$suggestion->user['name']}}" readonly="readonly">
                                        </div>
                                        <div class="col-md-9 mb-3">
                                            <label for="validationTooltip01">{{trans('suggestions.useremail')}}</label>
                                            <input class="form-control" id="title" type="text" value="{{$suggestion->user['email']}}" readonly="readonly">
                                        </div>
                                        <div class="col-md-9 mb-3">
                                            <label for="validationTooltip01">{{trans('suggestions.title')}}</label>
                                            <input class="form-control" id="title" type="text" value="{{$suggestion->title}}" readonly="readonly">
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 col-form-label">{{trans('suggestions.suggestion')}}</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" disabled name="text_E"  rows="5" cols="5" placeholder="Text English"readonly="readonly">{{$suggestion->suggestion}}</textarea>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="col-sm-9 offset-sm-3">
                                    <a href="{{route('suggestion.index')}}" class="btn btn-secondary">
                                        {{trans('suggestions.back')}}
                                    </a>
                                </div>
                            </div>

                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
@endsection