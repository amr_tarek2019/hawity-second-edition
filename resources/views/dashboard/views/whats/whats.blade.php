<!DOCTYPE html>
<html lang="ar" dir="rtl">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <style>
        @import url('https://fonts.googleapis.com.css?family=Open+Sans:400,700');
        *{
            margin: 0;
            padding: 0;
            line-height: 1.5;
            font-family: 'XB Riyaz';
            color: #1b1e21;
            direction: rtl;
        }

    </style>
</head>
<body dir=”rtl”>


<div class="container" style="
direction:rtl ">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="invoice">
                        <div>
                            <div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="media">
                                            <div class="media-left"><img class="media-object img-60" src="{{ asset('assets/coming/assets/img/favicons/Group 1188.png') }}" alt="" data-original-title="" title=""></div>
                                            <div class="media-body m-l-20">
                                                <h4 class="media-heading">فاتورة</h4>
                                            </div>
                                        </div>
                                        <!-- End Info-->
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="text-md-right">
                                            <h3> فاتورة # <span class="digits counter">{{ $reservation->order_number }}</span></h3>
                                            <p>تاريخ الطلب : <span class="digits"> {{ $reservation->created_at }}</span><br>
                                                طريقة الدفع :  <span class="digits">@if ($reservation->status == true) cash @else Online @endif</span></p>
                                        </div>
                                        <!-- End Title-->
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <!-- End InvoiceTop-->
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="media">
                                        <div class="media-left">
                                            <img class="media-object rounded-circle img-60" src="{{ asset($reservation->companySeller->icon) }}" style="height: 100px; width: 100px" alt="" data-original-title="" title="">
                                            <h4 class="media-heading">اسم الشركة : {{ $reservation->companySeller->company_name_A }}</h4>
                                        </div>
                                        <div class="media-body m-l-20">
                                            <h4 class="media-heading">اسم العميل : {{ $reservation->user['name'] }}</h4>
                                            <p> بريد العميل : {{ $reservation->user['email'] }}<br><span class="digits">هاتف العميل : {{ $reservation->user['phone'] }}</span></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="text-md-right" id="project">
                                        <h6>اسم الحاوية</h6>
                                        <p>{{ $reservation->wasteContainer->name_A }}</p>
                                    </div>
                                </div>
                            </div>
                            <!-- End Invoice Mid-->
                            <div style="direction: rtl;max-width: 100px;">
                                <div class="table-responsive invoice-table" id="table" style="direction: rtl;max-width: 100px;">
                                    <table class="table table-bordered table-striped" style="direction: rtl;max-width: 100px;">
                                        <tbody>
                                        <tr>
                                            <td class="item">
                                                <h6 class="p-2 mb-0">وصف الحاوية</h6>
                                            </td>
                                            <td class="Hours">
                                                <h6 class="p-2 mb-0">المسافة</h6>
                                            </td>
                                            <td class="Rate">
                                                <h6 class="p-2 mb-0">عدد الايام</h6>
                                            </td>
                                            <td class="subtotal">
                                                <h6 class="p-2 mb-0">السعر</h6>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>{{ $reservation->wasteContainer->description_A }}</label>

                                            </td>
                                            <td>
                                                <p class="itemtext digits">{{ $reservation->wasteContainer->distance }} yards</p>
                                            </td>
                                            <td>
                                                <p class="itemtext digits">{{ $reservation->days }}</p>
                                            </td>
                                            <td>
                                                <p class="itemtext digits">{{ $reservation->total }}</p>
                                            </td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>

                            </div>
                            <!-- End InvoiceBot-->
                        </div>
 <div class="addthis_inline_share_toolbox_jidg" style="text-align: center;"></div>
                        <!-- End Invoice-->
                        <!-- End Invoice Holder-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5bd443a85b400d47"></script>

</body>
</html>