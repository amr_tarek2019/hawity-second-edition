@extends('dashboard.layouts.master')
@section('content')
   

    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('reservations.editreservation')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}" data-original-title="" title=""><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg></a></li>
                                <li class="breadcrumb-item">{{trans('reservations.reservationsdata')}}</li>
                                <li class="breadcrumb-item active">{{trans('reservations.editreservation')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        
        <div class="container-fluid">
            <form method="POST" action="{{ route('reservation.update',$reservation->id) }}" enctype="multipart/form-data">
             @csrf
                <div class="row">
                    <div class="col-sm-12 col-xl-6">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>{{trans('reservations.userdata')}}</h5>
                                    </div>
                                    <div class="card-body">

                                        <div class="form-group">
                                            <label class="col-form-label pt-0" for="exampleInputEmail1">{{trans('reservations.username')}}</label>
                                            <input class="form-control" id="title" type="text"  value="{{ $reservation->user['name'] }}" readonly="readonly">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">{{trans('reservations.useremail')}}</label>
                                            <input class="form-control" id="title" type="text" value="{{ $reservation->user['email'] }}" readonly="readonly">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">{{trans('reservations.phone')}}</label>
                                            <input class="form-control" id="title" type="text" value="{{ $reservation->user['phone'] }}" readonly="readonly">
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>{{trans('reservations.companydata')}}</h5>
                                    </div>
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect9">{{trans('reservations.companyselect')}}</label>
                                            <select class="form-control digits" name="company_id" id="company_id">
                                                @foreach($companies as $company)
                                                    <option id="{{ $company->id }}" value="{{ $company->id }}">{{ $company->company_name_E }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-xl-6">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>{{trans('reservations.wastecontainerdata')}}</h5>
                                    </div>
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect9"  id="container_model" hidden>Waste Container</label>
                                            <select class="form-control" name="container" id="container_id">
                                       @foreach($containers as $container)
                                                <option {{ $container->id == $reservation->wasteContainer->id ? 'selected' : '' }} value="{{ $container->id }}">{{ $container->name_E }} - {{ $container->distance }} yard</option>
                                            @endforeach
                                        
                                              
                                            </select>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>{{trans('reservations.orderdata')}}</h5>
                                    </div>
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label class="col-form-label" for="inputInlineUsername">{{trans('reservations.ordernumber')}}</label>
                                            <input class="form-control" id="title" type="text" value="{{ $reservation->order_number }}" readonly="readonly">
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label" for="inputInlinePassword">{{trans('reservations.datefrom')}}</label>
                                            <input class="form-control" id="date_from" name="date_from" type="text" value="{{ $reservation->date_from }}">
                                        </div>
                                        
                                      
                                      
                                        
                                        <div class="form-group">
                                            <label class="col-form-label" for="inputInlinePassword">{{trans('reservations.dateto')}}</label>
                                            <input class="form-control" id="title" type="text" value="{{ $reservation->date_to }}"readonly="readonly">
                                        </div>
                                          
                                            <div class="form-group">
                                            <label class="col-form-label" for="inputInlinePassword">{{trans('reservations.basckets')}}</label>
                                            <input class="form-control" id="baskets" name="baskets" type="text" value="{{ $reservation->baskets }}">
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="col-form-label" for="inputInlinePassword">{{trans('reservations.days')}}</label>
                                            <input class="form-control" id="title" type="text" value="{{ $reservation->days }}"readonly="readonly">
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label" for="inputInlinePassword">{{trans('reservations.total')}}</label>
                                            <input class="form-control" id="title" type="text" value="{{ $reservation->total }}"readonly="readonly">
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label" for="inputInlinePassword">{{trans('reservations.status')}}</label>
                                            <input class="form-control" id="status" type="text" value="@if ($reservation->status == true) Confirmed @else not Confirmed @endif" readonly="readonly">
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label" for="inputInlinePassword">{{trans('reservations.paymentinfo')}}</label>
                                            <input class="form-control" id="payment_info" type="text" value="@if ($reservation->status == true) cash @else Online @endif" readonly="readonly">
                                        </div>
                                

                                    </div>
                                </div>
                            </div>
      
                            <div class="col-sm-12 text-center mt-3" style="margin-left: -260px;
    margin-bottom: 15px;">
                                <button class="btn btn btn-primary mr-2" type="submit" data-original-title="" title="">{{trans('reservations.edit')}}</button>
                                <a href="{{'reservation.index'}}"  class="btn btn-secondary" type="button" data-original-title="" title="">{{trans('reservations.cancel')}}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>



<script>



   
   $(document).on('change', '#company_id', function(){
            var company_id = $(this).val();
            
            if(company_id){
                $.ajax({
                    type:"GET",
                    
                     url:"/get-company-containers/"+company_id,
                    success:function(res){
                        if(res){
                            console.log(res);
                            $("#container_id").empty();
                            $.each(res,function(key,value){
                                $("#container_id").append('<option value="'+value+'" >'+key+'</option>');
                            });
                        }
                        if(res.length === 0){
                            $("#container_id").empty();
                        }
                    }
                });
            }else{
                $("#container_id").empty();
            }
        });
  
</script>
@endsection





