@extends('dashboard.layouts.master')
@section('content')

    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('reservations.orderdata')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}" data-original-title="" title=""><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg></a></li>
                                <li class="breadcrumb-item">{{trans('dashboard.dashboard')}}</li>
                                <li class="breadcrumb-item active">{{trans('reservations.orderdata')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    @include('dashboard.partials.msg')

                    <div class="card">

                        <div class="card-body">
                            <div class="table-responsive">
                                <div id="advance-1_wrapper" class="dataTables_wrapper">

                                    <table class="display dataTable" id="advance-1" role="grid" aria-describedby="advance-1_info">
                                        <thead>
                                        <tr role="row">
                                            <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 100px;">#</th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 100px;">{{trans('reservations.user')}}</th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 100px;">{{trans('reservations.wastecontainer')}}</th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 100px;">{{trans('reservations.ordernumber')}}</th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 100px;">{{trans('reservations.status')}}</th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 175px;">{{trans('reservations.is_emtpy')}}</th>

                                            <th class="sorting" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 50px;">{{trans('reservations.createdat')}}</th>
                                            <th class="sorting" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 50px;">{{trans('reservations.actions')}}</th>

                                        </thead>
                                        <tbody>
                                        @foreach($orders as $key=>$order)
                                            <tr role="row" class="odd">
                                                <td class="sorting_1">{{ $key + 1 }}</td>
                                                <td>{{ $order->user['name'] }}</td>
                                                <td>{{ $order->wasteContainer['name_E']}}</td>
                                                <td>{{ $order->reservation['order_number'] }}</td>
                                                <td>
                                                 <span class="label label-info">Confirmed</span>
                                                </td>
                                                <td>  @if($order->is_empty == '0')
                                                        <span class="label label-danger">{{trans('reservations.empty')}}</span>
                                                    @else
                                                        <span class="label label-info">{{trans('reservations.fill')}} </span>
                                                    @endif
                                                </td>
                                                <td>{{ $order->created_at }}</td>

                                                <td>
                                                <td>
                                                                                                    <a href="{{ route('order.show',$order->id) }}" class="btn btn-info btn-sm"><i class="material-icons">{{trans('reservations.details')}}</i></a>
                                                <a href="{{ route('order.edit',$order->id) }}" class="btn btn-warning btn-sm"><i class="material-icons">{{trans('reservations.edit')}}</i></a>

                                                    <form id="delete-form-{{ $order->id }}" action="{{ route('order.destroy',$order->id) }}" style="display: none;" method="POST">
                                                        @csrf
                                                    </form>
                                                    <button type="button" class="btn btn-danger active" onclick="if(confirm('Are you sure? You want to delete this?')){
                                                            event.preventDefault();
                                                            document.getElementById('delete-form-{{ $order->id }}').submit();
                                                            }else {
                                                            event.preventDefault();
                                                            }"><i class="material-icons">{{trans('reservations.delete')}}</i></button>
{{--                                                    <a target="_blank" href="whatsapp://send?text=Here is the Order:https://hawity.my-staff.net/en/order/{{$order->id}}" class="btn btn-success btn-sm"><i class="material-icons">{{trans('reservations.send')}}</i></a>--}}

                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 100px;">#</th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 100px;">{{trans('reservations.user')}}</th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 100px;">{{trans('reservations.wastecontainer')}}</th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 100px;">{{trans('reservations.ordernumber')}}</th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 100px;">{{trans('reservations.status')}}</th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 175px;">{{trans('reservations.is_emtpy')}}</th>

                                            <th class="sorting" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 50px;">{{trans('reservations.createdat')}}</th>
                                            <th class="sorting" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 50px;">{{trans('reservations.actions')}}</th>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>


@endsection