<!DOCTYPE html>
<html lang="ar" dir=”rtl”>
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<head>
    <style>
        @import url('https://fonts.googleapis.com.css?family=Open+Sans:400,700');
        *{
            margin: 0;
            padding: 0;
            line-height: 1.5;
            font-family: 'XB Riyaz';
            color: #1b1e21;
            direction: rtl;
        }

    </style>
</head>
<body>






<div class="container" style="
direction:rtl ">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="invoice">
                        <div>
                            <div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="media">
                                            <div class="media-left"><img class="media-object img-60" src="{{ asset('assets/coming/assets/img/favicons/Group 1188.png') }}" alt="" data-original-title="" title=""></div>
                                            <div class="media-body m-l-20">
                                                <h4 class="media-heading">فاتورة</h4>
                                            </div>
                                        </div>
                                         End Info
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="text-md-right">
                                            <h3> فاتورة # <span class="digits counter">{{ $order->order_number }}</span></h3>
                                            <p>تاريخ الطلب : <span class="digits"> {{ $order->created_at }}</span><br>
                                                طريقة الدفع :  <span class="digits">@if ($order->status == true) cash @else Online @endif</span></p>
                                        </div>
                                         End Title
                                    </div>
                                </div>
                            </div>
                            <hr>
                             End InvoiceTop
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="media">
                                        <div class="media-left">
                                            <img class="media-object rounded-circle img-60" src="{{ asset($order->companySeller->icon) }}" style="height: 100px; width: 100px" alt="" data-original-title="" title="">
                                            <h4 class="media-heading">اسم الشركة : {{ $order->companySeller->company_name_A }}</h4>
                                        </div>
                                        <div class="media-body m-l-20">
                                            <h4 class="media-heading">اسم العميل : {{ $order->user['name'] }}</h4>
                                            <p> بريد العميل : {{ $order->user['email'] }}<br><span class="digits">هاتف العميل : {{ $order->user['phone'] }}</span></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="text-md-right" id="project">
                                        <h6>اسم الحاوية</h6>
                                        <p>{{ $order->wasteContainer->name_A }}</p>
                                    </div>
                                </div>
                            </div>
                             End Invoice Mid
                            <div style="direction: rtl;max-width: 100px;">
                                <div class="table-responsive invoice-table" id="table" style="direction: rtl;max-width: 100px;">
                                    <table class="table table-bordered table-striped" style="direction: rtl;max-width: 100px;">
                                        <tbody>
                                        <tr>
                                            <td class="item">
                                                <h6 class="p-2 mb-0">وصف الحاوية</h6>
                                            </td>
                                            <td class="Hours">
                                                <h6 class="p-2 mb-0">المسافة</h6>
                                            </td>
                                            <td class="Rate">
                                                <h6 class="p-2 mb-0">عدد الايام</h6>
                                            </td>
                                            <td class="subtotal">
                                                <h6 class="p-2 mb-0">السعر</h6>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>{{ $order->wasteContainer->description_A }}</label>

                                            </td>
                                            <td>
                                                <p class="itemtext digits">{{ $order->wasteContainer->distance }} yards</p>
                                            </td>
                                            <td>
                                                <p class="itemtext digits">{{ $order->days }}</p>
                                            </td>
                                            <td>
                                                <p class="itemtext digits">{{ $order->total }}</p>
                                            </td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>

                            </div>
                             End InvoiceBot
                        </div>

                         End Invoice
                         End Invoice Holder
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>











<!--<!DOCTYPE html>-->
<!--<html lang="en">-->
<!--  <head>-->
    
<!--    <title>Example 1</title>-->
<!--    <link rel="stylesheet" href="{{ asset('assets/invoices/style.css')}}" media="all" />-->
<!--  </head>-->
<!--  <body>-->
<!--    <header class="clearfix">-->
<!--      <div id="logo">-->
<!--        <img src="{{ asset('assets/coming/assets/img/favicons/Group 1188.png') }}" alt="ambeco">-->
<!--      </div>-->
<!--      <h1>   حاويتي  </h1>-->
<!--      <div id="company" class="clearfix">-->
<!--        <div>شركة : {{ $order->companySeller->company_name_A }}</div>-->
<!--        <div>  س.ت :   {{$order->companySeller->phone}}<br /> رقم العضوية : {{$order->companySeller->id}}</div>-->
       
      
<!--      </div>-->
<!--      <div id="project">-->
<!--        <div><span>الرقم الضريبي </span> {{$order->companySeller->commercial_register}}</div>-->
<!--        <div><span>رقم الفاتورة</span> {{ $order->order_number }}</div>-->
<!--        <div><span>اسم العميل</span> <span >: {{ $order->user['name']}} </span></div>-->
<!--        <div><span>التاريخ</span> {{$order->created_at}}</div>-->
<!--        <div><span>بريد العميل</span> : {{ $order->user['email']}}</div>-->
<!--		  <div><span>الهاتف</span>  : {{ $order->user['phone']}}</div>-->
<!--      </div>-->
<!--    </header>-->
<!--    <main>-->
<!--      <table>-->
<!--        <thead>-->
<!--          <tr>-->
<!--             <th>رقم الصنف </th>-->
<!--			   <th>البيان</th>-->
<!--			  <th class="service">المسافة</th>-->
<!--			   <th class="service">عدد الايام</th>-->
<!--            <th>السعر الإجمالى  / الريال</th>-->
<!--          </tr>-->
<!--        </thead>-->
<!--        <tbody>-->
            
          
<!--                                        @foreach($orders-> $user as $order)-->
<!--          <tr>-->
<!--            <td class="service">{{ $order->id }}</td>-->
<!--            <td class="desc">{{ $order->wasteContainer->description_A }}</td>-->
<!--            <td class="unit">{{ $order->wasteContainer->distance }} yards </td>-->
<!--            <td class="qty">{{ $order->days }}</td>-->
<!--            <td class="total">{{ $order->total }} ر.س</td>-->
			
<!--          </tr>-->
<!--            @endforeach-->
                                 
		
          
			
<!--          <tr>-->
<!--			      <td class="grand total">562 ر.س</td>-->
<!--            <td colspan="7" class="grand total">الإجمالى </td>-->
        
			 
			
<!--          </tr>-->
						
<!--          <tr>-->
<!--			      <td class="grand total">10 %</td>-->
<!--            <td colspan="7" class="grand total">الخصم </td>-->
        
			 
			
<!--          </tr>-->
						
<!--          <tr>-->
<!--			      <td class="grand total">562 ر.س</td>-->
<!--            <td colspan="7" class="grand total">الضريبة </td>-->
        
			 
			
<!--          </tr>-->
						
<!--          <tr>-->
<!--			      <td class="grand total">992 ر.س</td>-->
<!--            <td colspan="7" class="grand total">الصافي </td>-->
        
			 
			
<!--          </tr>-->
<!--        </tbody>-->
<!--      </table>-->
		
<!--		<div class="signature">-->
<!--		<div id="project">-->
<!--        <div><span>توقيع المسئول </span> ...................................................</div>-->
<!--        <div><span>البائع </span> ...................................................</div>-->
<!--        <div><span>التوقيع  </span>...................................................</div>-->
        
<!--      </div>-->
<!--			<div id="project2">-->
<!--        <div><span> المستلم </span> ...................................................</div>-->
<!--        <div><span> التوقيع</span> ...................................................</div>-->
     
        
<!--      </div>-->
<!--     </div>-->
<!--    </main>-->
	  
  
<!--  </body>-->
<!--</html>-->