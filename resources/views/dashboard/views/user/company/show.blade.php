@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('company.showcompanydata')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">{{trans('dashboard.dashboard')}}</li>
                                <li class="breadcrumb-item active">{{trans('company.showcompanydata')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="edit-profile">
                <div class="row">

                    <div class="col-lg-12">
                        @include('dashboard.partials.msg')

                            <div class="card-header">
                                @if(request()->segment(1)=='en')
                                <h4 class="card-title mb-0">{{trans('company.showcompanydata')}}</h4>
                                    @else
                                    <h4 class="card-title mb-0" style="
    text-align: right;
">{{trans('company.showcompanydata')}}</h4>
                                    @endif
                            </div>
                        @if(request()->segment(1)=='en')
                            <div class="card-body">
                                @else
                                    <div class="card-body" style="
    text-align: right;
">
                                        @endif
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">{{trans('company.name')}}</label>
                                        <input class="form-control" readonly type="text" name="name" value="{{$user->name}}">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">{{trans('company.email')}}</label>
                                        <input class="form-control" readonly type="email" name="email" value="{{$user->email}}">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">{{trans('company.phone')}}</label>
                                        <input class="form-control" readonly type="number" name="phone" value="{{$user->phone}}">
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">{{trans('company.commercialregister')}}</label>
                                        <input class="form-control" readonly type="number" name="commercial_register" value="{{$user->commercial_register}}">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">{{trans('company.taxrecord')}}</label>
                                        <input class="form-control" readonly type="number" name="tax_record" value="{{$user->tax_record}}">
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">{{trans('company.city')}}</label>
                                        <select class="form-control" readonly name="city">
                                            @foreach($cities as $city)
                                                <option {{ $city->id == $user->city->id ? 'selected' : '' }} value="{{ $city->id }}">{{ $city->name_E }}</option>
                                            @endforeach
                                        </select>                                    </div>
                                </div>

                            </div>
                            <div class="card-footer text-left">
                                <a href="{{route('user.company.index')}}" class="btn btn-primary">{{trans('company.back')}}</a>
                            </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>



@endsection