@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('admin.addnew')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">{{trans('admin.users')}}</li>
                                <li class="breadcrumb-item active">{{trans('admin.addnew')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="edit-profile">
                <div class="row">

                    <div class="col-lg-12">
                        @include('dashboard.partials.msg')

                        <form class="card" method="POST" action="{{ route('user.admin.store') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="card-header">
                                <h4 class="card-title mb-0">{{trans('admin.addnew')}}</h4>
                            </div>
                            <div class="card-body">
                                    <div class="col-sm-6 col-md-12">
                                        <div class="form-group">
                                            <label class="form-label">{{trans('admin.name')}}</label>
                                            <input class="form-control" type="text" name="name">
                                        </div>
                                    </div>
                                <div class="col-sm-6 col-md-12">
                                    <div class="form-group">
                                        <label class="form-label">{{trans('admin.email')}}</label>
                                        <input class="form-control" type="email" name="email">
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-12">
                                    <div class="form-group">
                                        <label class="form-label" for="exampleFormControlSelect9">roles</label>
                                        <select class="form-control digits" name="role" id="selectbasic">
                                            @foreach($allroles as $role)
                                                <option value="{{$role->id}}">{{$role->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                            <label class="form-label">Upload File</label>
                                                <input class="form-control" name="image" type="file">
                                        </div>
                                    </div>


                                <div class="col-sm-6 col-md-12">
                                    <div class="form-group">
                                        <label class="form-label">{{trans('admin.password')}}</label>
                                        <input class="form-control" type="password" name="password" placeholder="Password">
                                    </div>
                                </div>


                            <div class="card-footer text-left">
                                <button class="btn btn-primary" type="submit">{{trans('admin.submit')}}</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>



@endsection
