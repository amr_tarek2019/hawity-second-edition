@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('admin.editadmin')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">{{trans('dashboard.dashboard')}}</li>
                                <li class="breadcrumb-item active">{{trans('admin.editadmin')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="edit-profile">
                <div class="row">

                    <div class="col-lg-12">
                        @include('dashboard.partials.msg')

                        <form class="card" method="POST" action="{{ route('user.admin.update',$user->id) }}" enctype="multipart/form-data">
                            @csrf
                            <div class="card-header">
                                <h4 class="card-title mb-0">{{trans('admin.editadmin')}}</h4>
                            </div>
                            <div class="card-body">
                                <div class="col-sm-6 col-md-12">
                                    <div class="form-group">
                                        <label class="form-label">{{trans('admin.name')}}</label>
                                        <input class="form-control" type="text" name="name" value="{{$user->name}}">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-12">
                                    <div class="form-group">
                                        <label class="form-label">{{trans('admin.email')}}</label>
                                        <input class="form-control" type="email" name="email" value="{{$user->email}}">
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-12">
                                    <div class="form-group">
                                        <label class="form-label" for="exampleFormControlSelect9">role</label>
                                        <select class="form-control digits" name="role" id="selectbasic">
                                            @foreach($allroles as $role)
                                                <option value="{{$role->id}}">@if($role->name=='Delegates Admin')مدير المناديب@else مدير الحركة@endif</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                    <label class="form-label" for="filebutton">admin image</label>
                                    <img src="{{$user->image}}" style="width:70px; height:70px; ">
                                    <div class="col-lg-3">
                                        <input id="filebutton" name="image" class="input-file" type="file">
                                    </div>
                                </div>
                                </div>

                                <div class="col-sm-6 col-md-12">
                                    <div class="form-group">
                                        <label class="form-label">{{trans('admin.password')}}</label>
                                        <input class="form-control" type="password" name="password" placeholder="Password">
                                    </div>
                                </div>



                            </div>
                            <div class="card-footer text-left">
                                <button class="btn btn-primary" type="submit">{{trans('admin.submit')}}</button>
                            </div>

                        </form>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>



@endsection
