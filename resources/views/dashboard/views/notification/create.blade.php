@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('notifications.addnew')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}" data-original-title="" title=""><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg></a></li>
                                <li class="breadcrumb-item">{{trans('dashboard.dashboard')}}</li>
                                <li class="breadcrumb-item active">{{trans('notifications.notification')}}</li>
                            </ol>
                        </div>
                    </div>
                    <!-- Bookmark Start-->

                    <!-- Bookmark Ends-->
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    @include('dashboard.partials.msg')

                    <div class="card">

                        <div class="card-body">
                            <form class="needs-validation" novalidate=""  method="POST" action="{{ route('notification.notify') }}" enctype="multipart/form-data">
                                @csrf
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label for="validationCustom01">{{trans('notifications.title')}}</label>
                                        <input class="form-control" id="title" type="text" name="title" placeholder="Title" required="" data-original-title="" title="">
                                    </div>

                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label for="validationCustom03">{{trans('notifications.notification')}}</label>
                                        <input class="form-control" id="notification" name="notification" type="text" placeholder="Notification" required="" data-original-title="" title="">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <label class="col-sm-3 col-form-label">{{trans('notifications.selectuser')}}</label>
                                <select id="selectbasic" name="user_type" class="form-control btn-square">
                                <option value="individual">Individuals</option>
                                <option value="company">Companies</option>
                                </select>
                                </div>

                                <br>

                                <div class="form-row">
                                <button class="btn btn-primary" type="submit" data-original-title="" title="">{{trans('notifications.submit')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>






@endsection
