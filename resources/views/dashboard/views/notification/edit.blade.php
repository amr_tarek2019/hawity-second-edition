@extends('dashboard.layouts.master')
@section('content')
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDhnmMC23noePz6DA8iEvO9_yNDGGlEaeM&callback=initMap">
    </script>
<style>
    .text-lg-left {
        text-align: right !important;
    }
</style>
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i data-feather="home"> </i> الرئيسية </a></li>
                                <li class="breadcrumb-item active"><a href="{{route('notification.index')}}">  كل الاشعارات </a> </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        <!-- Container-fluid starts-->
            @if ($errors->any())
                        @foreach ($errors->all() as $error)
                    <div class="alert alert-danger inverse alert-dismissible fade show" role="alert"><i class="icon-thumb-down"></i>

                        <p>{{ $error }}</p>
                        <button class="close" type="button" data-dismiss="alert" aria-label="Close" data-original-title="" title=""><span aria-hidden="true">×</span></button>

                    </div>

                @endforeach
            @endif

            <div class="container-fluid">
                <div class="card">
                    <div class="card-body">
                        <ul class="nav nav-tabs nav-material nav-secondary nav-left" id="danger-tab" role="tablist">
                            <li class="nav-item"><a class="nav-link active show" onclick="openCity('home','profile')" id="danger-home-tab" data-toggle="tab" href="tab-material.html#danger-home" role="tab" aria-controls="danger-home" aria-selected="true" data-original-title="" title=""><i class="icofont icofont-throne"></i>العربي</a>
                                <div class="material-border"></div>
                            </li>
                            <li class="nav-item"><a class="nav-link" id="profile-danger-tab" onclick="openCity('profile','home')" data-toggle="tab" href="tab-material.html#danger-profile" role="tab" aria-controls="danger-profile" aria-selected="false" data-original-title="" title=""><i class="icofont icofont-throne"></i>الانجليزية</a>
                                <div class="material-border"></div>
                            </li>
                        </ul>
                         <div class="tab-content" id="danger-tabContent">
                             <form  action="{{route('notification.update',$notify->id)}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                 @csrf
                                 @method('patch')
                                 <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="danger-home-tab">

                                        <!-- Form Name -->
                                        <!-- Text input-->
                                        <div class="form-group row">
                                            <div class="col-lg-6">
                                                <label class="col-lg-12 control-label text-lg-left" for="trip_arr_deps_airlines">العنوان</label>
                                                <input id="textinput" name="ar[notifications_title]" value="{{$notify->getTranslation('ar')->notifications_title}}" type="text" placeholder="عنوان الاشعار" class="form-control btn-square input-md">

                                            </div>
                                        </div>
                                        <div class="form-group row">

                                     <div class="col-lg-6">
                                                <label class="col-lg-12 control-label text-lg-left" for="trip_arr_deps_airlines">الاشعار</label>
                                                <textarea id="trip_arr_deps_airlines" placeholder="نص الاشعار" value="" name="ar[notifications_desc]"  class="form-control" rows="3">{{$notify->getTranslation('ar')->notifications_desc}}</textarea>

                                            </div>

                                        <!-- Text input-->
                                        </div>

                                        <!-- Text input-->



                            </div>
                                 <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-danger-tab">


                                        <!-- Form Name -->

                                        <!-- Text input-->
                                     <div class="form-group row">
                                         <div class="col-lg-6">
                                             <label class="col-lg-12 control-label text-lg-left" for="trip_arr_deps_airlines">العنوان</label>
                                             <input id="textinput" name="en[notifications_title]" value="{{$notify->getTranslation('en')->notifications_title}}" type="text" placeholder="عنوان الاشعار بالانجليزية" class="form-control btn-square input-md">

                                         </div>
                                     </div>
                                     <div class="form-group row">

                                         <div class="col-lg-6">
                                             <label class="col-lg-12 control-label text-lg-left" for="trip_arr_deps_airlines">الاشعار</label>
                                             <textarea id="trip_arr_deps_airlines" placeholder="نص الاشعار بالانجليزية" name="en[notifications_desc]"  value="" class="form-control" rows="3">{{$notify->getTranslation('en')->notifications_desc}}</textarea>

                                         </div>

                                         <!-- Text input-->
                                     </div>

                                        <!-- Text input-->

                                        <!-- Text input-->

                            </div>
                                 <div class="form-group row">
                                     <div class="col-lg-2">
                                         <label class="col-lg-12 control-label text-lg-left" for="trip_arr_deps_airlines">صورة الاشعار</label>
                                          <img src="{{SiteImages_path('notify') . '/thumbnail/'.$notify->notifications_img}}">
                                     </div>
                                     <div class="col-lg-6">
                                         <label class="col-lg-12 control-label text-lg-left" for="trip_arr_deps_airlines">رفع صورة الاشعار</label>
                                         <input id="filebutton" name="notifications_img" class="input-file" type="file">

                                     </div>


                                     <!-- Text input-->
                                 </div>
                                 <input type="submit" class="btn btn-success btn-lg active" value="تعديل">

                             </form>
                        </div>

                    </div>
                </div>



        </div>
        <!-- Container-fluid Ends-->
    </div>

    <!-- Modal -->
    </div>
    <script>
        function openCity(id,id1) {
          $('#'+id).addClass('active');
          $('#'+id).addClass('show');
          $('#'+id1).removeClass('active');
          $('#'+id1).removeClass('show');
           console.log('#'+id);
         }


    </script>
@endsection
