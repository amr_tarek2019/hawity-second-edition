@extends('dashboard.layouts.master')
@section('content')
<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <div class="page-header-left">
                        <h3>{{trans('dashboard.analysis')}}</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                            <li class="breadcrumb-item">{{trans('dashboard.dashboard')}}</li>
                            <li class="breadcrumb-item active">{{trans('dashboard.analysis')}}</li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-7 xl-100">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h5>{{trans('dashboard.totalusers')}}</h5>
                            </div>
                            <div class="card-body charts-box">
                                <div class="flot-chart-container">
                                    <div class="flot-chart-placeholder" id="graph123"></div>
                                </div>
                                <div class="code-box-copy">
                                    <button class="code-box-copy__btn btn-clipboard" data-clipboard-target="#example-head" title="Copy"><i class="icofont icofont-copy-alt"></i></button>
                                    <pre><code class="language-html" id="example-head">&lt;!-- Cod Box Copy begin --&gt;
  &lt;div class="card-body charts-box"&gt;
    &lt;div class="flot-chart-container"&gt;
      &lt;div id="graph123" class="flot-chart-placeholder"&gt;&lt;/div&gt;
    &lt;/div&gt;
  &lt;/div&gt;
&lt;!-- Cod Box Copy end --&gt;</code></pre>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- Container-fluid Ends-->
</div>
@endsection

@section('myjsfile')
    <script>
     var morris_chart = {
       init: function() {
            Morris.Area({
               element: 'graph123',
               behaveLikeLine: true,
               scaleShowGridLines: true,
                scaleGridLineColor: "rgba(0,0,0,0.05)",
                 scaleGridLineWidth: 0.2,
                 data: [
                         @foreach($numbers_of_users as $user)
                     {
                     x: '{{$user->date}}',
                     z: '{{$user->count}}'
                     },
                     @endforeach

                 ],
                 xkey: 'x',
                 ykeys: ['z'],
                 labels: ['Z'],
                 lineColors: [endlessAdminConfig.primary],
             })
         }
     };
     (function($) {
         "use strict";
         morris_chart.init()
     })(jQuery);
    </script>
@stop


{{--@section('script')--}}
    {{--<script>--}}

        {{--var morris_chart = {--}}
            {{--init: function() {--}}
                {{--Morris.Area({--}}
                    {{--element: 'graph123',--}}
                    {{--behaveLikeLine: true,--}}
                    {{--scaleShowGridLines: true,--}}
                    {{--scaleGridLineColor: "rgba(0,0,0,0.05)",--}}
                    {{--scaleGridLineWidth: 0.2,--}}
                    {{--data: [{--}}
                        {{--x: '2009',--}}
                        {{--z: 0--}}
                    {{--},--}}
                        {{--{--}}
                            {{--x: '2010',--}}
                            {{--z: 2.25--}}
                        {{--},--}}
                        {{--{--}}
                            {{--x: '2011',--}}
                            {{--z: 1.25--}}
                        {{--},--}}
                        {{--{--}}
                            {{--x: '2012',--}}
                            {{--z: 3--}}
                        {{--},--}}
                        {{--{--}}
                            {{--x: '2013',--}}
                            {{--z: 1.25--}}
                        {{--},--}}
                        {{--{--}}
                            {{--x: '2014',--}}
                            {{--z: 2.25--}}
                        {{--},--}}
                        {{--{--}}
                            {{--x: '2015',--}}
                            {{--z: 0--}}
                        {{--}--}}
                    {{--],--}}
                    {{--xkey: 'x',--}}
                    {{--ykeys: ['z'],--}}
                    {{--labels: ['Z'],--}}
                    {{--lineColors: [endlessAdminConfig.primary],--}}
                {{--})--}}
            {{--}--}}
        {{--};--}}
        {{--(function($) {--}}
            {{--"use strict";--}}
            {{--morris_chart.init()--}}
        {{--})(jQuery);--}}

    {{--</script>--}}
{{--@endsection--}}



