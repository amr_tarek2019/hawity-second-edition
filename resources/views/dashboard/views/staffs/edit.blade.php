@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('staff.Edit Staff')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">{{trans('staff.staffs')}}</li>
                                <li class="breadcrumb-item active">{{trans('staff.Edit Staff')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('category.completeform')}}</h5>
                        </div>
                        <div class="card-body">
                            <form class="needs-validation" novalidate="" action="{{ route('staffs.update', $staff->id) }}" method="POST" enctype="multipart/form-data">
                                <input name="_method" type="hidden" value="PATCH">
                                @csrf
                                <div class="col-md-12">
                                    <div class="form-group mb-0">
                                        <label class="form-label">{{trans('user.name')}}</label>
                                        <input class="form-control" value="{{ $staff->user->name }}" id="name" name="name" placeholder="name"/>
                                    </div>
                                </div>
                                <br>
                                <div class="col-md-12">
                                    <div class="form-group mb-0">
                                        <label class="form-label">{{trans('user.email')}}</label>
                                        <input class="form-control" value="{{ $staff->user->email }}" id="email" name="email"  placeholder="email"/>
                                    </div>
                                </div>
                                <br>

                                <div class="col-md-12">
                                    <div class="form-group mb-0">
                                        <label for="exampleFormControlTextarea4">{{trans('user.password')}}</label>
                                        <input class="form-control" id="Password" name="Password"  placeholder="Password"/>
                                    </div>
                                </div>
                                <br>
                                <div class="col-md-12">
                                    <div class="col-form-label">{{trans('staff.Role')}}</div>
                                    <select name="role_id" required class="form-control">
                                        @foreach($roles as $role)
                                            <option value="{{$role->id}}" @php if($staff->role_id == $role->id) echo "selected"; @endphp >{{$role->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <br>
                                <button class="btn btn-primary" type="submit">{{trans('user.submit')}}</button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
@endsection
