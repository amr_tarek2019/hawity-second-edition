{{--@extends('dashboard.layouts.master')--}}

{{--@section('content')--}}

{{--<div class="row">--}}
{{--    <div class="col-sm-12">--}}
{{--        <a href="{{ route('staffs.create')}}" class="btn btn-rounded btn-info pull-right">{{__('Add New Staff')}}</a>--}}
{{--    </div>--}}
{{--</div>--}}

{{--<br>--}}

{{--<!-- Basic Data Tables -->--}}
{{--<!--===================================================-->--}}
{{--<div class="panel">--}}
{{--    <div class="panel-heading">--}}
{{--        <h3 class="panel-title">{{__('Staffs')}}</h3>--}}
{{--    </div>--}}
{{--    <div class="panel-body">--}}
{{--        <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">--}}
{{--            <thead>--}}
{{--                <tr>--}}
{{--                    <th width="10%">#</th>--}}
{{--                    <th>{{__('Name')}}</th>--}}
{{--                    <th>{{__('Email')}}</th>--}}
{{--                    <th>{{__('Role')}}</th>--}}
{{--                    <th width="10%">{{__('Options')}}</th>--}}
{{--                </tr>--}}
{{--            </thead>--}}
{{--            <tbody>--}}
{{--                @foreach($staffs as $key => $staff)--}}
{{--                    <tr>--}}
{{--                        <td>{{$key+1}}</td>--}}
{{--                        <td>{{$staff->user->name}}</td>--}}
{{--                        <td>{{$staff->user->email}}</td>--}}
{{--                        <td>{{$staff->role->name}}</td>--}}
{{--                        <td>--}}
{{--                            <div class="btn-group dropdown">--}}
{{--                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">--}}
{{--                                    {{__('Actions')}} <i class="dropdown-caret"></i>--}}
{{--                                </button>--}}
{{--                                <ul class="dropdown-menu dropdown-menu-right">--}}
{{--                                    <li><a href="{{route('staffs.edit', encrypt($staff->id))}}">{{__('Edit')}}</a></li>--}}
{{--                                    <li><a onclick="confirm_modal('{{route('staffs.destroy', $staff->id)}}');">{{__('Delete')}}</a></li>--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                        </td>--}}
{{--                    </tr>--}}
{{--                @endforeach--}}
{{--            </tbody>--}}
{{--        </table>--}}

{{--    </div>--}}
{{--</div>--}}

{{--@endsection--}}













@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('staff.Staff DataTables')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">{{trans('staff.staffs')}}</li>
                                <li class="breadcrumb-item active">{{trans('staff.Staff DataTables')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <!-- Zero Configuration  Starts-->
                <div class="col-sm-12">
                    <a href="{{ route('staffs.create') }}" class="btn btn-primary">{{trans('user.addnew')}}</a>
                    @include('dashboard.partials.msg')
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('staff.Staff Data')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{trans('user.name')}}</th>
                                        <th>{{trans('user.email')}}</th>
                                        <th>{{trans('staff.Role')}}</th>
                                        <th>{{trans('user.actions')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($staffs as $key => $staff)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$staff->user->name}}</td>
                                            <td>{{$staff->user->email}}</td>
                                            <td>{{$staff->role->name}}</td>
                                            <td>
                                                <a href="{{ route('staffs.edit',encrypt($staff->id))}}" class="btn btn-info btn-sm"><i class="material-icons">{{trans('category.edit')}}</i></a>

                                                <form id="delete-form-{{ $staff->id }}" action="{{ route('staffs.destroy',$staff->id) }}" style="display: none;" method="POST">
                                                    @csrf
                                                </form>
                                                <button type="button" class="btn btn-danger btn-sm" onclick="if(confirm('{{trans('category.deletemsg')}}')){
                                                    event.preventDefault();
                                                    document.getElementById('delete-form-{{ $staff->id }}').submit();
                                                    }else {
                                                    event.preventDefault();
                                                    }"><i class="material-icons">{{trans('category.delete')}}</i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Zero Configuration  Ends-->

            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>






@endsection
