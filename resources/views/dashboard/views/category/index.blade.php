@extends('dashboard.layouts.master')
@section('content')

    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('categories.categoriesdatatable')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}" data-original-title="" title=""><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg></a></li>
                                <li class="breadcrumb-item">{{trans('categories.categoriesdatatable')}}</li>
                                <li class="breadcrumb-item active">{{trans('categories.categoriesdata')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                 
                    @include('dashboard.partials.msg')

                    <div class="card">

                        <div class="card-body">
                            <div class="table-responsive">
                                <div id="advance-1_wrapper" class="dataTables_wrapper">

                                    <table class="display dataTable" id="advance-1" role="grid" aria-describedby="advance-1_info">
                                        <thead>
                                        <tr role="row">
                                            <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 100px;">#</th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 150px;">{{trans('categories.namearabic')}}</th>
                                            <th class="sorting" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 150px;">{{trans('categories.nameenglish')}}</th>
                                             <th class="sorting" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 150px;">{{trans('categories.companiesorders')}}</th>
                                            <th class="sorting" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 150px;">{{trans('categories.image')}}</th>
                                            <th class="sorting" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 50px;">{{trans('categories.createdat')}}</th>
                                            <th class="sorting" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending" style="width: 50px;">{{trans('categories.updatedat')}}</th>
                                            <th class="sorting" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 200px;">{{trans('categories.actions')}}</th>
                                        </thead>
                                        <tbody>
                                        @foreach($categories as $key=>$category)

                                            <tr role="row" class="odd">
                                            <td class="sorting_1">{{ $key + 1 }}</td>
                                                <td>{{ $category->name_A }}</td>
                                            <td>{{ $category->name_E }}</td>
                                             
                                                    <td>{{App\CompanySeller::where('category_id',$category->id)->count()}}</td>
                                                
                                                    
                                                    
                                                <td><img class="img-responsive img-thumbnail" src="{{ asset($category->image) }}" style="height: 100px; width: 100px" alt=""></td>

                                                <td>{{ $category->created_at }}</td>
                                            <td>{{ $category->updated_at }}</td>
                                            <td>
                                                <a href="{{ route('category.edit',$category->id) }}" class="btn btn-info active"><i class="material-icons">{{trans('categories.edit')}}</i></a>

                                                <form id="delete-form-{{ $category->id }}" action="{{ route('category.destroy',$category->id) }}" style="display: none;" method="POST">
                                                    @csrf
                                                </form>
                                                <button type="button" class="btn btn-danger active" onclick="if(confirm('Are you sure? You want to delete this?')){
                                                        event.preventDefault();
                                                        document.getElementById('delete-form-{{ $category->id }}').submit();
                                                        }else {
                                                        event.preventDefault();
                                                        }"><i class="material-icons">{{trans('categories.delete')}}</i></button>
                                            </td>
                                        </tr>
                                       @endforeach

                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th rowspan="1" colspan="1">#</th>
                                            <th rowspan="1" colspan="1">{{trans('categories.namearabic')}}</th>
                                            <th rowspan="1" colspan="1">{{trans('categories.nameenglish')}}</th>
                                             <th rowspan="1" colspan="1">{{trans('categories.companiesorders')}}</th>
                                            <th rowspan="1" colspan="1">{{trans('categories.image')}}</th>
                                            <th rowspan="1" colspan="1">{{trans('categories.createdat')}}</th>
                                            <th rowspan="1" colspan="1">{{trans('categories.updatedat')}}</th>
                                            <th rowspan="1" colspan="1">{{trans('categories.actions')}}</th></tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>


@endsection