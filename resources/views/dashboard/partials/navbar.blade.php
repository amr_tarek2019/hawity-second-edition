<div class="page-main-header">
    <div class="main-header-right row">
        <div class="main-header-left d-lg-none">
            <div class="logo-wrapper"><a href="index.html"><img src="{{ asset('assets/dashboard/images/endless-logo.png') }}" alt=""></a></div>
        </div>
        <div class="mobile-sidebar d-block">
            <div class="media-body text-right switch-sm">
                <label class="switch"><i id="sidebar-toggle" data-feather="align-left"></i></label>
            </div>
        </div>
        <div class="nav-right col p-0">
            <ul class="nav-menus">
                <li>
                </li>
                <li class="onhover-dropdown"><a class="txt-dark" href="ico-icon.html#" data-original-title="" title="">
                        @if(request()->segment(1)=='en')
                        <h6>EN</h6>
                        @else
                            <h6>العربية</h6>
                            @endif
                    </a>
                    <ul class="language-dropdown onhover-show-div p-20">
                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                        <li><a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}" data-lng="en" data-original-title="" title=""><i class="flag-icon"></i>{{ $properties['native'] }}</a></li>
                        @endforeach
                    </ul>
                </li>

                <li class="onhover-dropdown">
                    <div class="media align-items-center"><img class="align-self-center pull-right img-50 rounded-circle" src="{{ asset('assets/coming/assets/img/favicons/Group 1188.png') }}" alt="">
                        <div class="dotted-animation"><span class="animate-circle"></span><span class="main-circle"></span></div>
                    </div>
                    @if(request()->segment(1)=='en')
                    <ul class="profile-dropdown onhover-show-div p-20">
                        @else
                            <ul class="profile-dropdown onhover-show-div p-20" style="
    margin-right: -35px;
">
                                @endif
                                @if(request()->segment(1)=='en')
                        <li><a href="{{route('profile.index')}}"><i data-feather="user"></i>                                   {{trans('dashboard.editprofile')}}</a></li>
                                @else
                                    <li  style="
                                    text-align: right;
                                    "><a href="{{route('profile.index')}}"><i data-feather="user"></i>                                   {{trans('dashboard.editprofile')}}</a></li>

                                    @endif
                        <li><a href="{{route('suggestion.index')}}"><i data-feather="mail"></i>                                    {{trans('dashboard.suggestions')}}</a></li>
                 
                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i data-feather="log-out"></i>
                                {{trans('dashboard.logout')}}
</a>

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form></li>
                    </ul>
                </li>
            </ul>
            <div class="d-lg-none mobile-toggle pull-right"><i data-feather="more-horizontal"></i></div>
        </div>
        <script id="result-template" type="text/x-handlebars-template">
            <div class="ProfileCard u-cf">
                <div class="ProfileCard-avatar"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-airplay m-0"><path d="M5 17H4a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2h-1"></path><polygon points="12 15 17 21 7 21 12 15"></polygon></svg></div>
                <div class="ProfileCard-details">
                    <div class="ProfileCard-realName">nina</div>
                </div>
            </div>
        </script>
        <script id="empty-template" type="text/x-handlebars-template">
            <div class="EmptyMessage">Your search turned up 0 results. This most likely means the backend is down, yikes!</div>

        </script>
    </div>
</div>