<div class="page-sidebar">

    @if(request()->segment(1)=='en')
        <div class="main-header-left d-none d-lg-block">
            <div class="logo-wrapper"><a href="{{route('dashboard')}}"><img src="{{ asset('assets/coming/assets/img/favicons/Group 1188.png') }}" style="margin-left: 60px" width="60px" alt=""></a></div>
        </div>
    @else
        <div class="main-header-right d-none d-lg-block">
            <div class="logo-wrapper"><a href="{{route('dashboard')}}"><img src="{{ asset('assets/coming/assets/img/favicons/Group 1188.png') }}" style="margin-left: 100px" width="60px" alt=""></a></div>
        </div>
    @endif
    <div class="sidebar custom-scrollbar">

        <ul class="sidebar-menu">
            <li><a class="sidebar-header" href="{{route('dashboard')}}"><i data-feather="home"></i><span>{{trans('dashboard.dashboard')}}</span></a>
            </li>

          <li><a class="sidebar-header" href="#"><i data-feather="users"></i><span>{{trans('dashboard.users')}}</span><i class="fa fa-angle-right pull-right"></i></a>
                <ul class="sidebar-submenu">

                    <li><a href="{{route('user.admin.index')}}"><i class="fa fa-circle"></i>{{trans('dashboard.admin')}}</a></li>
{{--                    <li><a href="{{route('user.member.index')}}"><i class="fa fa-circle"></i>{{trans('dashboard.member')}}</a></li>--}}


                    <li><a href="{{route('user.company.index')}}"><i class="fa fa-circle"></i>{{trans('dashboard.company')}}</a></li>
                    <li><a href="{{route('user.individual.index')}}"><i class="fa fa-circle"></i>{{trans('dashboard.individual')}}</a></li>
                </ul>
            </li>


   <li><a class="sidebar-header" href="index.html#">
                    <i data-feather="activity"></i><span>{{trans('dashboard.analysis')}}</span><i class="fa fa-angle-right pull-right"></i></a>
                <ul class="sidebar-submenu">
                    <li><a href="{{route('user.analysis.index')}}"><i class="fa fa-circle"></i>{{trans('dashboard.users')}}</a></li>
                    <li><a href="{{route('order.analysis.index')}}"><i class="fa fa-circle"></i>{{trans('dashboard.orders')}}</a></li>
                </ul>
            </li>

            <li><a class="sidebar-header" href="{{route('category.index')}}"><i data-feather="menu"></i><span>{{trans('dashboard.categories')}}</span></a>
            </li>
            @if(request()->segment(1)=='en')
            <li><a class="sidebar-header" href="{{route('company.index')}}"><i class="icofont icofont-company"></i><span>company</span></a>
@else
                <li><a class="sidebar-header" href="{{route('company.index')}}"><i class="icofont icofont-company"><span style="margin-right: 15px;">شركة</span></i></a>
@endif
                <li><a class="sidebar-header" href="{{route('container.index')}}"><i data-feather="trash"></i><span> {{trans('dashboard.containers')}}</span></a></li>
            <li><a class="sidebar-header" href="index.html#">
                    <i data-feather="tag"></i><span>{{trans('dashboard.booking')}}</span><i class="fa fa-angle-right pull-right"></i></a>
                <ul class="sidebar-submenu">
                    <li><a href="{{route('reservation.index')}}"><i class="fa fa-circle"></i>{{trans('dashboard.reservations')}}</a></li>
                    <li><a href="{{route('order.index')}}"><i class="fa fa-circle"></i>{{trans('dashboard.orders')}}</a></li>
                </ul>
            </li>
   <li><a class="sidebar-header" href="index.html#">
           <i data-feather="settings"></i><span>{{trans('dashboard.settings')}}</span><i class="fa fa-angle-right pull-right"></i></a>
                <ul class="sidebar-submenu">
                  <li><a href="{{route('about.index')}}"><i class="fa fa-circle"></i>{{trans('dashboard.about')}}</a></li>
                  <li><a href="{{route('privacy.index')}}"><i class="fa fa-circle"></i>{{trans('dashboard.privacy')}}</a></li>
                </ul>
              </li>
            <li><a class="sidebar-header" href="{{route('suggestion.index')}}"><i data-feather="mail"></i><span> {{trans('dashboard.suggestions')}}</span></a></li>
                       <li><a class="sidebar-header" href="{{route('notification.index')}}"><i data-feather="bell"></i><span>{{trans('dashboard.notifications')}}</span></a></li>


        </ul>
    </div>
</div>
