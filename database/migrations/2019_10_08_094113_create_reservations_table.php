<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('icon');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('container_id')->unsigned();
            $table->bigInteger('company_id')->unsigned();
            $table->string('order_number');
            $table->date('date_from');
            $table->date('date_to');
            $table->string('total');
            $table->boolean('status');
            $table->string('payment_info');
            $table->string('days');
            $table->string('price_per_day');
            $table->boolean('is_empty');
            $table->string('baskets');
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');

            $table->foreign('container_id')
                ->references('id')->on('waste_containers')
                ->onDelete('cascade');

            $table->foreign('company_id')
                ->references('id')->on('company_sellers')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
