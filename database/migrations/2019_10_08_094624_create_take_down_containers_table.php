<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTakeDownContainersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('take_down_containers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('photo');
            $table->bigInteger('container_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->text('note_E');
            $table->text('note_A');
            $table->timestamps();

            $table->foreign('container_id')
                ->references('id')->on('waste_containers')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('take_down_containers');
    }
}
