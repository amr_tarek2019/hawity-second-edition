<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
return view('welcome');
});

//Route::get('/login', function () {
//    return view('auth.login');
//});

Route::group(['namespace'=>'Dashboard'],function () {
Route::get('whats/show/{id}','ReservationController@whatsShow')->name('reservation.whats');
});
// Route::get('whatsapp://send?text=Here is the Order:https://hawity.my-staff.net/en/order/{id}','ReservationController@show')->name('reservation.whats');

Route::group(['prefix'=>'auth','namespace'=>'Authentication'],function () {
    Route::get('login','LoginController@index')->name('login');
    Route::post('admin/login','LoginController@adminLogin')->name('admin.login');
});
             Route::get('get-company-containers/{company_id}', 'Dashboard\ReservationController@getComanyContainers');

//Route::group(['prefix'=>'admin','middleware'=>'auth','namespace'=>'Dashboard'],function () {
Route::group(['prefix' => 'admin'], function () {
    Route::group(['middleware' => 'auth:admin','namespace'=>'Dashboard'], function () {
        Route::post('logout', 'DashboardController@logout')->name('logout');
        Route::group(['middleware' => ['role:Super Admin|Settings Admin']], function () {
    Route::get('dashboard','DashboardController@index')->name('dashboard');

 });
//    Route::resource('roles','RolesController');
//    Route::post('/roles/destroy/{id}', 'RolesController@destroy')->name('roles.destroy');
//
//    Route::resource('staffs','StaffController');
//    Route::post('/staffs/destroy/{id}', 'StaffController@destroy')->name('staffs.destroy');
    // Route::prefix('profile')->group(function (){
    //     Route::get('','ProfileController@index')->name('profile.index');
    //     Route::post('/update/{id}','ProfileController@update')->name('profile.update');

    // });
            Route::group(['middleware' => ['role:Super Admin|Settings Admin']], function () {
       Route::prefix('category')->group(function (){
            Route::get('','CategoryController@index')->name('category.index');
            Route::get('/create','CategoryController@create')->name('category.create');
            Route::post('/store','CategoryController@store')->name('category.store');
            Route::get('/edit/{id}','CategoryController@edit')->name('category.edit');
            Route::post('/update/{id}','CategoryController@update')->name('category.update');
            Route::post('/{id}','CategoryController@destroy')->name('category.destroy');
        });
            });

            Route::group(['middleware' => ['role:Super Admin|Settings Admin']], function () {
        Route::prefix('company')->group(function (){
            Route::get('','CompanySellersController@index')->name('company.index');
            Route::get('/create','CompanySellersController@create')->name('company.create');
            Route::post('/store','CompanySellersController@store')->name('company.store');
            Route::get('/edit/{id}','CompanySellersController@edit')->name('company.edit');
            Route::post('/update/{id}','CompanySellersController@update')->name('company.update');
            Route::post('/{id}','CompanySellersController@destroy')->name('company.destroy');
                        Route::get('/show/{id}','CompanySellersController@show')->name('company.show');
            Route::post('/status/{id}', 'CompanySellersController@updateStatus')->name('company.status');
        });
            });
            Route::group(['middleware' => ['role:Super Admin|Settings Admin']], function () {
        Route::prefix('suggestion')->group(function (){
            Route::get('','SuggestionsController@index')->name('suggestion.index');
            Route::get('/show/{id}','SuggestionsController@show')->name('suggestion.show');
            Route::post('/{id}','SuggestionsController@destroy')->name('suggestion.destroy');
        });
            });

        Route::group(['middleware' => ['role:Super Admin|Settings Admin']], function () {
        Route::prefix('container')->group(function (){
            Route::get('','ContainerController@index')->name('container.index');
            Route::get('/create','ContainerController@create')->name('container.create');
            Route::post('/store','ContainerController@store')->name('container.store');
            Route::get('/edit/{id}','ContainerController@edit')->name('container.edit');
                        Route::get('/show/{id}','ContainerController@show')->name('container.show');

            Route::post('/update/{id}','ContainerController@update')->name('container.update');
            Route::post('/{id}','ContainerController@destroy')->name('container.destroy');
        });
        });
        Route::group(['middleware' => ['role:Super Admin|Settings Admin']], function () {
          Route::prefix('about')->group(function (){
            Route::get('','AboutController@index')->name('about.index');
            Route::post('','AboutController@update')->name('about.update');
        });
        });
        Route::group(['middleware' => ['role:Super Admin|Settings Admin']], function () {
  Route::prefix('privacy')->group(function (){
            Route::get('','PrivacyController@index')->name('privacy.index');
            Route::post('','PrivacyController@update')->name('privacy.update');
        });
        });
        Route::group(['middleware' => ['role:Orders Admin|Super Admin']], function () {
              Route::prefix('reservation')->group(function (){
            Route::get('','ReservationController@index')->name('reservation.index');
                            Route::get('/{id}','ReservationController@show')->name('reservation.show');
                                        Route::get('/edit/{id}','ReservationController@edit')->name('reservation.edit');

  Route::post('/update/{id}','ReservationController@update')->name('reservation.update');

            Route::post('/{id}','ReservationController@destroy')->name('reservation.destroy');
        });
        });
        Route::group(['middleware' => ['role:Orders Admin|Super Admin']], function () {
        Route::prefix('order')->group(function (){
            Route::get('','OrderController@index')->name('order.index');
                Route::get('/{id}','OrderController@show')->name('order.show');
                            Route::get('/edit/{id}','OrderController@edit')->name('order.edit');

  Route::post('/update/{id}','OrderController@update')->name('order.update');
            Route::post('/{id}','OrderController@destroy')->name('order.destroy');
        });
        });
        // Route::prefix('user')->group(function (){
        //     Route::get('','UserController@index')->name('user.index');
        //     Route::get('/create','UserController@create')->name('user.create');
        //     Route::post('/store','UserController@store')->name('user.store');
        //     Route::get('/edit/{id}','UserController@edit')->name('user.edit');
        //     Route::post('/update/{id}','UserController@update')->name('user.update');
        //     Route::post('/{id}','UserController@destroy')->name('user.destroy');
        // });





            Route::group(['middleware' => ['role:Super Admin']], function () {
            Route::prefix('user/admin')->group(function (){
            Route::get('','UserAdminController@index')->name('user.admin.index');
            Route::get('/create','UserAdminController@create')->name('user.admin.create');
            Route::post('/store','UserAdminController@store')->name('user.admin.store');
            Route::get('/edit/{id}','UserAdminController@edit')->name('user.admin.edit');
            Route::post('/update/{id}','UserAdminController@update')->name('user.admin.update');
            Route::post('/{id}','UserAdminController@destroy')->name('user.admin.destroy');
    });
            });
//
//    Route::prefix('user/member')->group(function (){
//        Route::get('','UserMemberController@index')->name('user.member.index');
//        Route::get('/create','UserMemberController@create')->name('user.member.create');
//        Route::post('/store','UserMemberController@store')->name('user.member.store');
//        Route::get('/edit/{id}','UserMemberController@edit')->name('user.member.edit');
//        Route::post('/update/{id}','UserMemberController@update')->name('user.member.update');
//        Route::post('/{id}','UserMemberController@destroy')->name('user.member.destroy');
//    });

        Route::group(['middleware' => ['role:Super Admin']], function () {
    Route::prefix('user/company')->group(function (){
        Route::get('','UserCompanyController@index')->name('user.company.index');
        Route::get('/create','UserCompanyController@create')->name('user.company.create');
        Route::post('/store','UserCompanyController@store')->name('user.company.store');
        Route::get('/show/{id}','UserCompanyController@show')->name('user.company.show');
//        Route::post('/update/{id}','UserCompanyController@update')->name('user.company.update');
        Route::post('/{id}','UserCompanyController@destroy')->name('user.company.destroy');
    });
        });

        Route::group(['middleware' => ['role:Super Admin']], function () {
    Route::prefix('user/individual')->group(function (){
        Route::get('','UserIndividualController@index')->name('user.individual.index');
        Route::get('/create','UserIndividualController@create')->name('user.individual.create');
        Route::post('/store','UserIndividualController@store')->name('user.individual.store');
        Route::get('/edit/{id}','UserIndividualController@show')->name('user.individual.edit');
//        Route::post('/update/{id}','UserCompanyController@update')->name('user.company.update');
        Route::post('/{id}','UserIndividualController@destroy')->name('user.individual.destroy');
    });
        });
        Route::group(['middleware' => ['role:Super Admin|Settings Admin']], function () {
        Route::prefix('analysis')->group(function (){
        Route::get('/users','AnalysisController@userIndex')->name('user.analysis.index');
        Route::get('/orders','AnalysisController@orderIndex')->name('order.analysis.index');
        });
    });

    Route::resource('profile','ProfileController');

//    Route::get('reservation/invoice/{id}',array('as'=>'reservation/invoice','uses'=>'ReservationController@generate_reservation_pdf'));
//    Route::get('order/invoice/{id}',array('as'=>'order/invoice','uses'=>'OrderController@generate_order_pdf'));

    Route::get('reservation/invoice/{id}','ReservationController@generate_reservation_pdf')->name('reservation.invoice.show');

    Route::get('order/invoice/{id}','OrderController@generate_order_pdf')->name('order.invoice.show');


        Route::group(['middleware' => ['role:Super Admin|Settings Admin']], function () {
    Route::prefix('notification')->group(function (){
        Route::get('','NotificationsController@index')->name('notification.index');
        Route::get('/create','NotificationsController@create')->name('notification.create');
        Route::post('/store','NotificationsController@notify')->name('notification.notify');
        Route::get('/edit/{id}','NotificationsController@edit')->name('notification.edit');
        Route::post('/update/{id}','NotificationsController@update')->name('notification.update');
        Route::post('/{id}','NotificationsController@destroy')->name('notification.destroy');
    });
        });
        });

});


