-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 03, 2020 at 09:19 AM
-- Server version: 5.6.47
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hawity_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

CREATE TABLE `about_us` (
  `id` int(10) UNSIGNED NOT NULL,
  `icons` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `about_E` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `about_A` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `about_us`
--

INSERT INTO `about_us` (`id`, `icons`, `about_E`, `about_A`, `created_at`, `updated_at`) VALUES
(1, 'uploads/about/1571735512Group1117.png', 'Application contain two integrated application on smartphones to deal with the target group of (factories - hospitals -\r\nCompanies - warehouses, etc.) To provide container booking service for garbage collection\r\nThe application features a basic control panel for the application manager through which officials can add the description of containers and companies and their addresses\r\nIt also features a sub-control panel for companies characterized by ease\r\nThe advantages of the mobile application containers sent to the client and thus save him a lot of effort and time', 'تطبيق حاويتى  تطبیق متكامل على الھواتف الذكیة للتعامل مع الفئة المستھدفة  من ) مصانع – مستشفیات –\r\nشركات – مستودعات .. الخ ( لتقدیم خدمة حجز الحاویات لجمع القمامة\r\nيتميز التطبيق بلوحه تحكم اساسيه لمدير التطبيق يمكن من خلالها اضافه المسؤلين وصف الحاويات و الشركات و عناوينها\r\nايضا يتميز بلوحه تحكم فرعيه خاصه بلشركات تتميز بسهوله\r\nمن مميزات تطبيق جوال حاويتى ارسالها للعميل و بذلك نوفر له الكثير من الجهد و الوقت', NULL, '2019-10-30 08:40:20');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_E` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_A` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name_E`, `name_A`, `image`, `created_at`, `updated_at`) VALUES
(2, 'waste containers', 'حاويات النفايات', 'uploads/category/15782321285e11e940e2693.Rectangle1.png', NULL, '2020-05-02 07:53:55'),
(3, 'rubble containers', 'حاويات الأنقاض', 'uploads/category/15782321675e11e96759e79.1485.png', NULL, '2020-01-05 11:49:27'),
(4, 'The requests of companies and factories are long contracts', 'طلبات الشركات والمصانع عقود طويلة', 'uploads/category/15782321895e11e97df3fcf.373.png', NULL, '2020-01-29 05:41:23');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name_E` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_A` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name_E`, `name_A`, `created_at`, `updated_at`) VALUES
(1, 'Riyadh', 'الرياض', NULL, NULL),
(2, 'Jeddah', 'جدة', NULL, NULL),
(3, 'Mecca', 'مكة', NULL, NULL),
(4, 'Madina El Monawara', 'المدينة المنورة', NULL, NULL),
(5, 'Hasa', 'الأحساء', NULL, NULL),
(6, 'Taif', 'الطائف', NULL, NULL),
(7, 'Dammam', 'الدمام', NULL, NULL),
(8, 'Buraidah', 'بريدة', NULL, NULL),
(9, 'Tabuk', 'تبوك', NULL, NULL),
(10, 'Qatif', 'القطيف', NULL, NULL),
(11, 'Khamis Mushait', 'خميس مشيط', NULL, NULL),
(12, 'Hail', 'حائل	', NULL, NULL),
(13, 'Hafr Al-Batin', 'حفر الباطن	', NULL, NULL),
(14, 'Jubail', 'الجبيل', NULL, NULL),
(15, 'Kharj', 'الخرج', NULL, NULL),
(16, 'Abha', 'أبها', NULL, NULL),
(17, 'Najran', 'نجران', NULL, NULL),
(18, 'Yanbu', 'ينبع', NULL, NULL),
(19, 'Al Qunfudhah', 'القنفذة', NULL, NULL),
(20, 'Jazan', 'جازان', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `company_sellers`
--

CREATE TABLE `company_sellers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_name_E` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_name_A` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `text_E` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `text_A` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `commercial_register` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax_record` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitude` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `company_sellers`
--

INSERT INTO `company_sellers` (`id`, `icon`, `name`, `company_name_E`, `company_name_A`, `category_id`, `text_E`, `text_A`, `email`, `phone`, `address`, `commercial_register`, `tax_record`, `longitude`, `latitude`, `status`, `created_at`, `updated_at`) VALUES
(1, 'uploads/company/1572423839b4c8431eba2bb4ba6506db4d42821391.png', 'mohy elsharkawy', 'company name', 'اسم الشركة', 2, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق \"ليتراسيت\" (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل \"ألدوس بايج مايكر\" (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.', 'mohy@gmail.com', '2015104', 'san stifano, alexandria', '1234567895', '0123456789102', '31.3655983', '30.1341759', 1, '2019-10-09 02:40:44', '2019-10-30 04:23:59'),
(2, 'uploads/company/1571735046Group 28ي62.png', 'karim ahmed', 'company name', 'اسم الشركة', 3, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق \"ليتراسيت\" (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل \"ألدوس بايج مايكر\" (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.', 'company@gmail.com', '01018472418', 'helwan,cairo', '123456', '456789', '31.3655983', '30.1341759', 1, NULL, '2019-10-22 05:04:06'),
(3, 'uploads/company/157173507211.png', 'saad elmataamy', 'company name', 'اسم الشركة', 4, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق \"ليتراسيت\" (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل \"ألدوس بايج مايكر\" (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.', 'company@gmail.com', '01018472418', 'helwan,cairo', '123456', '456789', '31.3655983', '30.1341759', 1, NULL, '2019-10-22 05:04:32'),
(4, 'uploads/company/157173509011.png', 'farid khamis', 'company', 'شركة', 4, 'default', 'default', 'faris@gmail.com', '2973330955', 'منشية ناصر', '666666', '5000000', '31.3655983', '30.1341759', 0, '2019-10-14 08:40:17', '2019-10-22 05:04:50'),
(5, 'uploads/company/157173510511.png', 'farid khamis', 'company', 'شركة', 4, 'default', 'default', 'faris@gmail.com', '2973330955', 'منشية ناصر', '666666', '5000000', '31.3655983', '30.1341759', 0, '2019-10-15 03:25:54', '2019-10-22 05:05:05'),
(6, 'uploads/company/157173512411.png', 'farid khamis', 'company', 'شركة', 3, 'default', 'default', 'faris@gmail.com', '2973330955', 'منشية ناصر', '666666', '5000000', '31.3655983', '30.1341759', 0, '2019-10-16 04:54:38', '2019-10-22 05:05:24'),
(7, 'uploads/company/1571735021b4c8431eba2bb4ba6506db4d42821391.png', 'ahmed kamel ahmed', 'germany company', 'الشركة الالمانية', 4, 'sdsaadsa', 'ٍِ]ششيسيسشسي', 'germany_company@gmail.com', '2556699', '10 of october', '666666', '666666', '31.3655983', '30.1341759', 0, '2019-10-16 10:30:11', '2019-10-22 05:03:41'),
(8, 'uploads/company/1571735145b4c8431eba2bb4ba6506db4d42821391.png', 'farid khamis', 'company', 'شركة', 4, 'default', 'default', 'faris@gmail.com', '2973330955', 'منشية ناصر', '666666', '5000000', '31.3655983', '30.1341759', 0, '2019-10-20 02:32:38', '2019-10-22 05:05:45'),
(9, 'uploads/company/1571735158b4c8431eba2bb4ba6506db4d42821391.png', 'farid khamis', 'company', 'شركة', 3, 'default', 'default', 'faris@gmail.com', '2973330955', 'منشية ناصر', '666666', '5000000', '31.3655983', '30.1341759', 0, '2019-10-20 02:32:56', '2019-10-22 05:05:58'),
(10, 'uploads/company/1571735171Group 28ي62.png', 'farid khamis', 'company', 'شركة', 3, 'default', 'default', 'faris@gmail.com', '2973330955', 'منشية ناصر', '666666', '5000000', '31.3655983', '30.1341759', 0, '2019-10-20 09:46:46', '2019-10-28 10:10:14'),
(11, 'default.png', 'farid khamis', 'company', 'شركة', 4, 'default', 'default', 'faris@gmail.com', '2973330955', 'منشية ناصر', '666666', '5000000', '31.3655983', '30.1341759', 0, '2019-10-27 03:36:26', '2019-10-27 03:36:26'),
(12, 'default.png', 'farid khamis', 'company', 'شركة', 4, 'default', 'default', 'faris@gmail.com', '2973330955', 'منشية ناصر', '666666', '5000000', '31.3655983', '30.1341759', 0, '2019-10-27 03:57:33', '2019-10-27 03:57:33'),
(13, 'default.png', 'farid khamis', 'company', 'شركة', 4, 'default', 'default', 'faris@gmail.com', '2973330955', 'منشية ناصر', '666666', '5000000', '31.3655983', '30.1341759', 0, '2019-10-27 04:14:08', '2019-10-27 04:14:08'),
(14, 'default.png', 'farid khamis', 'company', 'شركة', 4, 'default', 'default', 'faris@gmail.com', '2973330955', 'منشية ناصر', '666666', '5000000', '31.3655983', '30.1341759', 0, '2019-10-27 04:15:05', '2019-10-27 04:15:05'),
(15, 'default.png', 'farid khamis', 'company', 'شركة', 4, 'default', 'default', 'faris@gmail.com', '2973330955', 'منشية ناصر', '666666', '5000000', '31.3655983', '30.1341759', 0, '2019-10-27 04:17:36', '2019-10-27 04:17:36'),
(16, 'default.png', 'hrbdbd', 'sgdgbs', 'sgdgbs', 4, 'default', 'default', 'svvs@bdbdnvdbd.dvsdv', '59494994', 'Adrar Province, Algeria\nAlgeria', '545464', '548494', '31.3655983', '30.1341759', 0, '2019-10-27 10:01:32', '2019-10-27 10:01:32'),
(17, 'default.png', 'ehhddh', 'dvgdgd', 'dvgdgd', 4, 'default', 'default', 'fasf@gssg.ff', '59949494', 'Adrar Province, Algeria\nAlgeria', '848448', '978484', '31.3655983', '30.1341759', 0, '2019-10-27 10:03:17', '2019-10-27 10:03:17'),
(18, 'default.png', 'asmaa mahmoud', 'grand', 'grand', 4, 'default', 'default', 'asmaaa@admin.com', '01020162705', 'Adrar Province, Algeria\nAlgeria', '1555567', '2558085', '31.3655983', '30.1341759', 0, '2019-10-28 07:26:32', '2019-10-28 07:26:32'),
(19, 'default.png', 'asmaa mahmoud', 'grand', 'grand', 4, 'default', 'default', 'asmaa@admin.com', '0101214151', 'Adrar Province, Algeria\nAlgeria', '123666', '1258008', '31.3655983', '30.1341759', 0, '2019-10-28 07:28:55', '2019-10-28 07:28:55'),
(20, 'default.png', 'asmaa', '2grand', '2grand', 4, 'default', 'default', 'asmaaaa@admin.com', '01014401966', 'Adrar Province, Algeria\nAlgeria', '99877655', '112234456', '31.3655983', '30.1341759', 0, '2019-10-28 09:07:53', '2019-10-28 09:07:53'),
(21, 'uploads/company/1572360237b4c8431eba2bb4ba6506db4d42821391.png', 'شركة كايد الإنجاز للتخلص من النفايات الخطرة', 'Kayed Al Enjaz Company for Hazardous Waste Disposal', 'شركة كايد الإنجاز للتخلص من النفايات الخطرة', 2, 'Health and environmental waste', 'النفايات الصحية والبيئية', 'admin@admin.com', '03/8823439', 'Saudi', '4', '252522', '31.3655983', '30.1341759', 0, '2019-10-29 10:41:27', '2019-10-29 10:43:57'),
(22, 'uploads/company/1572360604b4c8431eba2bb4ba6506db4d42821391.png', 'الشركه السعوديه', 'Saudi Gulf Environmental Protection Company', 'الشركة السعودية الخليجية لحماية البيئة', 2, 'النفايات الطبية', 'Medical waste', 'admin@admin.com', '5976973/02', 'Saudi', '4', '2255566', '31.3655983', '30.1341759', 0, '2019-10-29 10:50:04', '2019-10-29 10:50:04'),
(23, 'uploads/company/1572360861Group 28ي62.png', 'الشركة الخليجية', 'Gulf Waste Treatment Co', 'الشركة الخليجية لمعالجه النفايات', 3, 'Health and environmental waste', 'النفايات الصحية والبيئية', 'admin@admin.com', '014607036', 'Saudi', '-3', '225566', '31.3655983', '30.1341759', 0, '2019-10-29 10:54:21', '2019-10-29 10:54:21'),
(24, 'uploads/company/157236139111.png', 'International Company', 'International Petroleum Waste Works Co', 'الشركة الدولية لأعمال النفايات البترولية', 2, 'Petroleum waste', 'النفايات البترولية', 'admin@admin.com', '026983159', 'Saudi', '-3', '554466', '31.3655983', '30.1341759', 0, '2019-10-29 11:03:11', '2019-10-29 11:03:11'),
(25, 'uploads/company/157236180811.png', 'الشركة السعودية', 'Saudi Paper Recycling Company', 'الشركة السعودية لإعادة تدوير الورق', 2, 'Paper waste', 'النفايات الورقية', 'admin@admin.com', '4626888-1-966', 'Saudi', '4', '556644', '31.3655983', '30.1341759', 1, '2019-10-29 11:10:08', '2019-10-29 11:10:08'),
(26, 'default.png', 'dgdggd', 'bxbdvd', 'bxbdvd', 4, 'default', 'default', 'zcvss@fsvdg.hjd', '64649595', 'Location is added', '54545445', '49644545', '31.3655983', '30.1341759', 0, '2019-10-29 11:10:56', '2019-10-29 11:10:56'),
(27, 'uploads/company/157236211611.png', 'Saad Company', 'Saad Company', 'شركة سعد', 3, 'Medical waste', 'النفايات الطبية', 'admin@admin.com', '8682190', 'Saudi', '3', '556644', '31.3655983', '30.1341759', 1, '2019-10-29 11:15:16', '2019-10-29 11:15:16'),
(29, 'default.png', 'asmaaaa', 'granddd', 'granddd', 4, 'default', 'default', 'asmaa2019@gmail.com', '01122334455', 'Location is added', '1234564', '1234568', '31.3655983', '30.1341759', 0, '2019-10-30 02:50:26', '2019-10-30 02:50:26'),
(30, 'uploads/company/1572424124Group 28ي62.png', 'Saudi Company', 'Saudi Environmental Works Co. Ltd.', 'الشركة السعودية لأعمال البيئة المحدودة', 3, 'Medical waste', 'النفايات الطبية', 'admin@admin.com', '0558898765', 'Saudi', '225566', '1', '31.3655983', '30.1341759', 0, '2019-10-30 04:28:44', '2019-10-30 04:28:44'),
(31, 'default.png', 'amaal', 'company', 'AM COMPANY', 4, 'default', 'default', 'am@gmail.com', '01254848488868', 'Location is added', '55555', '5555', '31.3655983', '30.1341759', 0, '2019-10-30 05:01:31', '2019-10-30 05:07:16'),
(32, 'default.png', 'khaled abo taeya', 'spanish company', 'الشركة الاسبانية', 2, 'default', 'default', 'spanish_company@mail.com', '5000600', 'hashem elasher st', '5000600', '5000600', '31.3655983', '30.1341759', 0, '2019-10-30 05:51:42', '2019-10-30 05:51:42'),
(33, 'default.png', 'asmaa', 'grandd', 'grandd', 4, 'default', 'default', 'asmaa2019@admin.com', '0111223344555', 'تمت إضافة الموقع', '25698085', '0852', '31.3655983', '30.1341759', 0, '2019-10-30 08:29:11', '2019-10-30 08:29:11'),
(34, 'default.png', 'hawity', 'hawity', 'hawity', 4, 'default', 'default', 'hawity@admin.com', '01014401966', 'تمت إضافة الموقع', '252525', '262626', '31.3655983', '30.1341759', 0, '2019-10-30 09:08:38', '2019-10-30 09:08:38'),
(35, 'default.png', 'asmaa grand', 'asmaagrand', 'asmaagrand', 4, 'default', 'default', 'asmaagrang44@admin.com', '01020162708', 'تمت إضافة الموقع', '3690', '1258', '31.3655983', '30.1341759', 0, '2019-11-12 10:13:52', '2019-11-12 10:13:52'),
(36, 'uploads/company/1575878047Chrysanthemum.jpg', 'atef company', 'sca', 'ac', 2, 'sdcsadsa', 'adcsad', 'aa@gmail.com', '0122225555', 'النزهه', '54545', '6341641', '31.3655983', '30.1341759', 0, '2019-12-09 03:54:07', '2019-12-09 03:54:07'),
(37, 'default.png', 'qqqq', 'hdjkdk', 'hdjkdk', 4, 'default', 'default', 'alispm2010@gmail.com', '0540020169', 'تمت إضافة الموقع', '101085458', '300028488', '31.3655983', '30.1341759', 0, '2019-12-16 12:38:38', '2019-12-16 12:38:38'),
(38, 'uploads/company/1577186484Tulips.jpg', 'allaa', 'ihi', 'rrrd', 2, 'fdgfg', 'yfyfyfuy', 'f@gmail.com', '01161641616', 'هاشم الاشقر النزهه الجديده', '4', '5', '31.3655983', '30.1341759', 0, '2019-12-24 09:21:24', '2019-12-24 09:27:02'),
(40, 'uploads/company/1577943432Chrysanthemum.jpg', 'alaa', 'amaaal', 'alaa', 2, 'تيست', 'test', 'alaaamal@gmail.com', '966123123', 'مستشفي النزهة الدولي', '1', '9', '30.1127193', '31.3998532', 1, '2020-01-02 03:37:12', '2020-01-02 10:32:26'),
(41, 'uploads/company/15779669365e0ddd587e0f5.م.jpg', 'aklaaa', 'alaa', 'test', 2, 'text', 'textt', 'alaa1996@gmail.com', '01075133333', '3 الرياض', '3', '5', '31.3724293', '30.1223214', 1, '2020-01-02 10:08:56', '2020-01-02 10:21:56'),
(42, 'default.png', 'farid khamis', 'company', 'شركة', 4, 'default', 'default', 'faris@gmail.com', '2973330955', 'منشية ناصر', '666666', '5000000', '0', '0', 0, '2020-01-21 11:17:23', '2020-01-21 11:17:23'),
(43, 'default.png', 'farid khamis', 'company', 'شركة', 4, 'default', 'default', 'faris@gmail.com', '2973330955', 'منشية ناصر', '666666', '5000000', '0', '0', 0, '2020-01-21 11:17:43', '2020-01-21 11:17:43'),
(44, 'default.png', 'hahah', 'jajaj', 'jajaj', 4, 'default', 'default', 'jajaj@hsjaj.com', '979676', 'Location is added', '9766767', '9494994', '0', '0', 0, '2020-01-21 11:37:34', '2020-01-21 11:37:34'),
(45, 'default.png', 'farid khamis', 'company', 'شركة', 4, 'default', 'default', 'faris@gmail.com', '2973330955', 'منشية ناصر', '666666', '5000000', '0', '0', 0, '2020-01-21 11:40:37', '2020-01-21 11:40:37'),
(46, 'default.png', 'farid khamis', 'company', 'شركة', 4, 'default', 'default', 'faris@gmail.com', '2973330955', 'منشية ناصر', '666666', '5000000', '0', '0', 0, '2020-01-21 11:42:52', '2020-01-21 11:42:52'),
(47, 'default.png', 'farid khamis', 'company', 'شركة', 3, 'default', 'default', 'faris@gmail.com', '2973330955', 'منشية ناصر', '666666', '5000000', '0', '0', 0, '2020-01-21 11:42:56', '2020-01-21 11:42:56'),
(48, 'default.png', 'bvhjjb', 'gugdf', 'gugdf', 4, 'default', 'default', 'fghu@ghj.com', '5598', 'hjgghjk', '54566', '5585', '0', '0', 0, '2020-01-21 12:05:53', '2020-01-21 12:05:53'),
(49, 'default.png', 'alaa', 'alaa', 'alaa', 4, 'default', 'default', 'alaa@gmail.com', '05123', '3 شارع', '١٢٣٤٥٦', '١٢٣', '0', '0', 0, '2020-01-28 12:26:02', '2020-01-28 12:26:02'),
(50, 'default.png', 'Alaa', 'Alaa', 'Alaa', 4, 'default', 'default', 'Amaal@gmail.com', '05123979', 'تمت إضافة الموقع', '123', '123456', '0', '0', 0, '2020-01-28 12:26:32', '2020-01-28 12:26:32'),
(51, 'default.png', 'alaa', 'company', 'company', 4, 'default', 'default', 'alaahamza1996@gmail.com', '011111111', 'test', '1', '1', '0', '0', 0, '2020-01-29 04:41:38', '2020-01-29 04:41:38');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2019_10_08_092511_create_categories_table', 1),
(3, '2019_10_08_095203_create_privacy_table', 1),
(4, '2019_10_08_095628_create_about_us_table', 1),
(5, '2019_12_15_113723_create_cities_table', 1),
(6, '2019_12_15_114021_create_users_table', 1),
(7, '2019_12_15_114808_create_company_sellers_table', 1),
(8, '2019_12_15_114919_create_waste_containers_table', 1),
(9, '2019_12_15_115118_create_reservations_table', 1),
(10, '2019_12_15_115734_create_take_down_containers_table', 1),
(11, '2019_12_15_115857_create_suggestions_table', 1),
(12, '2019_12_15_120012_create_notifications_table', 1),
(13, '2019_12_15_120127_create_order_details_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Group 1188.png',
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notification` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `container_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `company_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`id`, `order_id`, `container_id`, `user_id`, `company_id`, `created_at`, `updated_at`) VALUES
(1, 1, 18, 142, 1, '2020-01-26 06:56:29', '2020-01-26 06:56:29'),
(2, 2, 1, 142, 1, '2020-01-26 07:36:10', '2020-01-26 07:36:10'),
(6, 4, 2, 142, 2, '2020-01-27 05:17:54', '2020-01-27 05:17:54'),
(7, 4, 2, 142, 2, '2020-01-27 05:18:03', '2020-01-27 05:18:03'),
(8, 4, 1, 142, 1, '2020-01-27 05:19:13', '2020-01-27 05:19:13'),
(9, 4, 16, 142, 40, '2020-01-27 05:23:12', '2020-01-27 05:23:12'),
(10, 4, 16, 142, 40, '2020-01-27 05:23:40', '2020-01-27 05:23:40'),
(11, 4, 2, 142, 2, '2020-01-27 06:13:58', '2020-01-27 06:13:58'),
(13, 6, 9, 142, 25, '2020-01-27 06:22:23', '2020-01-27 06:22:23'),
(14, 7, 1, 142, 1, '2020-01-27 08:03:06', '2020-01-27 08:03:06'),
(15, 8, 1, 142, 1, '2020-01-27 11:44:21', '2020-01-27 11:44:21'),
(16, 8, 1, 142, 1, '2020-01-27 11:49:35', '2020-01-27 11:49:35'),
(17, 9, 6, 142, 25, '2020-01-27 13:00:42', '2020-01-27 13:00:42'),
(18, 10, 1, 142, 1, '2020-01-27 13:13:26', '2020-01-27 13:13:26'),
(19, 10, 8, 142, 27, '2020-01-28 09:06:29', '2020-01-28 09:06:29'),
(20, 11, 18, 142, 1, '2020-01-28 10:51:42', '2020-01-28 10:51:42'),
(21, 12, 6, 199, 25, '2020-01-28 11:47:58', '2020-01-28 11:47:58'),
(22, 11, 2, 142, 2, '2020-01-28 11:48:13', '2020-01-28 11:48:13'),
(23, 11, 17, 142, 41, '2020-01-28 11:52:34', '2020-01-28 11:52:34'),
(24, 13, 16, 199, 40, '2020-01-28 12:04:41', '2020-01-28 12:04:41'),
(25, 14, 16, 142, 40, '2020-01-28 13:15:25', '2020-01-28 13:15:25'),
(26, 15, 1, 142, 1, '2020-01-28 13:35:46', '2020-01-28 13:35:46'),
(27, 13, 1, 199, 1, '2020-01-29 05:57:51', '2020-01-29 05:57:51'),
(28, 16, 1, 199, 1, '2020-01-29 07:21:41', '2020-01-29 07:21:41'),
(29, 17, 6, 199, 25, '2020-01-30 14:17:28', '2020-01-30 14:17:28'),
(30, 18, 2, 203, 2, '2020-01-30 19:16:21', '2020-01-30 19:16:21'),
(31, 19, 3, 203, 3, '2020-01-30 19:17:50', '2020-01-30 19:17:50'),
(32, 20, 1, 199, 1, '2020-02-03 11:59:51', '2020-02-03 11:59:51'),
(33, 15, 1, 142, 1, '2020-02-05 11:43:24', '2020-02-05 11:43:24'),
(34, 15, 1, 142, 1, '2020-02-05 11:43:33', '2020-02-05 11:43:33'),
(35, 15, 18, 142, 1, '2020-02-05 11:45:44', '2020-02-05 11:45:44'),
(36, 21, 2, 142, 2, '2020-02-05 11:47:32', '2020-02-05 11:47:32'),
(37, 22, 1, 142, 1, '2020-02-05 11:49:48', '2020-02-05 11:49:48'),
(38, 22, 1, 142, 1, '2020-02-06 09:15:23', '2020-02-06 09:15:23'),
(39, 22, 1, 142, 1, '2020-02-06 09:16:55', '2020-02-06 09:16:55'),
(40, 22, 8, 142, 27, '2020-02-06 09:18:09', '2020-02-06 09:18:09'),
(41, 22, 2, 142, 2, '2020-02-06 09:18:16', '2020-02-06 09:18:16'),
(42, 19, 2, 203, 2, '2020-02-06 13:19:54', '2020-02-06 13:19:54'),
(43, 19, 2, 203, 2, '2020-02-06 13:21:46', '2020-02-06 13:21:46'),
(44, 22, 6, 142, 25, '2020-02-09 05:16:48', '2020-02-09 05:16:48'),
(45, 23, 18, 142, 1, '2020-02-09 05:17:39', '2020-02-09 05:17:39'),
(46, 22, 1, 142, 1, '2020-02-09 05:51:37', '2020-02-09 05:51:37'),
(47, 24, 2, 142, 2, '2020-02-16 06:41:35', '2020-02-16 06:41:35'),
(48, 25, 2, 142, 2, '2020-02-16 06:48:56', '2020-02-16 06:48:56'),
(49, 25, 2, 142, 2, '2020-02-16 06:50:24', '2020-02-16 06:50:24'),
(50, 25, 2, 142, 2, '2020-02-16 06:52:14', '2020-02-16 06:52:14'),
(51, 26, 2, 142, 2, '2020-02-16 06:58:53', '2020-02-16 06:58:53'),
(52, 27, 2, 142, 2, '2020-02-16 07:00:44', '2020-02-16 07:00:44'),
(53, 28, 2, 142, 2, '2020-02-17 04:08:52', '2020-02-17 04:08:52'),
(54, 28, 2, 142, 2, '2020-02-17 04:21:27', '2020-02-17 04:21:27'),
(55, 29, 2, 147, 2, '2020-02-17 06:24:46', '2020-02-17 06:24:46'),
(56, 30, 1, 147, 1, '2020-02-17 06:25:51', '2020-02-17 06:25:51'),
(57, 31, 2, 147, 2, '2020-02-17 06:31:02', '2020-02-17 06:31:02'),
(58, 31, 2, 147, 2, '2020-02-17 06:32:31', '2020-02-17 06:32:31'),
(59, 32, 2, 147, 2, '2020-02-17 08:50:00', '2020-02-17 08:50:00'),
(60, 33, 2, 142, 2, '2020-02-18 07:36:57', '2020-02-18 07:36:57'),
(61, 34, 2, 147, 2, '2020-02-18 08:10:16', '2020-02-18 08:10:16'),
(62, 33, 2, 142, 2, '2020-02-19 04:12:29', '2020-02-19 04:12:29'),
(63, 34, 2, 147, 2, '2020-02-19 05:40:49', '2020-02-19 05:40:49'),
(64, 34, 2, 147, 2, '2020-02-19 05:50:55', '2020-02-19 05:50:55'),
(65, 34, 2, 147, 2, '2020-02-19 19:20:53', '2020-02-19 19:20:53'),
(66, 35, 3, 147, 3, '2020-02-19 19:21:57', '2020-02-19 19:21:57'),
(67, 36, 3, 147, 3, '2020-02-19 19:22:52', '2020-02-19 19:22:52'),
(68, 36, 2, 147, 2, '2020-02-19 19:24:47', '2020-02-19 19:24:47'),
(69, 36, 3, 147, 3, '2020-02-22 06:06:58', '2020-02-22 06:06:58'),
(70, 37, 3, 147, 3, '2020-03-01 06:33:22', '2020-03-01 06:33:22'),
(71, 38, 2, 147, 2, '2020-03-01 06:34:39', '2020-03-01 06:34:39'),
(72, 38, 2, 147, 2, '2020-03-01 06:35:42', '2020-03-01 06:35:42'),
(73, 33, 2, 142, 2, '2020-04-02 07:32:00', '2020-04-02 07:32:00'),
(74, 33, 2, 142, 2, '2020-04-12 09:51:47', '2020-04-12 09:51:47'),
(75, 33, 2, 142, 2, '2020-04-12 09:52:10', '2020-04-12 09:52:10'),
(76, 39, 3, 142, 3, '2020-04-13 07:47:55', '2020-04-13 07:47:55'),
(77, 40, 2, 142, 2, '2020-04-13 08:11:56', '2020-04-13 08:11:56'),
(78, 40, 3, 142, 3, '2020-04-13 08:12:56', '2020-04-13 08:12:56'),
(79, 41, 2, 147, 2, '2020-04-23 17:19:52', '2020-04-23 17:19:52'),
(80, 41, 2, 147, 2, '2020-04-28 09:14:19', '2020-04-28 09:14:19'),
(81, 42, 2, 142, 2, '2020-04-29 06:06:41', '2020-04-29 06:06:41'),
(82, 41, 2, 147, 2, '2020-04-29 12:19:23', '2020-04-29 12:19:23'),
(83, 41, 2, 147, 2, '2020-04-29 12:19:24', '2020-04-29 12:19:24');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `privacy`
--

CREATE TABLE `privacy` (
  `id` int(10) UNSIGNED NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text_E` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `text_A` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `privacy`
--

INSERT INTO `privacy` (`id`, `icon`, `text_E`, `text_A`, `created_at`, `updated_at`) VALUES
(1, 'uploads/privacy/1571735552shield-with-lock.png', 'Privacy and security\r\nMy Container respects your privacy and appreciates your concern about the way we handle and share your personal information. We value your trust in us and always protect the privacy of your data (individuals - companies) while using full encryption to protect your transmitted data.\r\n\r\nWe hereby state in the terms of this Privacy Policy, the information we collect and what you use, and the disclosures of some of your data. You may require regulated disclosure of your information to third parties who may process this information on our behalf, among other cases.', 'الخصوصية والأمان\r\nيحترم تطبيق حاويتى خصوصيتك، ويقدر قلقك بشأن طريقة التعامل مع المعلومات الشخصية الخاصة بك ومشاركتها. نحن نقدر ثقتك بنا ونعمل دائمًا على حماية خصوصية البيانات الخاصة بك ( الأفراد – الشركات) مع استخدام التشفير التام لحماية البيانات المرسلة.\r\n\r\nلذا نوضح في بنود سياسة الخصوصية هذه، المعلومات التي نقوم بجمعها وفيما تستخدم، وحالات الإفصاح عن بعض البيانات الخاصة بك، لذا يجب العلم أنه بموجب هذا الاتفاق فأنت توافق على احتفاظنا بمعلومات التسجيل الخاصة بك ولا تمانع في استخدام هذه البيانات بغرض تحسين الخدمة وتسيير المعاملات التي قد تتطلب الإفصاح المقنن عن المعلومات الخاصة بك لأطراف ثالثة قد تقوم بمعالجة هذه المعلومات بالنيابة عنا، إلى جانب الحالات الأخرى.', NULL, '2019-10-29 09:46:03');

-- --------------------------------------------------------

--
-- Table structure for table `reservations`
--

CREATE TABLE `reservations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `container_id` bigint(20) UNSIGNED NOT NULL,
  `company_id` bigint(20) UNSIGNED NOT NULL,
  `order_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_from` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_to` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `payment_info` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `days` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price_per_day` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_empty` tinyint(1) NOT NULL DEFAULT '0',
  `baskets` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reservations`
--

INSERT INTO `reservations` (`id`, `icon`, `user_id`, `container_id`, `company_id`, `order_number`, `date_from`, `date_to`, `total`, `status`, `payment_info`, `days`, `price_per_day`, `is_empty`, `baskets`, `created_at`, `updated_at`) VALUES
(1, '', 142, 18, 1, '17872', '31-1-2020', '01-02-2020', '150', 1, '0', '1', '', 1, '6', '2020-01-26 06:56:29', '2020-01-26 07:31:32'),
(2, '', 142, 1, 1, '95466', '30-1-2020', '31-01-2020', '150', 1, '0', '1', '', 1, '4', '2020-01-26 07:36:10', '2020-01-26 07:50:18'),
(4, '', 142, 2, 2, '60166', '31-1-2020', '01-02-2020', '40', 1, '0', '1', '', 0, '1', '2020-01-27 05:17:54', '2020-01-27 06:16:45'),
(6, '', 142, 9, 25, '70186', '27-02-2020', '28-02-2020', '0', 1, '0', '1', '', 0, '5', '2020-01-27 06:22:23', '2020-01-27 11:12:08'),
(7, '', 142, 1, 1, '29760', '27-03-2020', '29-03-2020', '0', 1, '0', '2', '', 0, '5', '2020-01-27 08:03:06', '2020-01-27 11:40:11'),
(8, '', 142, 1, 1, '71522', '29-1-2020', '30-01-2020', '150', 1, '0', '1', '', 0, '5', '2020-01-27 11:44:21', '2020-01-27 11:51:25'),
(9, '', 142, 6, 25, '41737', '13-02-2020', '14-02-2020', '150', 1, '0', '1', '', 0, '9', '2020-01-27 13:00:42', '2020-01-27 13:02:03'),
(10, '', 142, 1, 1, '51832', '28-01-2020', '29-01-2020', '150', 1, '0', '1', '', 0, '5', '2020-01-27 13:13:26', '2020-01-28 10:51:14'),
(11, '', 142, 18, 1, '86832', '28-02-2020', '29-02-2020', '150', 1, '0', '1', '', 0, '5', '2020-01-28 10:51:42', '2020-01-28 11:54:07'),
(12, '', 199, 6, 25, '41131', '28-02-2020', '29-02-2020', '150', 1, '0', '1', '', 1, '3', '2020-01-28 11:47:58', '2020-01-28 12:30:30'),
(13, '', 199, 16, 40, '83213', '28-01-2020', '29-01-2020', '900', 1, '0', '1', '', 0, '1', '2020-01-28 12:04:41', '2020-01-29 05:58:57'),
(14, '', 142, 16, 40, '23678', '٢٨-٠٢-٢٠٢٠', '01-01-1970', '900', 1, '0', '1', '', 0, '5', '2020-01-28 13:15:25', '2020-01-28 13:35:17'),
(15, '', 142, 1, 1, '23495', '٢٨-٠٢-٢٠٢٠', '01-01-1970', '150', 1, '0', '1', '', 0, '5', '2020-01-28 13:35:46', '2020-02-05 11:46:32'),
(16, '', 199, 1, 1, '87153', '29-02-2020', '01-03-2020', '150', 1, '0', '1', '', 0, '1', '2020-01-29 07:21:41', '2020-01-29 07:22:23'),
(17, '', 199, 6, 25, '79057', '31-01-2020', '01-02-2020', '0', 1, '0', '1', '', 0, '3', '2020-01-30 14:17:28', '2020-01-30 14:18:18'),
(18, '', 203, 2, 2, '79485', '31-1-2020', '10-02-2020', '400', 1, '0', '10', '', 0, '1', '2020-01-30 19:16:21', '2020-01-30 19:16:31'),
(19, '', 203, 3, 3, '92039', '31-1-2020', '10-02-2020', '300', 1, '0', '10', '', 0, '1', '2020-01-30 19:17:50', '2020-02-06 13:22:31'),
(20, '', 199, 1, 1, '15863', '05-02-2020', '06-02-2020', '150', 1, '0', '1', '', 0, '3', '2020-02-03 11:59:51', '2020-02-03 12:00:13'),
(21, '', 142, 2, 2, '71757', '23-2-2020', '24-02-2020', '40', 1, '0', '1', '', 0, '8', '2020-02-05 11:47:32', '2020-02-05 11:48:03'),
(22, '', 142, 1, 1, '40929', '16-3-2020', '17-03-2020', '150', 1, '0', '1', '', 0, '6', '2020-02-05 11:49:48', '2020-02-09 05:51:59'),
(23, '', 142, 18, 1, '27350', '10-02-2020', '11-02-2020', '150', 1, '0', '1', '', 0, '٣', '2020-02-09 05:17:39', '2020-02-09 05:51:59'),
(24, '', 142, 2, 2, '68240', '16-2-2020', '17-02-2020', '40', 1, '0', '1', '', 0, '1', '2020-02-16 06:41:35', '2020-02-16 06:41:57'),
(25, '', 142, 2, 2, '86566', '29-2-2020', '03-03-2020', '120', 1, '0', '3', '', 0, '4', '2020-02-16 06:48:56', '2020-02-16 06:52:19'),
(26, '', 142, 2, 2, '64526', '29-2-2020', '02-03-2020', '80', 1, '0', '2', '', 0, '4', '2020-02-16 06:58:53', '2020-02-16 06:58:57'),
(27, '', 142, 2, 2, '78371', '29-2-2020', '03-03-2020', '120', 1, '0', '3', '', 0, '5', '2020-02-16 07:00:44', '2020-02-16 07:00:49'),
(28, '', 142, 2, 2, '75634', '17-2-2020', '18-02-2020', '40', 1, '0', '1', '', 0, '1', '2020-02-17 04:08:52', '2020-02-17 04:21:32'),
(29, '', 147, 2, 2, '34889', '29-2-2020', '29-02-2020', '0', 1, '0', '0', '', 0, '1', '2020-02-17 06:24:46', '2020-02-17 06:24:53'),
(30, '', 147, 1, 1, '42585', '29-2-2020', '01-03-2020', '150', 1, '0', '1', '', 1, '1', '2020-02-17 06:25:51', '2020-03-01 06:34:59'),
(31, '', 147, 2, 2, '81819', '29-2-2020', '01-03-2020', '40', 1, '0', '1', '', 0, '1', '2020-02-17 06:31:02', '2020-02-17 06:32:42'),
(32, '', 147, 2, 2, '20859', '29-2-2020', '01-03-2020', '40', 1, '0', '1', '', 0, '1', '2020-02-17 08:50:00', '2020-02-17 08:50:34'),
(33, '', 142, 2, 2, '44824', '29-2-2020', '03-03-2020', '120', 1, '0', '3', '', 0, '4', '2020-02-18 07:36:57', '2020-04-12 09:52:42'),
(34, '', 147, 2, 2, '33739', '29-2-2020', '01-03-2020', '40', 1, '0', '1', '', 0, '2', '2020-02-18 08:10:16', '2020-02-19 19:21:04'),
(35, '', 147, 3, 3, '87840', '29-2-2020', '10-03-2020', '300', 1, '0', '10', '', 0, '1', '2020-02-19 19:21:57', '2020-02-19 19:22:04'),
(36, '', 147, 3, 3, '81821', '29-2-2020', '10-03-2020', '300', 1, '0', '10', '', 0, '1', '2020-02-19 19:22:52', '2020-02-22 06:07:24'),
(37, '', 147, 3, 3, '30324', '31-3-2020', '10-04-2020', '300', 1, '0', '10', '', 0, '1', '2020-03-01 06:33:22', '2020-03-01 06:33:28'),
(38, '', 147, 2, 2, '23595', '31-3-2020', '31-03-2020', '0', 1, '0', '0', '', 0, '5', '2020-03-01 06:34:39', '2020-03-01 06:35:48'),
(39, '', 142, 3, 3, '60113', '30-4-2020', '10-05-2020', '300', 1, '0', '10', '', 0, '2', '2020-04-13 07:47:55', '2020-04-13 07:48:20'),
(40, '', 142, 2, 2, '17879', '30-4-2020', '02-05-2020', '80', 1, '0', '2', '', 0, '2', '2020-04-13 08:11:56', '2020-04-13 08:13:19'),
(41, '', 147, 2, 2, '95993', '30-4-2020', '30-04-2020', '0', 1, '0', '0', '', 0, '1', '2020-04-23 17:19:52', '2020-04-29 12:19:37'),
(42, '', 142, 2, 2, '23362', '29-4-2020', '30-04-2020', '40', 0, '0', '1', '', 0, '1', '2020-04-29 06:06:41', '2020-04-29 06:06:41');

-- --------------------------------------------------------

--
-- Table structure for table `suggestions`
--

CREATE TABLE `suggestions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `suggestion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `suggestions`
--

INSERT INTO `suggestions` (`id`, `user_id`, `title`, `suggestion`, `created_at`, `updated_at`) VALUES
(1, 142, 'alaa', 'ggagj', '2020-01-02 11:23:00', '2020-01-02 11:23:00'),
(2, 142, 'alaa', 'ggagj', '2020-01-02 11:23:02', '2020-01-02 11:23:02'),
(3, 22, 'hi', 'hi every one', '2020-01-19 06:02:01', '2020-01-19 06:02:01'),
(4, 22, 'hi', 'hi every one', '2020-01-19 06:05:36', '2020-01-19 06:05:36'),
(5, 22, 'add', 'add', '2020-01-19 06:11:18', '2020-01-19 06:11:18'),
(6, 22, 'hsjj', 'jsjsjs', '2020-01-22 11:31:04', '2020-01-22 11:31:04'),
(7, 22, 'dfgf', 'hgdd', '2020-01-22 11:47:39', '2020-01-22 11:47:39'),
(8, 22, 'hi', 'hi', '2020-01-27 05:29:23', '2020-01-27 05:29:23'),
(9, 142, 'gdhj', 'excellent yfc', '2020-01-28 12:27:19', '2020-01-28 12:27:19'),
(10, 142, 'gdhj', 'excellent yfc', '2020-01-28 12:27:20', '2020-01-28 12:27:20'),
(11, 142, 'gdhj', 'excellent yfc', '2020-01-28 12:27:22', '2020-01-28 12:27:22'),
(12, 22, 'test', 'test', '2020-01-28 12:27:28', '2020-01-28 12:27:28'),
(13, 22, 'test', 'test', '2020-01-29 04:22:39', '2020-01-29 04:22:39');

-- --------------------------------------------------------

--
-- Table structure for table `take_down_containers`
--

CREATE TABLE `take_down_containers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `container_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `note_E` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `note_A` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `take_down_containers`
--

INSERT INTO `take_down_containers` (`id`, `photo`, `container_id`, `user_id`, `note_E`, `note_A`, `created_at`, `updated_at`) VALUES
(1, '1580139413.jpeg', 1, 0, 'Gujdf', 'Gujdf', '2020-01-27 13:36:53', '2020-01-27 13:36:53'),
(2, '1580139539.jpeg', 1, 0, 'Hyfhj', 'Hyfhj', '2020-01-27 13:38:59', '2020-01-27 13:38:59'),
(3, '1580219478.jpeg', 6, 0, 'Uhhjhh', 'Uhhjhh', '2020-01-28 11:51:18', '2020-01-28 11:51:18'),
(4, '1580284726.jpeg', 16, 0, 'ملاحظة', 'ملاحظة', '2020-01-29 05:58:46', '2020-01-29 05:58:46'),
(5, '1580401071.jpeg', 6, 0, 'Aaa', 'Aaa', '2020-01-30 14:17:51', '2020-01-30 14:17:51');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `commercial_register` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax_record` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_id` bigint(20) UNSIGNED DEFAULT NULL,
  `verify_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_status` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `jwt_token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `firebase_token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `phone`, `user_type`, `image`, `commercial_register`, `tax_record`, `city_id`, `verify_code`, `user_status`, `status`, `jwt_token`, `latitude`, `longitude`, `firebase_token`, `remember_token`, `created_at`, `updated_at`) VALUES
(22, 'andrew', 'andrew@gmail.com', NULL, '$2y$10$M/0lhQDXo3erWzaQnblDM.NPMT7VaBVW0380OiTzyIJHYmj9K6oFm', '110000000000000000', 'individual', '1580287967.jpg', 'default', 'default', 2, '7298', 1, 1, 'GiTA0Oc3JsrHJkgKGWzLxtvHm', 123456, 456789, 'dc80jY-lJYE:APA91bFs3Kc3SpeAzl_wN3gNuo1l1ZtvV0UOuhGVuAKSv-yA8Jb8g2U9xH8KqM4t05RTHw_e8E9DizdFv2DTbHFf-YYmmB583Ubm8J2gWB-iyUaNaGdeyn-2aWS_tQEImscVILua5tVs', NULL, '2019-10-09 02:23:56', '2020-01-29 06:52:47'),
(24, '7azal2om', '7azal2om@gtmail.com', NULL, '$2y$10$LidxxDvgPL1gC5jwcmn0y.JGkbktlGbvsIGqbnc4QRGI8tx9IjiPu', '01011110021', 'company', '', '1111111111111', '2222222222', 1, '6806', 1, 1, 'bXWalSLLN2ABNhYQ5S1Xl7y2I', 29.9905689, 31.2237162, '', NULL, '2019-10-14 08:04:37', '2019-10-14 08:04:37'),
(25, 'karem', 'karem@gmail.com', NULL, '$2y$10$SiV8UxAO51G9dtJH6Fk15OlixgvdTVIrz66TJVJfXzteGyxZPmlsG', '25005006', 'individual', '', 'default', 'default', 1, '4168', 1, 1, 'aeJ4DnjUZjOLvUMAm58cXSRky', 30.099812, 31.2925784, '', NULL, '2019-10-14 08:11:41', '2019-10-20 05:01:32'),
(26, 'Mohamed', 'mohamed@yahoo.com', NULL, '$2y$10$shYdeR2lbYn9onuwDBQZC.fBV8ucSG1CK82laCuI0dHTwRTtWOlVy', '01121805383', 'company', '', '123456789', '123456789', 1, '', 0, 1, '0fnDbiYho5ET3UpiFszAZ8VsP', 29.9905689, 31.2237162, '', NULL, '2019-10-15 03:07:17', '2020-05-02 10:37:02'),
(28, 'karem', 'karem1@gmail.com', NULL, '$2y$10$3BQG2T96KFHMixYMie.GR.jSchAEeGnjoeAYBN1yLDBcc12D7gaJC', '26837585', 'individual', '', 'default', 'default', 1, '6611', 0, 1, 'IjROsnR042OlsUZy958iv67pN', 30.099812, 31.2925784, '', NULL, '2019-10-15 03:11:37', '2019-10-15 03:11:37'),
(29, 'taftaf', 'mohamed@gmail.com', NULL, '$2y$10$M/0lhQDXo3erWzaQnblDM.NPMT7VaBVW0380OiTzyIJHYmj9K6oFm', '266778890', 'individual', '', 'default', 'default', 1, '6115', 1, 1, '08UQjgAHi1HNNM2dji3NhP2vB', 30.099812, 31.2925784, 'dc80jY-lJYE:APA91bFs3Kc3SpeAzl_wN3gNuo1l1ZtvV0UOuhGVuAKSv-yA8Jb8g2U9xH8KqM4t05RTHw_e8E9DizdFv2DTbHFf-YYmmB583Ubm8J2gWB-iyUaNaGdeyn-2aWS_tQEImscVILua5tVs', NULL, '2019-10-15 06:30:47', '2020-01-02 05:18:13'),
(30, 'karem', 'karem11@gmail.com', NULL, '$2y$10$48TgzpNeMCMNYG2KPBx8QeUpZA8QBLYrATtNw/14b1kLebys9lnNO', '011218055555', 'individual', '', 'default', 'default', 1, '4061', 0, 1, 'ut4AftrShwMwHElC0fquW3w66', 30.099812, 31.2925784, '', NULL, '2019-10-15 07:04:51', '2019-10-15 07:04:51'),
(45, 'Mohamed', 'mohsdsdamed@yahoo.com', NULL, '$2y$10$ZrJtnW8KHeNdO.nyDx4SOuBLgrS4q/Y8kc1vw/f2Y9qn6zhTzvrSO', '01132321805383', 'company', '', '123456789', '123456789', 1, '4598', 0, 1, 'z4KiecF3nyHosz4sjyXeE9CTM', 0, 0, '', NULL, '2019-10-23 07:03:28', '2019-10-23 07:03:28'),
(46, 'karem', 'karemsss15@gmail.com', NULL, '$2y$10$UqV3w/I8SoKI1TeeypEmIO7oVtCjC4ZUi30Jh74YGGl2/mngGf7.6', '011213411111111', 'individual', '', 'default', 'default', 1, '7207', 0, 1, '2XYBTPf2rOKB1mnOUvV7d4dZ0', 0, 0, '', NULL, '2019-10-23 07:03:53', '2019-10-23 07:03:53'),
(47, 'karem', 'karemdd@gmail.com', NULL, '$2y$10$uHN7EX4s7LRAvflQCxZSPe0akhmMn7PdZfn82Gc57qEN.4aX/s3F.', '2683337585', 'individual', '', 'default', 'default', 1, '2364', 0, 1, 'tJIdHIM14erPRTh3DcjZOVsbC', 0, 0, '', NULL, '2019-10-23 07:27:30', '2019-10-23 07:27:30'),
(48, 'fkarem', 'fkaremdd@gmail.com', NULL, '$2y$10$R8caNVYOGmIG70UcY.zLeurJ.09lhEvh1i7FrhlLqMkpl4fosFWqu', '22683337585', 'individual', '', 'default', 'default', 1, '', 0, 1, 'XwdPG5TcxqnsspCbze8ETA5hU', 0, 0, '', NULL, '2019-10-23 09:48:27', '2019-10-23 09:49:18'),
(49, 'ahmed', 'ahmedamr@gmail.com', NULL, '$2y$10$Do8MTOwVZH0JxBAt7dKPXemNw2GqjH/jESWzOLobIOlDgiN9fKIdm', '01017196286', 'individual', '1572338229.jpg', 'default', 'default', 1, '', 0, 1, '5gCuUhGUNrq5mLqCqfMKZXcK0', 0, 0, 'd0qDqUTPD1Y:APA91bFB6B329GpSTHXMNDFPUqmP9yQqTr_vULC1huxvcfQdei9cSQAEt6F7Lu6pJXpiuIxgrHp5ty-IFvN-qRn-xEAqTCWqJYeYDKBbmyLtprTY5S0Ictf0ZnhhD8YMLH-_aXM0PFpS', NULL, '2019-10-23 11:05:19', '2019-11-14 05:02:09'),
(97, 'amaal', 'amaal011@gmail.com', NULL, '$2y$10$H5B/uJaeWFsbpfT0jERoaOBq1fCPtPydOy8RZwSYClWbSAgpuliOq', '011011011', 'individual', '', 'default', 'default', 3, '8635', 0, 0, '274lcHeDie6FdiNsbhjkmaYZm', 0, 0, '', NULL, '2019-12-16 04:25:45', '2019-12-16 04:25:45'),
(98, 'amaaldesouky', 'amaaaal011@gmail.com', NULL, '$2y$10$z./j9nxn1UUnfUW8l0JXKe.lKYSLTm3xLWD46Q5q0fAVL/AL4iIVC', '0110110111', 'individual', '1576477601.jpg', 'default', 'default', 3, '', 0, 0, 'D00gLw6UyvSVaFyvm9nUrFZtF', 0, 0, 'cdHNnUgGHHY:APA91bECB4NikygzJYPkvPTraLRZEQPGbLnI3yjrGn3cOJXpGVvAN5kn4kzQDrMqhy_DtcRECvYYL1B-g_4c0JNz2MOnqsvKLqixT92eq8TMdZclwDEjhf9xneiyX7U413_Wdd4Lofxb', NULL, '2019-12-16 04:26:15', '2019-12-16 04:27:30'),
(99, 'admin', 'admin@admin.com', NULL, '$2y$10$DuuAv4viWD9Lcd0SZyQIJOMrjzFfPXwE4kHI1HfucM26/zQz4yBl6', '0', 'admin', '0', '0', '0', 16, '0', 1, 1, '0', 0, 0, '0', 'CF2GysQJoxVpDcVwhJcTsbkBLLSwenfxwI08vBOkYmC0L7e4UvSlZE6J72Jf', NULL, '2020-05-02 07:40:59'),
(132, 'amr', 'ammmr@gmail.com', NULL, '$2y$10$ldAmO.Coa0h2CCIJRaT/KeyrNc6ozuvS7ccFvAlPst/gEU5JtAOxq', '524002015569', 'company', '', '626627', '662727', 1, '1425', 0, 0, 'oftP2Ktd6uJk0MytHET3EtkiR', 0, 0, '', NULL, '2019-12-16 11:18:38', '2019-12-16 11:18:38'),
(133, 'amr', 'ammmmr@gmail.com', NULL, '$2y$10$C1BLbb6rmNzT2kQgvpOUC.EkZdFJ9QZk0hIMEGW1s5bkduEKR.6S2', '5240020155699', 'company', '', '626627', '662727', 1, '6263', 0, 0, 'vm4yJrVWhQPAPnqZIlhRyt8y5', 0, 0, '', NULL, '2019-12-16 11:20:39', '2019-12-16 11:20:39'),
(134, 'asa', 'alialsewed@hotmail.com', NULL, '$2y$10$N8VteClZ2d5psEjN9Ji6w.SXNxCn0fWtJkczDQsTQvgYzK3y4hRai', '532889790', 'individual', '', 'default', 'default', 1, '', 0, 0, 'joVlepGwVsNCtGsnCMk2MtJVS', 0, 0, 'dpynLgeL6xk:APA91bHp_W8WYisShEOwjGtvjfn0hd_5vdIXJkRWHaPwo_8HxRUkSBv14uwr1YVzZJ9eIMVJfD5BKA3YPDfMA1yFfm8j1lSH6Vj61CD4EP6UkJNw4WZTINbLEwhGNXG7vsYQunAvVAQi', NULL, '2019-12-16 12:30:55', '2020-01-14 03:39:06'),
(135, 'amall', 'testttt@gmail.com', NULL, '$2y$10$LbzqVO86sIQ0Gt60KaaAO.q7KAoZe4bmrw6NF3AHw/WLQ6E6uLpfy', '1150115', 'company', '', '3', '7', 7, '6689', 0, 0, 'BjbLYgTb4eNGuQkDcAxDO2rEI', 0, 0, '', NULL, '2019-12-17 06:20:26', '2019-12-17 06:20:26'),
(136, 'amall', 'testtttt@gmail.com', NULL, '$2y$10$FkKdYUWMvfdm8tJXI0JhAeHMTzfBNufaW8FEeFtG7Zf2D3gS8V5DG', '11501155', 'company', '', '3', '7', 7, '', 0, 0, 'y7Xi7ZTvDyIqywm1xTnyqvn6C', 0, 0, 'd1_gKrnU55U:APA91bFFtI_zd8u3h1IDJgbFbqiDloXwMdTo7XlTx47aMDyl_qOBDC8OkZQtwizcAaA-p1JnxVyjuf8ngMuBOBeTYTFiN-cmiLyRnyIo-mn2-24W-5gO9Y5qK1gHQTP3XUzG3YnmtUKS', NULL, '2019-12-17 06:20:42', '2019-12-17 06:20:55'),
(137, 'khalellll', 'khalellll@gmail.com', NULL, '$2y$10$axQHUm//L0.Af.WZ4deHyOPxSEsk53t0NLe.g5q/WBqUWdZgBmiNi', '404040404040', 'individual', '/tmp/phpZ3ITx5', 'default', 'default', 2, '', 1, 1, '0oyYYrNDEQWPYWZe0Am9g77Y7', 0, 0, 'fSpm3okW_so:APA91bGFzxjMGYF3uckSMvdL1Hd65rSe6xVsPorMJD_BNpbqE6IrllDwo0ZgVFEEuAanNwT47WQwyXpWrdrBHWG2WbYZN7WBzqmNyTCOBOQNwcxW5tJFSH5OVFeaZJ0eOc1X7qta-3gY', NULL, '2019-12-17 10:44:59', '2020-01-20 06:26:19'),
(138, 'qwertyui', 'thanksh@gm.com', NULL, '$2y$10$qx1cbXBBI62NBeYq8L8IKe2uEYhQoMMutNtUER3sEOmyfsPQkXGu2', '124578', 'individual', '', 'default', 'default', 6, '', 0, 0, 'orgqGbUAwd32LRHBN5j9coN0v', 0, 0, 'duk0Ym_B_bI:APA91bFG-TzP-fxo0NlnKUvofocFBKYaOCVuRTQIZhU8gT8TCmU2hjDSMOaDH0nqyqNfwNWOuBnaVYhUkzRVg6KYJqMfyTfxXwoeGMpZ6Y1lQYo3z3d7Ywu_oP4IXzCGZbzaQviunrT4', NULL, '2019-12-17 10:47:28', '2019-12-17 10:47:36'),
(139, 'alaa', 'alaaalaa@gmail.com', NULL, '$2y$10$DSeBFN4veC2ayOvxhiwCfeRIBFgkRpHN0HK5VGwRfOE3jjz5z3bsi', '1133333', 'individual', '', 'default', 'default', 2, '9659', 0, 0, '45o8LGVRKtBnRn0qzvsEp682W', 0, 0, '', NULL, '2019-12-18 09:50:36', '2019-12-18 09:50:36'),
(140, 'alaa', 'alaaaalaa@gmail.com', NULL, '$2y$10$alpBT.c9p7M8UnFi/lJsm.W/.JAX088TnYRDoFcUBkKjAxeo6yy7y', '11333333', 'individual', '', 'default', 'default', 2, '4923', 0, 0, '6zf4114cIPOfAUc9QFuyFznxf', 0, 0, '', NULL, '2019-12-18 09:50:57', '2019-12-18 09:50:57'),
(141, 'alaa', 'alaaaaalaa@gmail.com', NULL, '$2y$10$hDMBK9keXQjQ4gqQgs4nRe4FG0CBLcTeaAEQ/xFQCP6ZmDfrKKAqa', '113333333', 'individual', '', 'default', 'default', 2, '2808', 0, 0, 'cN42Uhq73LRuusfki6WIlJtTh', 0, 0, 'dc80jY-lJYE:APA91bFs3Kc3SpeAzl_wN3gNuo1l1ZtvV0UOuhGVuAKSv-yA8Jb8g2U9xH8KqM4t05RTHw_e8E9DizdFv2DTbHFf-YYmmB583Ubm8J2gWB-iyUaNaGdeyn-2aWS_tQEImscVILua5tVs', NULL, '2019-12-18 09:51:26', '2020-01-02 13:25:31'),
(142, 'alaa', 'eslam@gmail.com', NULL, '$2y$10$.V0c61K6EWu4AD6exZYyQueYhviZhuHh4VW6TFDT9s6JjgeAMB4U.', '1111111', 'individual', '1580717836.jpeg', 'default', 'default', 7, '', 0, 0, 'PuHAkyBB23LijLVto7YBYj8ck', 0, 0, 'eQCBb99Ot2Q:APA91bFLX3GOnwhMwqClMNheSOkBsYu8fx6bvE6NxEdVTnM1e43hSn5I-gd2eNMXrNEDYvA6JGxMYTyhwJyLgBeP7lXqMMOL3NnA2ImCbQfY3H-X0FXnvCOjCMm77c2lx3HJsoUCWbVk', NULL, '2019-12-18 09:52:23', '2020-05-02 10:32:20'),
(144, 'alll', 'al@gmail.com', NULL, '$2y$10$TUjdZYhOEFJJMzwnmKbD5uXK.NqmnMegSjDPVZ7HIOcr0wbkAZosS', '577774444', 'individual', '', 'default', 'default', 2, '8670', 0, 0, 'tAdQgqKsswc1xbal3ASEo0Wqk', 0, 0, '', NULL, '2019-12-18 10:45:03', '2019-12-18 10:45:03'),
(145, 'alll', 'all@gmail.com', NULL, '$2y$10$NT7wKGqVuda2GE/5Q9E90.Z0CY7D1rPnx0tF4TJrqlTMnpmUviqVu', '5777744445', 'individual', '', 'default', 'default', 2, '2886', 0, 0, '3PalisPXUxflODfKOUx4BE6lY', 0, 0, '', NULL, '2019-12-18 10:45:27', '2019-12-18 10:45:27'),
(146, 'aas', 'aa@gmail.com', NULL, '$2y$10$E5no8d4tbcc7pwzjjEw.bOWJxYkwMV8EMCIkPXmW/M.eBdnxlrIja', '533331111', 'individual', '', 'default', 'default', 2, '', 0, 0, 'hbG5yX6kS2A7dwgNFEgkugJFm', 0, 0, 'dc80jY-lJYE:APA91bFs3Kc3SpeAzl_wN3gNuo1l1ZtvV0UOuhGVuAKSv-yA8Jb8g2U9xH8KqM4t05RTHw_e8E9DizdFv2DTbHFf-YYmmB583Ubm8J2gWB-iyUaNaGdeyn-2aWS_tQEImscVILua5tVs', NULL, '2019-12-18 10:47:18', '2020-01-02 13:20:07'),
(147, 'ali', 'test@t.com', NULL, '$2y$10$oMwAVd.hYVb5oGYrYD06n.Ivdvb/Bl/iXcYr.cvxth.xwYopVLMGO', '540020169', 'individual', '', 'default', 'default', 1, '', 0, 0, 'acmUNS2FUgEWq4EmXppC01X4O', 0, 0, 'fElsqKWs8Xo:APA91bGvF4BZkk-nUJuytnG4AzJSnx97Bu5lJhtW6pI1k0FUx-pLpvEZqnuj4No0M8zKDk2yh7SZsiBF-MjtD6CJfiKRbUbiYvzwmk6yf3WgF62k26jMB4EFjq3GxElHYy_oniZZHsvD', NULL, '2019-12-18 11:05:24', '2020-04-29 12:18:37'),
(154, 'elshaloodi', 'nayeltrader@gmail.com', NULL, '$2y$10$cPkuQIKsthSDohviO1c27uWKPNLyrYpwevSQF6fYvyQoxTP2y3LBG', '569757872', 'individual', '', 'default', 'default', 1, '9347', 0, 0, '3hahkoQvMyhTuBV9KbS1DnBU2', 0, 0, '', NULL, '2019-12-18 16:06:08', '2019-12-18 16:06:08'),
(155, 'test', 'tes1t@gmail.com', NULL, '$2y$10$zfqxczsfL45SJg0weiIpXuXR6elZTR1OFJljM.nN/YBJ3QqbYxd6.', '12534685', 'individual', '', 'default', 'default', 3, '6976', 0, 0, 'BT0qg9dJyLn5OoSuMahONr0HC', 0, 0, 'dc80jY-lJYE:APA91bFs3Kc3SpeAzl_wN3gNuo1l1ZtvV0UOuhGVuAKSv-yA8Jb8g2U9xH8KqM4t05RTHw_e8E9DizdFv2DTbHFf-YYmmB583Ubm8J2gWB-iyUaNaGdeyn-2aWS_tQEImscVILua5tVs', NULL, '2019-12-26 16:48:00', '2020-01-02 13:21:20'),
(159, 'aaamal', 'ama@gmail.com', NULL, '$2y$10$l3Tu/5M78JAdzVjWtbc5E.Lfll1NfpxiOhi.REMWMXEZAsZ2ZZOim', '21994979', 'company', '', '855555', '05555', 1, '', 0, 0, '1p3WWbJNzaJUIkItQJUsUXPKR', 0, 0, 'dc80jY-lJYE:APA91bFs3Kc3SpeAzl_wN3gNuo1l1ZtvV0UOuhGVuAKSv-yA8Jb8g2U9xH8KqM4t05RTHw_e8E9DizdFv2DTbHFf-YYmmB583Ubm8J2gWB-iyUaNaGdeyn-2aWS_tQEImscVILua5tVs', NULL, '2020-01-02 12:40:29', '2020-01-02 12:42:03'),
(162, 'mohamed', 'mohamed_atef250@yahoo.com', NULL, '$2y$10$DdmMETu.jssic0vBWBpcx.sCfNu/XI.vlgD8UZnaR8ju9SpebIbIO', '54631843', 'individual', '', 'default', 'default', 9, '', 0, 0, 'aLaJcUZia6sPMZ9FugKlHUorJ', 0, 0, 'dc80jY-lJYE:APA91bFs3Kc3SpeAzl_wN3gNuo1l1ZtvV0UOuhGVuAKSv-yA8Jb8g2U9xH8KqM4t05RTHw_e8E9DizdFv2DTbHFf-YYmmB583Ubm8J2gWB-iyUaNaGdeyn-2aWS_tQEImscVILua5tVs', NULL, '2020-01-06 10:46:52', '2020-01-06 10:48:50'),
(163, 'alaa', 'alaa1996@gmail.com', NULL, '$2y$10$NjpwlVbc/RkVsDTxejuY5.Iulyi5hs0YQZFWc9AtqvdwwpILewfCi', '1122334455', 'individual', '1578387548.jpg', 'default', 'default', 4, '', 0, 0, 'zJAsU0nIE40DprtdARRN2xTyZ', 0, 0, 'frHcvksQYLo:APA91bGvIVrbWFQNp7-4i6ieaw2Hv2ZBMBVek79Y5dFh4mImquRPxSLGM0dLgPXP0g6jX5YG3fBl_ndiviebFSRTkh-4EK13UZ3dySZUhyWfcgoHUz2hRQ5FUpak3J3ic-8adtVxNt_P', NULL, '2020-01-07 06:56:19', '2020-01-08 06:23:28'),
(164, 'asa', 'alispm2010@gmail.com', NULL, '$2y$10$NHUQMiz4JWtc58LvBR/.Wuskp8bXseA1OM8ma5jFtJkf4evFx/zQy', '541134381', 'individual', '', 'default', 'default', 17, '', 0, 0, 'kA2FqCIPu1m8eRR9uKGmGkudJ', 0, 0, 'dpynLgeL6xk:APA91bHp_W8WYisShEOwjGtvjfn0hd_5vdIXJkRWHaPwo_8HxRUkSBv14uwr1YVzZJ9eIMVJfD5BKA3YPDfMA1yFfm8j1lSH6Vj61CD4EP6UkJNw4WZTINbLEwhGNXG7vsYQunAvVAQi', NULL, '2020-01-09 08:11:28', '2020-01-09 08:11:59'),
(165, '0550017857', 'abdullahalsuwayyid@gmail.com', NULL, '$2y$10$7APy5IdASgKSARLUiHg3sudphHJg8VmKAYrBlIS1biCFaEB8uDusC', '550017857', 'individual', '', 'default', 'default', 17, '', 0, 0, 'qfNPG37Xa8VkrnVUc64tntXTx', 0, 0, 'emFkcvWo1SY:APA91bF4UHqcWApYdE_vrTO9qFFiur6CrCsdgJkFNh0Qwv8SOVIbl0p5U00VIyhOX3NDNa9wprHMMUtM6G0QkPw9SYMPgixaOOmvfJM3QKz7pymsFw5WtNIJlJ0cYgYk0tb5dxIP-0_g', NULL, '2020-01-10 08:03:16', '2020-01-30 19:08:55'),
(166, 'Ali z', 'azandan@hotmail.com', NULL, '$2y$10$4gvMpWrcAG7gpd6egG/vxunmkjK/T8eVCdZtDuIX78EIzvgOU1Q5u', '550634333', 'individual', '', 'default', 'default', 1, '', 0, 0, 'WlorXYoyZkra7oFEGUplgziU5', 0, 0, 'dAlK7hfoBlQ:APA91bEOzI9wUHf1hTSHLqgotA1Kdm_uJbfhiXgX7S0RUUsDs6p9yETEPSMebLyhQ2hUVcb6wbbaWLeooZ_oACnwwRl74vQKQ8JihXn5Gjgew5rjaB1TZiylbuB-a_R4hxI3ml8rz7BG', NULL, '2020-01-10 13:55:46', '2020-01-10 13:56:12'),
(167, 'fahad', 'saeed2009191@hotmail.com', NULL, '$2y$10$mtN88YjFl.qtHNcOQC8nL.vceBIUy7diVnaTMCjjiO3W78oKC0InK', '532794006', 'individual', '', 'default', 'default', 17, '', 0, 0, 'HJPQAczZeOq4pU8ZE9MCAA3uV', 0, 0, 'cYvAf4s0nEY:APA91bHEDBmxZ5RmgPvJjy2oPWAkDU2k0yYUHEdw8TAndiQ_WNlVAtHw7PetT8aZRBHPXnb5YIKS1GFSmevYOqPRBLhe11KbCvq_-L9JBsfYDA8kwq9yaR6rf4Soa-FGT37yNVWKwt_f', NULL, '2020-01-10 15:50:09', '2020-01-10 15:50:30'),
(168, 'rashed', 'rashed1707@gmail.com', NULL, '$2y$10$sf7N1GZYWxzCspXCV2wrTewrNCQo3dlgaEoGyvm0zHts32e7wsTCy', '555588154', 'individual', '', 'default', 'default', 1, '', 0, 0, 'chE5uBjYfQ2dF7cXL3tJUFEXR', 0, 0, 'cN6NnHwYgHk:APA91bFFKaSxnw_Uf51v-D2PWdyOgUlixo8DVJ458yrkds1e84HXLTEV_PqICOMyCwpzN-9ALE1dlguhiu5nQz8QwM9pqhyiorXhMKC4h_nk2qNnpnnQUZeFUdnFg-vzuXfMSEuwyEHy', NULL, '2020-01-10 17:32:19', '2020-01-10 17:32:46'),
(169, 'alaa', 'alaa@gmail.com', NULL, '$2y$10$SMrKEoJd3/O7V4VvFQvQ6uMZAezD5Y2aDZClg9JXXaJPfUjP1oWL6', '123456789', 'company', '', '7', '9', 1, '4140', 0, 0, '8XbLxL6O1DcjJD6cFrGqRXS4M', 0, 0, '', NULL, '2020-01-11 21:17:18', '2020-01-11 21:17:18'),
(170, 'abdullah', 'emass15@hotmail.com', NULL, '$2y$10$z4/sbcwCuqHLejj/aGml4uBFJfq42a8P2.Yh39IL2AhD4hcdtnEHu', '560904789', 'individual', '', 'default', 'default', 1, '', 0, 0, 'jjlDCgIkhqfMusZ0i7JuilvBO', 0, 0, 'dpynLgeL6xk:APA91bHp_W8WYisShEOwjGtvjfn0hd_5vdIXJkRWHaPwo_8HxRUkSBv14uwr1YVzZJ9eIMVJfD5BKA3YPDfMA1yFfm8j1lSH6Vj61CD4EP6UkJNw4WZTINbLEwhGNXG7vsYQunAvVAQi', NULL, '2020-01-13 06:24:54', '2020-01-13 06:25:25'),
(179, 'alaa', 'alaahamzahamza@gmail.com', NULL, '$2y$10$rdaKeR6TpaFavy1PpqOQeOghGoItCZ.W4Erazhh/b1N4GYYNWFAUq', '1101201333', 'company', '', '2', '7', 3, '9278', 0, 0, 'o4rZobCxkN4ErmmOn4Q8YruS5', 0, 0, '', NULL, '2020-01-14 04:40:34', '2020-01-14 04:40:34'),
(184, 'alaa', 'alaaalaaaaaaa@gmail.com', NULL, '$2y$10$get25chhMnIsukUHc6zpjeXyXU6M9eugz4OPf1eVfn3FBp9MBj6Su', '11012013', 'company', '', '123456', '3791258963147', 1, '4626', 0, 0, '4pGdgDeh8Zm0gpysMljkQ6CVs', 0, 0, '', NULL, '2020-01-14 05:15:08', '2020-01-14 05:15:08'),
(185, 'alaa', 'alaaaaaalaaaaaaaa@gmail.com', NULL, '$2y$10$TNc4PwjpGM4j.tEaJ8ZE/OZht2bhMbQmC4PpgTEqCIoof3hR4Numm', '11012013333', 'company', '', '123455567', '337911258963147', 1, '6428', 0, 0, 'LlEKnjdnLxU8NbbXCWZrGXMLz', 0, 0, '', NULL, '2020-01-14 05:17:47', '2020-01-14 05:17:47'),
(186, '1047736994', 'nxn989@gmail.com', NULL, '$2y$10$T7ynvGyaT502AF6TUJbtbO1VygbfjFz2yNm6qFmiKaYlC6wrC2lAq', '554147337', 'individual', '', 'default', 'default', 1, '', 0, 0, 'nF8vIQzVJsg0ojHJQW6Zxb60g', 0, 0, 'ftZmxcvmWoI:APA91bF3YuN4cyWSoW0F6TNeZ5j7qOH5fJMwqk7biIUnt1mM-C8l6tjdp3SuNIfdZvsj1lX3a6X62-xtvqS7hU-26-QoHQwRsa6Rtpha6n-x7h1STg34Bz4OPJAYcgzDzG1IwpxB84jA', NULL, '2020-01-17 13:24:07', '2020-01-17 13:24:50'),
(196, 'ess', 'e@e.com', NULL, '$2y$10$oFj6Ja0gsUdNh7WfcBRMVuy2Zi8F76RqwcUbzdJzW6BS2WblWfHI2', '88', 'individual', '', 'default', 'default', 2, '', 0, 0, 'x7c2m5grEHnCIoz7ZzTnxWRlr', 0, 0, '', NULL, '2020-01-21 11:01:38', '2020-01-21 11:02:16'),
(197, 'mohamed', 'mohamed_atef2500@yahoo.com', NULL, '$2y$10$xRxQMelZhGsemAF6oTu84enjEgpJZlxMr3wp0nWaANGc3R.eYJSLm', '53694315', 'individual', '', 'default', 'default', 1, '', 0, 0, 'LJP0jcD0X4vk8bUUWsFydclv4', 0, 0, 'dc80jY-lJYE:APA91bFs3Kc3SpeAzl_wN3gNuo1l1ZtvV0UOuhGVuAKSv-yA8Jb8g2U9xH8KqM4t05RTHw_e8E9DizdFv2DTbHFf-YYmmB583Ubm8J2gWB-iyUaNaGdeyn-2aWS_tQEImscVILua5tVs', NULL, '2020-01-22 05:28:14', '2020-01-22 05:28:36'),
(198, 'حامد', 'hamed.higazy@yahoo.com', NULL, '$2y$10$8shm.1FI4WagHToshRxuq.heOfulM/8OA8Cu92I1mifUJ1RZGp.4G', '1025744857', 'individual', '', 'default', 'default', 1, '7491', 0, 0, 'kNR2PbRrsOsUD74xQ9f6OSQuD', 0, 0, 'ec9RY01MHoo:APA91bHD9iXWmbaUvLoVqtCtZKH_f85OaFi9gvbTkKefvyyuhd24u55VjwKgryDHX-rG99WM0ZbE-tIHYLxYRxoNWCDDK7mIKcjusNNP9mWpxbHOyTJUw96r01FYWtKkbDtUOBmaDmD9', NULL, '2020-01-27 10:35:06', '2020-01-27 10:37:10'),
(199, 'amal', 'alaaamaaaalalaa@gmail.com', NULL, '$2y$10$V6kkHSt3FuRE.yiQRiZwEOQjaA5b27B95/anrNg76adKvq0wdrUNK', '5147741', 'individual', '', 'default', 'default', 3, '', 0, 0, '0aqwhCDOxMi4yi6bVJ3T16Chy', 0, 0, '', NULL, '2020-01-28 11:40:37', '2020-01-28 11:41:36'),
(203, 'asd', 'alispm1234@gmail.com', NULL, '$2y$10$XVxQvjGia56Xi9JoMBOALuPvq37CvJdgVv3fOez7Mk8vNp0AI.aFi', '569146704', 'individual', '', 'default', 'default', 1, '', 0, 0, '1UxnjsTi2Ui6Arl0eCO2ferM1', 0, 0, 'emFkcvWo1SY:APA91bF4UHqcWApYdE_vrTO9qFFiur6CrCsdgJkFNh0Qwv8SOVIbl0p5U00VIyhOX3NDNa9wprHMMUtM6G0QkPw9SYMPgixaOOmvfJM3QKz7pymsFw5WtNIJlJ0cYgYk0tb5dxIP-0_g', NULL, '2020-01-30 19:14:26', '2020-02-12 15:13:12'),
(204, 'ahmed', 'ghjaj@ghhgg.com', NULL, '$2y$10$12aWvzG/nH9.fac7AOpjrOQUJ9MEEKjT0RVu0aUwVcaQ/QmZHkd6e', '1111190368', 'company', '', '526885', '5888228888', 2, '6014', 0, 0, 'aURCRVIIr0zFVpLMiuYi9Gp0S', 0, 0, '', NULL, '2020-02-03 09:04:23', '2020-02-03 09:04:23'),
(205, 'شركة ابناء قاسم', '6544743@gmail.com', NULL, '$2y$10$nfMdy/eVjt39Xst1Msu6r.3l5ANdUZ70iFwPCClFKQGuUNx3K9YKa', '552001466', 'company', '', '088878', '0505055', 7, '5066', 0, 0, '3O6BBbfdzKFcBJBWMpn5nzzMS', 0, 0, '', NULL, '2020-02-13 21:26:32', '2020-02-13 21:26:32'),
(206, 'شركة ابناء قاسم', '65447743@gmail.com', NULL, '$2y$10$J2KToLXHEpwZKVyhiCqxXeroXwMGZYd0kv7doy3ld8/GeELrpUOT2', '552001468', 'company', '', '088878', '0505055', 7, '', 0, 0, 'bMmndsRTsvOYv5WNPDAsXhWiJ', 0, 0, 'dEUFab7M4Og:APA91bGL3mnx3w8RyABB2w6-gjiSSksoo3i6k1ZuxZYhVuh3gewBFEry-QcinqmXSWxSEud7dDgeAWF4HPI5MLoL-dEBlJk8JWyeoxB3KMMd9jR6HBDRTvM-3KSo5D0StdH0a6ZnHwMZ', NULL, '2020-02-13 21:26:49', '2020-02-13 21:27:22'),
(207, 'fadeel', 'fadeel.aloqili@gmail.com', NULL, '$2y$10$ikF0XOIi1Rq4ssJFovacL.A/vxtKI4Rt65VXkIQ6yCxTTAEqqWCS.', '550035947', 'individual', '', 'default', 'default', 1, '', 0, 0, 'DZ3DxyKv6uKG3nl7iYihsioTu', 0, 0, 'cWZODbxCp-M:APA91bHH3vKlctuwkEWqaExIGUQyIgqt9HYISdjXgDiXJ5C-B-KU28F69K53X_2_C-J_ZTJ4fSG8xF-algYDhYr6FLkPQki4px7nZtGLRzdQEDlU8TydUgch5DcuVw7MKxNRMuo4dtrL', NULL, '2020-02-17 08:47:43', '2020-02-17 08:48:01'),
(208, 'amera', 'amerahelmi96@gmail.com', NULL, '$2y$10$9huHsvt5ErIxkv/utqUwreYVO4Y0PGlxdFpTZ67E2ECUJdDi0NRiW', '1062237404', 'individual', '', 'default', 'default', 3, '6962', 0, 0, 'DqyUVWX6ImYN8Et7OA0c3EzkV', 0, 0, 'dlSjxJ7lzxE:APA91bF7Y5ULQ5cR1yhqTW1hWVdYmtzYeNkn0aeZa0iKMtE_YjmXlNoVVdebwYg1XPi5yvIsUpTg-aZpaTjbELdzhpF9BllZSil9BSIy6e6sWIO361iS-mv1t7bF6L7ZOJJ_HkMW8bBC', NULL, '2020-03-03 08:25:18', '2020-03-04 06:03:55'),
(209, 'ahmed', 'ahmed.elbakary25991@gmail.com', NULL, '$2y$10$QY31SIfe5kEPci6GxCpa5eDcx37p41o2X9Xmj3TOO2R0FqzcV0hBC', '+201017196286', 'individual', '', 'default', 'default', 12, '8942', 0, 0, 'nWciUmXdSTCpusgCKostbGqff', 0, 0, '', NULL, '2020-03-09 22:25:29', '2020-03-09 22:25:29'),
(210, 'hesham', 'heshamboss35@gmail.com', NULL, '$2y$10$Oc1sABdM9L9qZrlTjQmI..i3oEzG1tpdlUGFiRCeRNG0AMwdb5HWe', '555666332', 'individual', '', 'default', 'default', 2, '4648', 0, 0, 'ZeYnxR4HFgVUgAoFNukGS0Z6P', 0, 0, '', NULL, '2020-04-11 05:14:30', '2020-04-11 05:14:30');

-- --------------------------------------------------------

--
-- Table structure for table `waste_containers`
--

CREATE TABLE `waste_containers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name_E` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_A` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_id` bigint(20) UNSIGNED NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `distance` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_E` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_A` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `month` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `days` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `waste_containers`
--

INSERT INTO `waste_containers` (`id`, `name_E`, `name_A`, `company_id`, `price`, `distance`, `description_E`, `description_A`, `date`, `month`, `days`, `total`, `image`, `created_at`, `updated_at`) VALUES
(1, 'garbage box 1', 'صندوق القمامة 1', 1, '150', '11', 'high quality stainless steel trash bin,soft close waste bins compressed garbage can', 'لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق \"ليتراسيت\" (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل \"ألدوس بايج مايكر\" (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.', '0000-00-00', '', '', '', 'uploads/container/1571734895Group2862.png', '2019-10-07 20:00:00', '2020-05-02 07:55:19'),
(2, 'garbage box 1', 'صندوق القمامة 2', 2, '40', '8', 'high quality stainless steel trash bin,soft close waste bins compressed garbage can', 'لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق \"ليتراسيت\" (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل \"ألدوس بايج مايكر\" (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.', '0000-00-00', '', '', '', 'uploads/container/1571734905Group2863.png', '2019-10-21 20:00:00', '2019-10-22 05:01:45'),
(3, 'garbage box 3', 'صندوق القمامة 3', 3, '30', '11', 'high quality stainless steel trash bin,soft close waste bins compressed garbage can', 'لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق \"ليتراسيت\" (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل \"ألدوس بايج مايكر\" (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.', '0000-00-00', '', '', '', 'uploads/container/1571734919Group2864.png', NULL, '2019-10-22 05:01:59'),
(4, 'garbage box 4', 'صندوق القمامة 4', 4, '150', '50', 'high quality stainless steel trash bin,soft close waste bins compressed garbage can', 'لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق \"ليتراسيت\" (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل \"ألدوس بايج مايكر\" (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.', '0000-00-00', '', '', '', 'uploads/container/1571734929Group2865.png', NULL, '2019-10-22 05:02:09'),
(5, 'garbage box 5', 'صندوق القمامة 5', 5, '150', '60', 'high quality stainless steel trash bin,soft close waste bins compressed garbage can', 'لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق \"ليتراسيت\" (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل \"ألدوس بايج مايكر\" (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.', '0000-00-00', '', '', '', 'uploads/container/1571734943Group2866.png', NULL, '2019-10-22 05:02:23'),
(6, 'garbage box 6', 'صندوق القمامة 6', 25, '150', '70', 'high quality stainless steel trash bin,soft close waste bins compressed garbage can', 'لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق \"ليتراسيت\" (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل \"ألدوس بايج مايكر\" (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.', '0000-00-00', '', '', '', 'uploads/container/1571734960Group1117.png', NULL, '2019-10-22 05:02:40'),
(7, 'Waste container', 'حاويه نفايات', 30, '3950', '660', 'Waste container with lid and mobile legs', 'حاويه نفايات بغطاء و ارجل متحركه', '0000-00-00', '', '', '', 'uploads/container/1572433698Group2866.png', '2019-10-30 07:08:18', '2019-10-30 07:08:18'),
(8, 'Waste container 2 separate cover', 'حاويه نفايات 2 غطاء منفصل', 27, '500', '555', 'Large waste container 2 part 2 Separate cover fixed to the ground', 'حاويه نفايات كبيرة 2 جزء2 غطاء منفصل ثابته على الارض', '0000-00-00', '', '', '', 'uploads/container/1572434030Group2864.png', '2019-10-30 07:13:50', '2019-10-30 07:14:37'),
(9, 'Fixed waste container', 'حاويه نفايات ثابته', 25, '555', '525', 'A large waste container is fixed on the ground with a lid', 'حاويه نفايات كبيرة ثابته على الارض بغطاء', '0000-00-00', '', '', '', 'uploads/container/1572434242Group2865.png', '2019-10-30 07:17:22', '2019-10-30 07:17:22'),
(10, 'Waste container', 'حاويه نفايات', 30, '2000', '550', 'Waste container without mobile cover', 'حاويه نفايات بدون غطاء متحركه', '0000-00-00', '', '', '', 'uploads/container/1572434480Group2863.png', '2019-10-30 07:21:20', '2019-10-30 07:21:20'),
(11, 'Waste container', 'حاويه نفايات', 24, '5555', '565', 'حاويات متحركه بالوان مختلفه\r\nMobile containers in different colors', 'حاويات متحركه بالوان مختلفه', '0000-00-00', '', '', '', 'uploads/container/1572434952Group1117.png', '2019-10-30 07:29:12', '2019-10-30 07:29:12'),
(12, 'Waste container', 'حاويه نفايات', 21, '6666', '666', 'Large waste container fixed with lid', 'حاويه نفايات كبيرة ثابته بغطاء', '0000-00-00', '', '', '', 'uploads/container/1572435137Group2867.png', '2019-10-30 07:32:17', '2019-10-30 07:32:17'),
(13, 'Waste container', 'حاويه نفايات', 23, '6566', '450', 'Large waste container suitable for factories', 'حاويه نفايات كبيرة تصلح للمصانع', '0000-00-00', '', '', '', 'uploads/container/1572435225Group2868.png', '2019-10-30 07:33:45', '2019-10-30 07:33:45'),
(14, 'Waste container', 'حاويه نفايات', 22, '6566', '450', 'Waste container with mobile cover', 'حاويه للنفايات بغطاء متحركه', '0000-00-00', '', '', '', 'uploads/container/1572435333Group2862.png', '2019-10-30 07:35:33', '2019-10-30 07:35:33'),
(15, 'test', 'حاويه تيست', 38, '31', '6464', 'jbjbjb', 'ohlknlknkl;', '0000-00-00', '', '', '', 'uploads/container/1577186687Hydrangeas.jpg', '2019-12-24 09:24:47', '2019-12-24 09:24:47'),
(16, 'hawity', 'حاوية', 40, '900', '5', 'test', 'تيست', '0000-00-00', '', '', '', 'uploads/container/1577943776Desert.jpg', '2020-01-02 03:42:56', '2020-01-02 03:42:56'),
(17, 'amalalaa', 'amaaaal', 41, '90', '5', 'test90', 'test900', '0000-00-00', '', '', '', 'uploads/container/15779671855e0dde51d53bd.صخور.jpg', '2020-01-02 10:13:05', '2020-01-02 10:13:05'),
(18, 'garbage box 2', 'صندوق القمامة 2', 1, '150', '11', 'high quality stainless steel trash bin,soft close waste bins compressed garbage can', 'لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق \"ليتراسيت\" (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل \"ألدوس بايج مايكر\" (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.', '0000-00-00', '', '', '', 'uploads/container/1571734895Group2862.png', '2019-10-07 20:00:00', '2019-10-22 05:01:35');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_us`
--
ALTER TABLE `about_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_sellers`
--
ALTER TABLE `company_sellers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_user_id_foreign` (`user_id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_details_order_id_foreign` (`order_id`),
  ADD KEY `order_details_container_id_foreign` (`container_id`),
  ADD KEY `order_details_user_id_foreign` (`user_id`),
  ADD KEY `order_details_company_id_foreign` (`company_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `privacy`
--
ALTER TABLE `privacy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reservations_user_id_foreign` (`user_id`),
  ADD KEY `reservations_container_id_foreign` (`container_id`),
  ADD KEY `reservations_company_id_foreign` (`company_id`);

--
-- Indexes for table `suggestions`
--
ALTER TABLE `suggestions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `suggestions_user_id_foreign` (`user_id`);

--
-- Indexes for table `take_down_containers`
--
ALTER TABLE `take_down_containers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `take_down_containers_container_id_foreign` (`container_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_phone_unique` (`phone`),
  ADD KEY `users_city_id_foreign` (`city_id`);

--
-- Indexes for table `waste_containers`
--
ALTER TABLE `waste_containers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `waste_containers_company_id_foreign` (`company_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_us`
--
ALTER TABLE `about_us`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `company_sellers`
--
ALTER TABLE `company_sellers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT for table `privacy`
--
ALTER TABLE `privacy`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `reservations`
--
ALTER TABLE `reservations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `suggestions`
--
ALTER TABLE `suggestions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `take_down_containers`
--
ALTER TABLE `take_down_containers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=214;

--
-- AUTO_INCREMENT for table `waste_containers`
--
ALTER TABLE `waste_containers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `order_details_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `company_sellers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `order_details_container_id_foreign` FOREIGN KEY (`container_id`) REFERENCES `waste_containers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `order_details_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `reservations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `order_details_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `reservations`
--
ALTER TABLE `reservations`
  ADD CONSTRAINT `reservations_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `company_sellers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reservations_container_id_foreign` FOREIGN KEY (`container_id`) REFERENCES `waste_containers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reservations_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `suggestions`
--
ALTER TABLE `suggestions`
  ADD CONSTRAINT `suggestions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `take_down_containers`
--
ALTER TABLE `take_down_containers`
  ADD CONSTRAINT `take_down_containers_container_id_foreign` FOREIGN KEY (`container_id`) REFERENCES `waste_containers` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `waste_containers`
--
ALTER TABLE `waste_containers`
  ADD CONSTRAINT `waste_containers_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `company_sellers` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
