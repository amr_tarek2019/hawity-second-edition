<?php

namespace App\Http\Controllers\API;

use App\TakeDownContainer;
use App\WasteContainer;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Illuminate\Support\Facades\Validator;


class TakeDownContainerController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
             $language = $request->header('lang');
        if($language=="en"){
             $language = "E";
        }else {
            $language = "A";
        }
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;

        $user = \App\User::where('jwt_token',$jwt)->first();
      
        $validator = Validator::make($request->all(), [
            'photo' => 'required',
            'note_E' => 'required',
            'note_A' => 'required'
        ]);
        if ($validator->fails()) {
      
             $response=[
            'message'=>trans('api.somethingwentwrong'),
            'status'=>404,
        ];
        }
        WasteContainer::where('id',$request->id)->select('id')->first();
        TakeDownContainer::create($request->all())->find($request->jwt_token);

        $response=[
            'message'=>trans('api.reservation'),
            'status'=>202,
        ];
        return \Response::json($response,202);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
