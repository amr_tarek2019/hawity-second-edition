<?php

namespace App\Http\Controllers\API;

use App\OrderDetail;
use App\Reservation;
use App\WasteContainer;
use App\TakeDownContainer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as BaseController;
use Illuminate\Support\Facades\Validator;

class OrderDetailsController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
            $language = $request->header('lang');
        if($language=="en"){
             $language = "E";
        }else {
            $language = "A";
        }
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = \App\User::where('jwt_token', $jwt)->first();
        $reservation = Reservation::where('user_id', $user->id)->where('status', 1)->where('id',$request->id)->orderBy('id','desc')->get();
        $res_item = [];
        $res_list  = [];
        foreach ($reservation as $res) {
             $res_item['reservation_id'] = $res->id;
            $res_item['user_id'] = $res->user_id;
            $res_item['container_details'] = WasteContainer::where('id',$res->container_id)->select('image','name_'.$language.' as name','description_'.$language.' as description','price','distance')->first();
             $res_item['company_details'] = \App\CompanySeller::where('id',$res->company_id)->select('company_name_'.$language.' as name')->first();
             $res_item['takedown_container'] = TakeDownContainer::where('id',$res->container_id)->select('photo')->first();
            $res_item['date_from'] = $res->date_from;
            $res_item['date_to'] = $res->date_to;
            $res_item['days'] = $res->days;
            $res_list[] = $res_item;

    }

        $response = [
            'message' => 'get data of User Order successfully',
            'status' => 202,
            'data' => $res_list,
        ];
        return \Response::json($response, 202);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }
    


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $language = $request->header('lang');
        if($language=="en"){
            $language = "E";
        }else {
            $language = "A";
        }
        $reservation=Reservation::where('status','1')->where('id',$request->id)->get();
        $res_item = [];
        $res_list  = [];
        foreach ($reservation as $res) {
            $res_item['id'] = $res->id;
            $res_item['user_id'] = $res->user_id;
            $res_item['container_id'] = $res->container_id;
            $res_item['container_details'] = WasteContainer::where('id',$res->container_id)->select('id','image','name_'.$language.' as name','description_'.$language.' as description','price','distance')->first();
            $res_item['company_details'] = \App\CompanySeller::where('id',$res->company_id)->select('company_name_'.$language.' as name')->first();
            $res_item['takedown_container'] = TakeDownContainer::where('id',$res->container_id)->select('photo')->first();
            $res_item['date_from'] = $res->date_from;
            $res_item['date_to'] = $res->date_to;
            $res_item['days'] = $res->days;
            $res_item['total'] = $res->total;
            $res_list[] = $res_item;

        }

        $response = [
            'message' => 'get data of User Reservations successfully',
            'status' => 202,
            'data' => $res_list,
        ];
        return \Response::json($response, 202);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }
        $response=[
            'message'=>'get reservation item successfully',
            'status'=>200,
            'data'=>$reservation,
        ];

        return \Response::json($response,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;

        $user = \App\User::where('jwt_token',$jwt)->first();
        // return  $user['id'];
        $validator = Validator::make($request->all(), [
            'date_from' => 'required',
            'days' => 'required',
        ]);
        if ($validator->fails()) {
          
            $response=[
                'message'=>'there is something wrong',
                'status'=>'404',
            ];
        }
        $price = WasteContainer::where('id',$request->container_id)->select('price')->first();

        $date_from =$request->date_from;
        $days=$request->days;
        $date_to=date('d-m-Y', strtotime($date_from. ' + '.$days.' days'));

        $total=$days*$price['price'];
        $reservation=Reservation::find($request->id);
        $reservation->date_from = $request->date_from;
        $reservation->days=$request->days;
        $reservation->date_to=$date_to;
        $reservation->total=$total;
        $reservation->status='1';
        $reservation->save();
        $response=[
            'message'=>trans('api.reservation'),
            'status'=>'202',
        ];
        return \Response::json($response,202);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $orderDetail=OrderDetail::find($request->id);
        if (OrderDetail::destroy($request->id)){

            $response=[
                'message'=>trans('api.deleted'),
                'status'=>202,
            ];
            return \Response::json($response,202);
        }else{
            $response=[
                'message'=>trans('api.somethingwentwrong'),
                'status'=>401,
            ];
            return \Response::json($response,404);
        }
    }
    
    





}
