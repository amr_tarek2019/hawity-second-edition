<?php

namespace App\Http\Controllers\API;

use App\WasteContainer;
use App\Reservation;
use App\CompanySeller;
use http\Client\Curl\User;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;

class WasteContainerController extends BaseController
{
    public function index(Request $request)
    {
        $language = $request->header('lang');
        if($language=="en"){
             $language = "E";
        }else {
            $language = "A";
        }
        
        $wasteContainers=WasteContainer::where('company_id',$request->company_id)->get();
        
          $container_Items = [];
            $container_list =[];
        foreach($wasteContainers as $wasteContainer)
        {
             $container_Items['id'] = $wasteContainer->id;
           $container_Items['name'] = WasteContainer::where('company_id',$request->company_id)->select('name_'.$language.' as name')->first();
        $container_Items['company_id'] = $wasteContainer->company_id;
        $container_Items['company_name'] = \App\CompanySeller::where('id',$wasteContainer->company_id)->select('id','company_name_'. $language . ' as name')->first();
         $container_Items['image'] = $wasteContainer['image'];
          $container_Items['price'] = $wasteContainer['price'];
          $container_Items['distance'] = $wasteContainer['distance'];
        $container_list[] = $container_Items;
        
        }
     
   
        $response=[
            'message'=>'get data of user successfully',
            'status'=>202,
            'data'=>$container_list,
        ];
      
        return \Response::json($response,202);
    }

    public function show(Request $request)
    {

     $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $language = $request->header('lang');
        if($language=="en"){
             $language = "E";
        }else {
            $language = "A";
        }
        $wasteContainers=WasteContainer::where('id',$request->id)->get();
        
          $container_Items = [];
            $container_list =[];
        foreach($wasteContainers as $wasteContainer)
        {
       $container_Items['name'] = WasteContainer::where('id',$request->id)->select('name_'.$language.' as name')->first();
        $container_Items['id'] = $wasteContainer['id'];
   
        $container_Items['company_id'] = $wasteContainer->company_id;
        $container_Items['company_name'] = \App\CompanySeller::where('id',$wasteContainer->company_id)->select('company_name_'. $language . ' as name')->first();
         $container_Items['image'] = $wasteContainer['image'];
          $container_Items['price'] = $wasteContainer['price'];
          $container_Items['distance'] = $wasteContainer['distance'];
     
           $container_Items['description'] = \App\WasteContainer::where('id',$wasteContainer->id)->select('description_'.$language.' as description')->first();
         $container_list = $container_Items;
        
        }
            $response=[
                'message'=>'get data of waste container successfully',
                'status'=>202,
                'data'=>$container_list,
            ];
            return \Response::json($response,202);
        }
        
        
        public function containerDetails(Request $request)
        {
                     $language = $request->header('lang');
        if($language=="en"){
             $language = "E";
        }else {
            $language = "A";
        }
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = \App\User::where('jwt_token', $jwt)->first();
        $reservation = Reservation::where('user_id', $user->id)->where('status', 1)->get();
        $res_item = [];
        $res_list  = [];
        foreach ($reservation as $res) {
             $res_item['reservation_id'] = $res->id;
            $res_item['user_id'] = $res->user_id;
            $res_item['container_details'] = WasteContainer::where('id',$res->container_id)->select('id','image','name_'.$language.' as name','description_'.$language.' as description','price','distance')->first();
             $res_item['company_details'] = \App\CompanySeller::where('id',$res->company_id)->select('company_name_'.$language.' as name')->first();
             $res_item['takedown_container'] = TakeDownContainer::where('id',$res->container_id)->select('photo')->first();
            $res_item['date_from'] = $res->date_from;
            $res_item['date_to'] = $res->date_to;
            $res_item['days'] = $res->days;
            $res_list[] = $res_item;

    }

        $response = [
            'message' => 'get data of User Order successfully',
            'status' => 202,
            'data' => $res_list,
        ];
        return \Response::json($response, 202);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }
        }

    }


