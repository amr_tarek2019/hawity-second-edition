<?php

namespace App\Http\Controllers\API;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\PushNotification;


class AuthController extends BaseController
{
    public function sms($user,$code, $send = false)
    {
       
        if ($send) {
            $username = '966540020169';
            $password = 'Ali9790a';
            // TODO :: send the code by sms to the user and enable it for production
            $message = " كود التفعيل الخاص بك هو " . $code;
            $message = urlencode($message);
            $url = "https://www.hisms.ws/api.php?send_sms&username=$username&password=$password&message=$message&numbers=$user->phone&sender=hawity&unicode=e&Rmduplicated=1&return=json";
            $ch = curl_init();
            $headers = array(
                'Content-Type:application/json',
                'Authorization:Basic ' . base64_encode("$username:$password")
            );
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $data = curl_exec($ch);
            curl_close($ch);
            $decodedData = json_decode($data);
        }
        return $code;
    }

    public function login(Request $request)
    {
        if (auth()->attempt(['phone' => ltrim($request->input('phone'), '0'),
            'password' => $request->input('password')])) {
            $user = auth()->user();
            if($user) {
                return response()->json([
                    'status'=>202,
                    'message'=>trans('api.logged'),
                    'data'=>$user
                ]);
            }
        } else {

            return response()->json([
                'status'=>401,
                'message'=>trans('api.check')
            ]);
        }
    }

    public function registerCompany(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required',
            'password' => 'required',
            'email' => 'required|email',
            'commercial_register' => 'required',
            'tax_record' => 'required',
            'city_id' => 'required',

        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $verify_code = rand(1111, 9999);
        $jwt_token = Str::random(25);
        $user=User::where('phone',$request->phone)->orWhere('email',$request->email)->exists();
           $userPhone=$request->phone;
       if ($user){
            return $response=[
                'status'=>401,
                'message'=>trans('api.submitted'),
            ];
        }
        $user = User::create(array_merge($request->all(),[
            'user_type' => 'company',
            'jwt_token' => $jwt_token,
            'verify_code' => $verify_code,
            'longitude'=>'0',
            'latitude'=>'0',
            'phone'=>$request->phone,

        ]));
       

        $response=[
            'status'=>202,
            'data'=>$user,
            'message'=>trans('api.register'),

        ];

        $this->sms($user,$verify_code, true);
        return \Response::json($response,202);
    }

    public function registerUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required',
            'password' => 'required',
            'email' => 'required|email',
            'city_id'=>'required',

        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $verify_code = rand(1111, 9999);
        $jwt_token = Str::random(25);
        $user=User::where('phone',$request->phone)->orWhere('email',$request->email)->exists();
          $userPhone=$request->phone;
     
               if ($user){
            return $response=[
                'status'=>'failed',
                'message'=>trans('api.submitted'),
            ];
        }
        $user = User::create(array_merge($request->all(),[
            'user_type' => 'individual',
            'jwt_token' => $jwt_token,
            'verify_code' => $verify_code,
            'commercial_register'=>'default',
            'tax_record'=>'default',
            'longitude'=>'0',
            'latitude'=>'0',
        ]));
        $this->sms($user,$verify_code, true);


        $response=[
            'status'=>202,
            'data'=>$user,
            'message'=>trans('api.register'),

        ];
        return \Response::json($response,202);
    }

    public function forgotPassword(Request $request)
    {
        $this->validate($request,[
            'phone' => 'required'
        ]);
        $user = User::where('phone', request('phone'))->first();
        $verify_code = rand(1111, 9999);

        if (!empty($user)) {
            $user->verify_code=$verify_code;
            $user->save();
            $response=[
                'message'=>trans('api.code'),
                'status'=>202,
                'data' => ['code'=>$user->verify_code],
            ];
            return \Response::json($response,202);

        }else{
    
            $response=[
                'message'=>trans('api.phone'),
                'status'=>404,
            ];
            return \Response::json($response,404);
        }

    }

    public function activateCode(Request $request)
    {
        $this->validate($request,[
            'verify_code' => 'required',
        ]);
        $user = User::where('verify_code', request('verify_code'))->first();
        if (!empty($user)) {
            $user->verify_code=null;
            $user->save();
            $response=[
                'message'=>trans('api.verify'),
                'status'=>202,
                'data'=>$user,
            ];

            return \Response::json($response,202);

        }else{
            
            $response=[
                'message'=>trans('api.codenotfound'),
                'status'=>404,
            ];

            return \Response::json($response,200);
        }
    }

    public function resetPassword(Request $request)
    {
        $user=User::find($request->id);
        $this->validate($request,[
            'password' => 'required',
            'password_confirmation'=>'required',
        ]);
        if ($request->password !=null&&($request->password==$request->password_confirmation)){
            $user->password=$request->password_confirmation;
        }
        if ($user->save())
        {
            $response=[
                'message'=>trans('api.passwordchangedsuccess'),
                'status'=>202,
                'data'=>$user,
            ];

            return \Response::json($response,202);

        }else{
            $response=[
                'message'=>trans('api.somethingwentwrong'),
                'status'=>401,
                'data'=>$user,
            ];

            return \Response::json($response,401);
        }
    }


    public function location(Request $request)
    {

        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;

        $user = \App\User::where('jwt_token', $jwt)->first();
        //return  $user['id'];
        $validator = Validator::make($request->all(), [
            'longitude'=>'required',
            'latitude'=>'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $user = User::find($request->user_id);
        $user->longitude = $request->longitude;
        $user->latitude = $request->latitude;
        $user->save();

 
        $response = [
            'message' => 'Location inserted successfully',
            'status' => 202,
        ];
        // }
        return \Response::json($response, 202);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }
    }



}

