<?php

namespace App\Http\Controllers\API;

use App\User;
use App\City;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Illuminate\Support\Facades\Validator;
use DB;


class ProfileController extends BaseController
{
    public function index(Request $request)
    {

           $language = $request->header('lang');
        if($language=="en"){
             $language = "E";
        }else {
            $language = "A";
        }
        
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = \App\User::where('jwt_token',$jwt)->first();

        $data['name'] = $user['name'];
        $data['email'] = $user['email'];
        $data['phone'] = $user['phone'];
        $data['image'] = $user['image'];
        $data['city_name'] = city::where('id',$user['city_id'])->select('id','name_'. $language . ' as name')->first();
        if($user){
        $response=[
            'message'=>trans('api.getdataprofile'),
            'status'=>202,
            'data'=>$data,
        ];
        }else{
                $response=[
                'message'=>trans('api.somethingwentwrong'),
                'status'=>404,
            ];
        }
        return \Response::json($response,202);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }

    }

    public function update(Request $request)
    {
    //     $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
    //     $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
    
    //     $user = \App\User::where('jwt_token',$jwt)
    //     ->Where('phone',$request->phone)->Where('email',$request->email)->first();
    //   $validator = Validator::make($request->all(), [
    //          'name' => 'required',
    //          'email' => 'required',
    //          'city_id'=>'required',
    //          'phone'=>'required',
    //          'image'=>'required|mimes:jpeg,jpg,bmp,png',
    //      ]);
    
    //      }
    //      if($user)
    //         {
    //           return $response=[
    //             'success'=>401,
    //             'message'=>trans('api.submitted'),
    //         ]; 
    //         }
    //     $user->name = $request->name;
    //     $user->email=$request->email;
    //     $user->city_id=$request->city_id;
    //     $user->phone=$request->phone;
    //     $user->image=$request->image;
    //     $user->save();
    //     if($user->save())
    //       {
    //         $response=[
    //             'message'=>trans('api.profiledatachanged'),
    //             'status'=>202,
    //         ];
    //     }
    //     return \Response::json($response,202);
    //     /*if ($user){
    //     $userw=User::where('phone',$request->phone)->orWhere('email',$request->email)
    //         ->first();
    //         if($userw)
    //         {
    //           return $response=[
    //             'success'=>401,
    //             'message'=>trans('api.submitted'),
    //         ]; 
    //         }
    //     }*/
            
    //     if (!$request->headers->has('jwt')){
    //         return response(401, 'check_jwt');
    //     }elseif (!$request->headers->has('lang')){
    //         return response(401, 'check_lang');
    //     }
  
  
  
  
  
    //   $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
    
    //     if($jwt != ""){
            
    //         $userData = \App\User::where('jwt_token',$jwt)
    //         ->first();
    //         if($userData){
         
    //         if($userData){
            
    //             if($request->name){
    //                 DB::table('users')
    //                     ->where('jwt_token', $jwt)
    //                     ->update([
    //                         'name'       => $request->name,
    //                     ]);
              
    //             }
              
    //             if($request->email){
    //                 DB::table('users')
    //                     ->where('jwt_token', $jwt)
    //                     ->update([
    //                         'email'       => $request->email,
    //                     ]);
              
    //             }
    //               if($request->image){
    //                 DB::table('users')
    //                     ->where('jwt_token', $jwt)
    //                     ->update([
    //                         'image'       => $request->image,
    //                     ]);
              
    //             }
    //             if($request->city_id){
    //                 DB::table('users')
    //                     ->where('jwt_token', $jwt)
    //                     ->update([
    //                         'city_id'       => $request->city_id,
    //                     ]);
                 
    //             }
                
    //               if($request->phone){
    //                 DB::table('users')
    //                     ->where('jwt_token', $jwt)
    //                     ->update([
    //                         'phone'       => $request->phone,
    //                     ]);
                 
    //             }
                
    //             //return data
    //               return response()->json([
    //                 'status' => 202,
    //                 'message' => "data changed successfully",
    //                  'data'=>User::where("jwt_token",$jwt)
    //             ->first(),
    //             ]);
               
    //         }else{
    //             return response()->json([
    //                 'status' => 304,
    //                 'message' => "Error! user not found",
    //             ]);
    //         }
    //     }else{
    //         return response()->json([
    //             'status' => 401,
    //             'message' => "Error! in jwt ",
    //         ]);
    //     }
    //         }else{
    //             return "hhhhhhhhhhhhhhhhh";
    //         }
    
    
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->first();
        $validator = Validator::make($request->all(), [
            'name' => 'required_if:name,n',
            'email' => 'required_if:email,e',
            'phone'=>'required_if:phone,p',
            'image'=>'required_if:image,i|mimes:jpeg,jpg,bmp,png',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $user->name = $request->name;
        $user->email=$request->email;
        $user->phone=$request->phone;
        $user->image=$request->image;
        if($user->save())
        {
            $data['id'] = $user['id'];
            $data['name'] = $user['name'];
            $data['email'] = $user['email'];
            $data['phone'] = $user['phone'];
            $data['image'] = $user['image'];
            $response=[
                'message'=>'profile changed successfully',
                'status'=>200,
                'data'=> $data
            ];

        }
        return \Response::json($response,200);
        if ($user){
            $user=User::where('phone',$request->phone)->orWhere('email',$request->email)
                ->orWhere('name',$request->name)->orWhere('city_id',$request->city_id)->exists();
            return $response=[
                'success'=>401,
                'message'=>'submitted before',
            ];
        }

        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }

            
    }
    

    public function updatePassword(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = \App\User::where('jwt_token',$jwt)->first();

        $validator = Validator::make($request->all(), [
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }


        $user->password = $request->password;
        if($user->save()){
              $response=[
            'message'=>trans('api.passwordchangedsuccess'),
            'status'=>202,
        ];
        return \Response::json($response,202);
        }
       
        return \Response::json($response,202);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    

    }


}
