<?php

namespace App\Http\Controllers\API;

use App\OrderDetail;
use App\Reservation;
use App\WasteContainer;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Illuminate\Support\Facades\Validator;
use App\TakeDownContainer;
use App\CompanySeller;
use App\PushNotification;
use niklasravnsborg\LaravelPdf\Facades\Pdf;


class ReservationController extends BaseController
{
    public function indexBooking(Request $request)
    {

        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;

        $language = $request->header('lang');
        if($language=="en"){
            $language = "E";
        }else {
            $language = "A";
        }
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = \App\User::where('jwt_token', $jwt)->first();
        $reservation = Reservation::where('user_id', $user->id)->where('status', 0)->orderBy('id','desc')->get();
        $reservationId=Reservation::find($request->id);

        $res_item = [];
        $res_list  = [];
        foreach ($reservation as $res) {
            $res_item['reservation_id'] = $res->id;
            $res_item['user_id'] = $res->user_id;
            $res_item['container_id'] = $res->container_id;
            $res_item['container_details'] = WasteContainer::where('id',$res->container_id)->select('id','image','name_'.$language.' as name','description_'.$language.' as description','price','distance')->first();
            $res_item['company_details'] = \App\CompanySeller::where('id',$res->company_id)->select('company_name_'.$language.' as name','icon')->first();
            $res_item['takedown_container'] = TakeDownContainer::where('id',$res->container_id)->select('photo')->first();
            $res_item['date_from'] = $res->date_from;
            $res_item['date_to'] = $res->date_to;
            $res_item['days'] = $res->days;
            $res_item['order_number'] = $res->order_number;
            $res_item['total'] = $res->total;
            $res_list[] = $res_item;

        }

        $response = [
            'message' => trans('api.reservation'),
            'status' => 202,
            'data' => $res_list,
        ];
        return \Response::json($response, 202);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }
    }

    public function indexHistory(Request $request)
    {

    $language = $request->header('lang');
        if($language=="en"){
             $language = "E";
        }else {
            $language = "A";
        }
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = \App\User::where('jwt_token', $jwt)->first();
        $reservation = Reservation::where('user_id', $user->id)->where('status', 1)->orderBy('id','desc')->get();
        $reservationId=Reservation::find($request->id);

        $res_item = [];
        $res_list  = [];
        foreach ($reservation as $res) {
            $res_item['reservation_id'] = $res->id;
            $res_item['user_id'] = $res->user_id;
            $res_item['container_id'] = $res->container_id;
            $res_item['container_details'] = WasteContainer::where('id',$res->container_id)->select('id','image','name_'.$language.' as name','description_'.$language.' as description','price','distance')->first();
             $res_item['company_details'] = \App\CompanySeller::where('id',$res->company_id)->select('company_name_'.$language.' as name','icon')->first();
             $res_item['takedown_container'] = TakeDownContainer::where('id',$res->container_id)->select('photo')->first();
            $res_item['date_from'] = $res->date_from;
            $res_item['date_to'] = $res->date_to;
            $res_item['days'] = $res->days;
            $res_item['order_number'] = $res->order_number;
            $res_item['total'] = $res->total;
            $res_list[] = $res_item;

    }

        $response = [
            'message' => 'get data of User Reservations successfully',
            'status' => 202,
            'data' => $res_list,
        ];
        return \Response::json($response, 202);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }
    }



      public function reserve(Request $request)
    {

              $language = $request->header('lang');
        if($language=="en"){
             $language = "E";
        }else {
            $language = "A";
        }
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = \App\User::where('jwt_token',$jwt)->first();
        $validator = Validator::make($request->all(), [
            'date_from' => 'required',
            'days' => 'required',
            'baskets'=>'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $price = WasteContainer::where('id',$request->container_id)->select('price')->first();
        $idCompany = CompanySeller::where('id',$request->company_id)->select('company_name_'.$language.' as company_name')->first();
        $order_number = rand(11111, 99999);
        $date_from =$request->date_from;
        $days=$request->days;
        $baskets=$request->baskets;
         $date_to=date('d-m-Y', strtotime($date_from. ' + '.$days.' days'));
        $total=$days*$price['price'];

         $reservation = Reservation::where('user_id',$user->id)->select
        ('id')->where('status',0)->get();


        if(count($reservation->all())==0)
        {

         $reservationResult =   Reservation::create(array_merge($request->all(),[
            'order_number' => $order_number,
            'date_to'=>$date_to,
            'container_id'=>$request->container_id,
            'company_id'=>$request->company_id,
            'total'=>$total,
            'status'=>'0',
        ]));


        $orderDetails=New OrderDetail();
        $orderDetails->order_id=$reservationResult->id;
        $orderDetails->container_id=$request->container_id;
        $orderDetails->company_id=$request->company_id;
        $orderDetails->user_id=$request->user_id;
        $orderDetails->save();
        $response=[
            'message'=>trans('api.reservation'),
            'status'=>'202',
        ];
        }else{
        $orderDetails=New OrderDetail();
        $orderDetails->order_id=$reservation->all()[0]->id;
        $orderDetails->container_id=$request->container_id;
        $orderDetails->company_id=$request->company_id;
        $orderDetails->user_id=$request->user_id;
        $orderDetails->save();
        $response=[
            'message'=>trans('api.reservation'),
            'status'=>202,
        ];
        }

        return \Response::json($response,202);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }

    public function editBooking(Request $request)
    {


                   $language = $request->header('lang');
        if($language=="en"){
             $language = "E";
        }else {
            $language = "A";
        }
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = \App\User::where('jwt_token', $jwt)->first();
        $reservation = Reservation::where('user_id', $user->id)->where('status', 0)->get();
        $reservationId=Reservation::find($request->id);

        $res_item = [];
        $res_list  = [];
        foreach ($reservation as $res) {
            $res_item['reservation_id'] = $res->id;
            $res_item['user_id'] = $res->user_id;
            $res_item['container_id'] = $res->container_id;
            $res_item['container_details'] = WasteContainer::where('id',$res->container_id)->select('image','name_'.$language.' as name','description_'.$language.' as description','price','distance')->first();
             $res_item['company_details'] = \App\CompanySeller::where('id',$res->company_id)->select('company_name_'.$language.' as name')->first();
             $res_item['takedown_container'] = TakeDownContainer::where('id',$res->container_id)->select('photo')->first();
            $res_item['date_from'] = $res->date_from;
            $res_item['date_to'] = $res->date_to;
            $res_item['days'] = $res->days;
            $res_item['total'] = $res->total;
            $res_list= $res_item;

    }

        $response = [
            'message' => trans('api.reservation'),
            'status' => 202,
            'data' => $res_list,
        ];
        return \Response::json($response, 202);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }
    }

    public function destroy(Request $request){

        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;

        $user = \App\User::where('jwt_token',$jwt)->first();


        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;

        $user = \App\User::where('jwt_token',$jwt)->first();
        // return  $user['id'];
        $validator = Validator::make($request->all(), [
            'is_empty' => 'required',
        ]);
        if ($validator->fails()) {

            $response=[
                'message'=>trans('api.somethingwentwrong'),
                'status'=>'404',
            ];
        }
        $reservation=Reservation::find($request->id);
        $reservation->is_empty =$request->is_empty;
        $reservation->save();
        $response=[
            'message'=>trans('api.empty'),
            'status'=>'202',
        ];
        return \Response::json($response,202);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }

    public function update(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;

        $user = \App\User::where('jwt_token',$jwt)->first();
       // return  $user['id'];
        $validator = Validator::make($request->all(), [
            'date_from' => 'required',
            'days' => 'required',
             'baskets'=>'required',
        ]);
        if ($validator->fails()) {

            $response=[
            'message'=>'there is something wrong',
            'status'=>'404',
        ];
        }
        $price = WasteContainer::where('id',$request->container_id)->select('price')->first();

        $date_from =$request->date_from;
        $days=$request->days;
         $date_to=date('d-m-Y', strtotime($date_from. ' + '.$days.' days'));

        $total=$days*$price['price'];
        $reservation=Reservation::find($request->id);
        $reservation->date_from = $request->date_from;
        $reservation->days=$request->days;
        $reservation->baskets=$request->baskets;
         $reservation->date_to=$date_to;
        $reservation->total=$total;
        $reservation->status='0';
        $reservation->save();
        $response=[
            'message'=>trans('api.reservation'),
            'status'=>'202',
        ];
        return \Response::json($response,202);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }




   public function IndexReservation(Request $request)
    {

        $language = $request->header('lang');
        if($language=="en"){
             $language = "E";
        }else {
            $language = "A";
        }
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = \App\User::where('jwt_token', $jwt)->first();
        $reservation = Reservation::where('user_id', $user->id)->where('status', 0)->orderBy('id','desc')->get();
//        $reservationId=Reservation::find($request->id);

        $res_item = [];
        $res_list  = [];
        foreach ($reservation as $res) {
            $res_item['reservation_id'] = $res->id;
            $res_item['user_id'] = $res->user_id;
            $res_item['container_id'] = $res->container_id;
            $res_item['container_details'] = WasteContainer::where('id',$res->container_id)->select('id','image','name_'.$language.' as name','description_'.$language.' as description','price','distance')->first();
             $res_item['company_details'] = \App\CompanySeller::where('id',$res->company_id)->select('company_name_'.$language.' as name')->first();
             $res_item['takedown_container'] = TakeDownContainer::where('id',$res->container_id)->select('photo')->first();
            $res_item['date_from'] = $res->date_from;
            $res_item['date_to'] = $res->date_to;
            $res_item['days'] = $res->days;
            $res_item['total'] = $res->total;
            $res_list[] = $res_item;

    }

        $response = [
            'message' => 'get data of User Reservations successfully',
            'status' => 202,
            'data' => $res_list,
        ];
        return \Response::json($response, 202);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }
    }




        //update reserve
        public function confirmReservation(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = \App\User::where('jwt_token',$jwt)->first();
         $token = \App\User::select('firebase_token')->where('firebase_token',$jwt)->pluck('firebase_token')->toArray();
         PushNotification::send($token,'done',1);
       // return  $user['id'];
        $validator = Validator::make($request->all(), [
            'date_from' => 'required',
            'days' => 'required',
            'payment_info'=>'required'
        ]);
        if ($validator->fails()) {
            $response=[
            'message'=>'failed to booking',
            'status'=>404,
        ];
        }
        $price = WasteContainer::where('id',$request->container_id)->select('price')->first();

         for($i=0;$i<count($request->id);$i++)
         {

        $reservation=Reservation::find($request->id[$i]);

        $reservation->payment_info=$request->payment_info;
        $reservation->status='1';
        $reservation->save();
         }

        $response=[
            'message'=>trans('api.reservation'),
            'status'=>202,
        ];
        return \Response::json($response,202);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }


    public function generate_reservation_pdf(Request $request) {
        $reservation = Reservation::where('id',$request->id)->first();
        $pdf = Pdf::loadView('dashboard.views.invoices.reservation_invoice',compact('reservation'));
        $data= json_decode( json_encode($pdf), true);
        return  $pdf->download('reservation_invoice-'.$data.$reservation->order_number.'.pdf');
    }


    public function delete(Request $request)
    {
         $reservation = Reservation::where('id',$request->id)->where('status','0')->first();
                if (\App\Reservation::destroy($request->id)){

            $response=[
                'message'=>trans('api.Order successfully deleted'),
                'status'=>202,
            ];
            return \Response::json($response,200);
        }else{
            $response=[
                'message'=>trans('api.somethingwentwrong'),
                'status'=>404,
            ];
            return \Response::json($response,404);
        }
    }

 }







