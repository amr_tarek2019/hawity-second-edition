<?php

namespace App\Http\Controllers\API;

use App\Notification;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\User;
use Illuminate\Support\Facades\Validator;


class NotificationsController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = \App\User::where('jwt_token',$jwt)->first();

        $notifications = Notification::where('user_id',$user->id)->select
        ('id','icon','notification','created_at')->get();
        if (!$notifications)
        {
            return \Response::json('notifications not found',404);
        }else{
            $response=[
                'message'=>'get data of waste container successfully',
                'status'=>202,
                'data'=>$notifications,
            ];
            return \Response::json($response,202);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateToken(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = \App\User::where('jwt_token',$jwt)->first();
        $validator = Validator::make($request->all(), [
            'firebase_token' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $user->firebase_token = $request->firebase_token;
        if($user->save()){
               $response=[
            'message'=>'firebase token changed successfully',
            'status'=>202,
        ];
        return \Response::json($response,202);
        }
       
       
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
