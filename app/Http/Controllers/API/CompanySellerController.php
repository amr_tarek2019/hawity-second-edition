<?php

namespace App\Http\Controllers\API;

use App\Category;
use App\CompanySeller;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;


class CompanySellerController extends BaseController
{
    public function CompanySellerRegister(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'company_name_E' => 'required',
            'company_name_A' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'commercial_register' => 'required',
            'tax_record' => 'required',
            'category_id'=>'required',

        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $companySeller = \App\CompanySeller::create(array_merge($request->all(),[
            'icon'=>'default.png',
            'text_E'=>'default',
            'text_A'=>'default',
            'longitude'=>'0',
            'latitude'=>'0',
            'status' => false,
        ]));
        // return $this->sendResponse($companySeller, 'Company register successfully.');
               $response=[
            'status'=>202,
            'data'=>$companySeller,
            'message'=>trans('api.register'),

        ];
        return \Response::json($response,202);
    }

    public function index(Request $request)
    {

        $language = $request->header('lang');
        if($language=="en"){
             $language = "E";
        }else {
            $language = "A";
        }
            $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = \App\User::where('jwt_token',$jwt)->first();
        $validator = Validator::make($request->all(), [
            'longitude' => '',
            'latitude' => '',

        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $user->longitude = $request->longitude;
        $user->latitude=$request->latitude;
        
         $companySellers=CompanySeller::select('id','icon','longitude','latitude','company_name_'. $language.' as company_name', 'category_id','text_'. $language . ' as text','status'
                ,DB::raw("3959 * round(cos(radians(" . $user->latitude . ")) 
        * cos(radians(latitude)) 
        * cos(radians(longitude) - radians(" .  $user->longitude . ")) 
        + sin(radians(" .$user->latitude. ")) 
        * sin(radians(latitude))) AS distance",2))->orderBy('distance', 'asc')->take(20)->where('category_id',$request->category_id)->where('status','1')->get();
        
        $response=[
            'message'=>'get data of company successfully',
            'status'=>202,
            'data'=>$companySellers,
        ];
        return \Response::json($response,202);
    }
    
     
    public function location(Request $request)
    {
       
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;

     
        $validator = Validator::make($request->all(), [
            'longitude'=>'required',
            'latitude'=>'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $companyLocation = CompanySeller::find($request->id);
        $companyLocation->longitude = $request->longitude;
        $companyLocation->latitude = $request->latitude;
        $companyLocation->save();

            $response = [
                'message' => 'Location inserted successfully',
                'status' => 202,
            ];
        // }
        return \Response::json($response, 202);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }
    }
}
