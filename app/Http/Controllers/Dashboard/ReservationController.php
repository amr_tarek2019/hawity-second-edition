<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Reservation;
use App\WasteContainer;
use App\CompanySeller;
use niklasravnsborg\LaravelPdf\Facades\Pdf;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $reservations = Reservation::where('status',0)->get();
        return view('dashboard.views.reservation.index',compact('reservations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $reservation = Reservation::find($id);
        return view('dashboard.views.reservation.show',compact('reservation'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $containers=WasteContainer::all();
        $companies=CompanySeller::all();
        $reservation = Reservation::find($id);
        return view('dashboard.views.reservation.edit',compact('reservation','companies','containers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    $this->validate($request, [
        'container' => 'required',
        'company' => 'required',
        'date_from'=>'required',
        'baskets'=>'required',
    ]);


        $reservation = Reservation::find($id);

        $reservation->container_id = $request->container;
     $reservation->company_id = $request->company;
     $reservation->date_from = $request->date_from;
 
 $reservation->baskets = $request->baskets;
          $reservation->save();

    return redirect()->route('reservation.index')->with('successMsg','Rservation Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reservation= Reservation::find($id);
        $reservation->delete();
        return redirect()->route('reservation.index')->with('successMsg','Rservation Successfully Deleted');

    }

    function generate_reservation_pdf($id) {
        $reservation = Reservation::find($id);
        ini_set('memory_limit', '-1');  //increase size as you need
        $pdf = PDF::loadView('dashboard.views.invoices.reservation_invoice',compact('reservation'));
        return $pdf->download('reservation_invoice.pdf');
    }
    
    
    public function getComanyContainers($company_id)
    {
        // return $company_id;
        $company_containers = WasteContainer::where('company_id',$company_id)->pluck('company_id','name_E');
        return response()->json($company_containers);
    }
    
    
        public function whatsShow($id)
    {
         $reservation = Reservation::find($id);
        return view('dashboard.views.whats.whats',compact('reservation'));
    }
}
