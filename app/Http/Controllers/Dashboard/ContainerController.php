<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\WasteContainer;
use App\CompanySeller;


class ContainerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          $containers=WasteContainer::all();
        return view('dashboard.views.container.index',compact('containers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies=CompanySeller::all();
        return view('dashboard.views.container.create',compact('companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
    {
        $this->validate($request,[
            'name_A'=>'required',
            'name_E'=>'required',
            'price' => 'required',
            'distance' => 'required',
            'company' => 'required',
            'image' => 'required|mimes:jpeg,jpg,bmp,png',
            'description_E'=>'required',
            'description_A'=>'required',
        ]);
        $container = new WasteContainer();
        $container->name_A=$request->name_A;
        $container->name_E=$request->name_E;
        $container->price=$request->price;
        $container->distance=$request->distance;
        $container->company_id = $request->company;
        if ( $request->hasFile('image')) {
            $image = $request->image;
            $image_new_name = time().uniqid().'.'.$image->getClientOriginalName();
            $image->move('uploads/container/',$image_new_name);
            $container->image = 'uploads/container/'.$image_new_name;
        }
        $container->description_E = $request->description_E;
        $container->description_A = $request->description_A;
        if ($container->save())
        {
            return redirect()->route('container.index')->with('successMsg','Container Successfully Saved');
        }
        flash('sorry something went wrong');
        return redirect()->route('container.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     $companies=CompanySeller::all();
        $container = WasteContainer::find($id);
        return view('dashboard.views.container.edit',compact('companies','container'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $container = WasteContainer::find($id);
        $container->name_A=$request->name_A;
        $container->name_E=$request->name_E;
        $container->price=$request->price;
        $container->distance=$request->distance;
        $container->company_id = $request->company;
        if ( $request->hasFile('image')) {
            $image = $request->image;
            $image_new_name = time().uniqid().'.'.$image->getClientOriginalName();
            $image->move('uploads/container/',$image_new_name);
            $container->image = 'uploads/container/'.$image_new_name;
        }
        $container->description_E = $request->description_E;
        $container->description_A = $request->description_A;
        if ($container->save())
        {
            return redirect()->route('container.index')->with('successMsg','Container Successfully Updated');
        }
        flash('sorry something went wrong');
        return redirect()->route('container.create');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $container = WasteContainer::find($id);
        $container->delete();
        return redirect()->back()->with('successMsg','Container Successfully Delete');
    }
}
