<?php

namespace App\Http\Controllers\Dashboard;

use App\City;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserIndividualController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.views.user.individual.index')->with('users',User::where('user_type','individual')->get());
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.views.user.individual.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required',
            'password'=>'required',
            'user_status'=>'required',
            'status'=>'required',
        ]);
        $user=User::where('email',$request->email)->orWhere('password',$request->password)->exists();
        if ($user){
            return redirect()->route('user.individual.index')->with('successMsg','User Created Before');
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' =>  bcrypt('password'),
            'phone'=>'0',
            'commercial_register'=>'0',
            'tax_record'=>'0',
            'user_type'=>'company',
            'user_status'=>$request->user_status,
            'status'=>$request->status,
            'verify_code'=>'0',
            'jwt_token'=>'0',
            'latitude'=>'0',
            'longitude'=>'0',
        ]);

        $user->save();
        return redirect()->route('user.individual.index')->with('successMsg','User Successfully Created');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cities = City::all();

        $user = User::find($id);
        return view('dashboard.views.user.individual.show',compact('cities','user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
//        $user = User::find($id);
//        return view('dashboard.views.user.company.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
//    public function update(Request $request, $id)
//    {
//        $user = User::find($id);
//        $user->name = $request->name;
//        $user->email = $request->email;
//        $user->password = bcrypt($request->password);
//        $user->user_status=$request->user_status;
//        $user->status=$request->status;
//        $user->save();
//        return redirect()->route('user.company.index')->with('successMsg','User Successfully Updated');
//    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect()->back()->with('successMsg','User Successfully Delete');

    }
}
