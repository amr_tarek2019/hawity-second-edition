<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Privacy;

class PrivacyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
  
         public function index()
    {
        return view('dashboard.views.privacy.index')->with('setting', Privacy::first());
    }
   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
       $this->validate($request,[
            "text_A"    => "required",
            "text_E"    => "required",
        ]);
        $setting = Privacy::first();
        $setting->text_A =  $request->text_A;
        $setting->text_E =  $request->text_E;
        if ( $request->hasFile('icon')  ) {
            $image = $request->icon;
            $image_new_name = time().uniqid().'.'.$image->getClientOriginalName();
            $image->move('uploads/privacy/',$image_new_name);
            $setting->icon = 'uploads/privacy/'.$image_new_name;
            $setting->save();
        }
        $setting->save();

        return redirect()->route('privacy.index')->with('successMsg','Privacy Updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
