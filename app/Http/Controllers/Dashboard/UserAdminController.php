<?php

namespace App\Http\Controllers\Dashboard;

use App\Admin;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class UserAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        return view('dashboard.views.user.admin.index')->with('users',User::where('user_type','admin')->get());
        $users=Admin::where('id','!=' , Auth::guard('admin')->user()->id)->with('roles')->get();
        return view('dashboard.views.user.admin.index',compact('users'));

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $allroles=Role::where('id' , '!=' , 1)->get();
        return view('dashboard.views.user.admin.create',compact('allroles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required',
            'password'=>'required',
            'image'=>'required',
        ]);

        $user = Admin::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
            'image'=>$request->image
        ]);
        $user->assignRole($request->role);
        if ( $user->save())
        {
            return redirect()->route('user.admin.index')->with('successMsg','User Successfully Created');
        }
        return redirect()->route('user.admin.index')->with('successMsg','sorry something went wrong');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Admin::find($id);
        $allroles=Role::where('id' , '!=' , 1)->get();
        $admintoedit['roles']=DB::table('model_has_roles')->
        where('model_id',$id)->where('model_type','App\Admin')->select('role_id')->pluck('role_id');
        return view('dashboard.views.user.admin.edit',compact('allroles','admintoedit','user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Admin::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->image = $request->image;
        if($request->roles) {
            $user->roles()->sync($request->role);
        }
        $user->save();
        return redirect()->route('user.admin.index')->with('successMsg','User Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Admin::find($id);
        $user->delete();
        return redirect()->back()->with('successMsg','User Successfully Delete');

    }
}
