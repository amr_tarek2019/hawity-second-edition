<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests\NotificationRequest;
use App\Notification;
use App\Notifications;
use App\PushNotification;
use App\Reservation;
use App\User;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class NotificationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $notifies= Notification::orderBy('created_at','desc')->get();

         return view('dashboard.views.notification.index',compact('notifies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.views.notification.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function notify(Request $request)
    {

             $users= User::where('user_status','1')->where('user_type',$request->user_type)
                 ->select('id','firebase_token')->get();

        foreach ($users as $user) {
                $notification_table = Notification::create([
                'user_id' => $user->id,
                'title' => $request->title,
                'notification' => $request->notification,
                'user_type' => $request->user_type,
                ]);
        }

            PushNotification::send_details($users, $request->title,$request->notification,$notification_table->notification,1);

            $notification_table->save();
          return redirect()->route('notification.index')->with('successMsg','Notification Successfully Sended');

        }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $notify=PushNotification::find($id);
        return view('dashboard.views.notification.edit',compact('notify'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NotificationRequest $request, PushNotification $notify)
    {
//        $data=$request->validated();
//        $notify->update($data);
//        if(isset($request->notifications_img))
//        {
//            $notify->notifications_img=$data['notifications_img'];
//            $notify->save();
//
//
//        }
//        return redirect()->route('notification.index')->with('success','Notification Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $notify= Notification::find($id);
        $notify->delete();
        return redirect()->route('notification.index')->with('success','Notification Deleted Successfully');
    }
}
