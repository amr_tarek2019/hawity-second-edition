<?php

namespace App\Http\Controllers\Dashboard;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return view('dashboard.views.category.index',compact('categories'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.views.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name_E' => 'required',
            'name_A' => 'required',
            'image' => 'required|mimes:jpeg,jpg,bmp,png',
        ]);
        $category = new Category();
        $category->name_E = $request->name_E;
        $category->name_A = $request->name_A;
        if ( $request->hasFile('image')  ) {
            $image = $request->image;
            $image_new_name = time().uniqid().'.'.$image->getClientOriginalName();
            $image->move('uploads/category/',$image_new_name);
            $category->image = 'uploads/category/'.$image_new_name;
            $category->save();
        }
        $category->save();
        return redirect()->route('category.index')->with('successMsg','Category Successfully Saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        return view('dashboard.views.category.edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $category = Category::find($id);
        $category->name_E = $request->name_E;
        $category->name_A = $request->name_A;
        if ( $request->hasFile('image')  ) {
            $image = $request->image;
            $image_new_name = time().uniqid().'.'.$image->getClientOriginalName();
            $image->move('uploads/category/',$image_new_name);
            $category->image = 'uploads/category/'.$image_new_name;
            $category->save();
        }
        $category->save();
        return redirect()->route('category.index')->with('successMsg','Category Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $caregory=Category::find($id);
        $caregory->delete();
        return redirect()->back()->with('successMsg','Category Successfully Delete');
    }
}
