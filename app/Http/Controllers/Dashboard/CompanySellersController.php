<?php

namespace App\Http\Controllers\Dashboard;

use App\Category;
use App\CompanySeller;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CompanySellersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companySellers=CompanySeller::all();
        return view('dashboard.views.company_seller.index',compact('companySellers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories=Category::all();
        return view('dashboard.views.company_seller.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'company_name_E' => 'required',
            'company_name_A' => 'required',
            'icon' => 'required|mimes:jpeg,jpg,bmp,png',
            'category' => 'required',
            'text_E'=>'required',
            'text_A'=>'required',
            'email'=>'required',
            'phone'=>'required',
            'address'=>'required',
            'longitude'=>'required',
            'latitude'=>'required',
            'commercial_register'=>'required',
            'tax_record'=>'required'
        ]);
        $companySeller = new CompanySeller();
        $companySeller->name=$request->name;
        $companySeller->company_name_E = $request->company_name_E;
        $companySeller->company_name_A = $request->company_name_A;
        if ( $request->hasFile('icon')) {
            $image = $request->icon;
            $image_new_name = time().uniqid().'.'.$image->getClientOriginalName();
            $image->move('uploads/company/',$image_new_name);
            $companySeller->icon = 'uploads/company/'.$image_new_name;
        }
        $companySeller->category_id = $request->category;
        $companySeller->text_E = $request->text_E;
        $companySeller->text_A = $request->text_A;
        $companySeller->email = $request->email;
        $companySeller->phone = $request->phone;
        $companySeller->address = $request->address;
        $companySeller->longitude = $request->longitude;
        $companySeller->latitude = $request->latitude;
        $companySeller->commercial_register = $request->commercial_register;
        $companySeller->tax_record = $request->tax_record;
        if ($companySeller->save())
        {
            return redirect()->route('company.index')->with('successMsg','Company Successfully Saved');
        }
        flash('sorry something went wrong');
        return redirect()->route('company.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $companySeller=CompanySeller::find($id);
        $categories=Category::all();
        return view('dashboard.views.company_seller.edit',compact('companySeller','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $companySeller=CompanySeller::find($id);
        $companySeller->name=$request->name;
        $companySeller->company_name_E = $request->company_name_E;
        $companySeller->company_name_A = $request->company_name_A;
        if ( $request->hasFile('icon')) {
            $image = $request->icon;
            $image_new_name = time().uniqid().'.'.$image->getClientOriginalName();
            $image->move('uploads/company/',$image_new_name);
            $companySeller->icon = 'uploads/company/'.$image_new_name;
        }
        $companySeller->category_id = $request->category;
        $companySeller->text_E = $request->text_E;
        $companySeller->text_A = $request->text_A;
        $companySeller->email = $request->email;
        $companySeller->phone = $request->phone;
        $companySeller->address = $request->address;
        $companySeller->longitude = $request->longitude;
        $companySeller->latitude = $request->latitude;
        $companySeller->commercial_register = $request->commercial_register;
        $companySeller->tax_record = $request->tax_record;
        if ($companySeller->save())
        {
            return redirect()->route('company.index')->with('successMsg','Company Successfully Updated');
        }
        flash('sorry something went wrong');
        return redirect()->route('company.create');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $companySeller=CompanySeller::find($id);
        $companySeller->delete();
        return redirect()->back()->with('successMsg','Company Successfully Delete');
    }

    public function updateStatus(Request $request)
    {
        $company = CompanySeller::findOrFail($request->id);
        $company->status = $request->status;
        if($company->save()){
            return 1;
        }
        return 0;
    }
}
