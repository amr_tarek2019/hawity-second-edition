<?php

namespace App\Http\Controllers\Authentication;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('auth.login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function adminLogin(Request $request)
    {
//        $user=User::whereIn('user_type',['admin','member'])->where('email',$request->email)->first();
//        if ($user!=null)
//        {
//            if (Hash::check($request->password,$user->password)){
//                if ($request->has('remember')){
//                    auth()->login($user,true);
//                }
//                else{
//                    auth()->login($user,false);
//                }
//                return redirect()->route('dashboard');
//            }
//        }
//        return back();
        $credentials = array(
            'email' => $request->email,
            'password' =>$request->password
        );

        if (Auth::guard('admin')->attempt($credentials)) {

            return redirect()->route('dashboard');

        }
        else{

            return redirect()->route('login')->with('error','البريد الاكتروني أو كلمة السر غير صحيحة');

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
