<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class Notification extends Model
{
    protected $fillable=['user_id', 'icon', 'notification','title','created_at'];
    protected $table='notifications';
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getCreatedAtAttribute($value)
    {
       return Carbon::parse($value)->toDateString();
    }
}
