<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $fillable=['order_id', 'container_id', 'user_id','company_id'];
    protected $table='order_details';
     public function reservation()
    {
        return $this->belongsTo('App\Reservation','order_id');
    }
   public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }


    public function wasteContainer()
    {
        return $this->belongsTo('App\WasteContainer','container_id');
    }
    
    public function companySeller()
    {
        return $this->belongsTo('App\CompanySeller','company_id');
    }
}
