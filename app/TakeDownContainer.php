<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TakeDownContainer extends Model
{
    protected $fillable=[ 'photo', 'container_id', 'note_E','note_A','user_id'];
    protected $table='take_down_containers';

    public function WasteContainer()
    {
        return $this->belongsTo('App\WasteContainer');
    }

    public function setPhotoAttribute($value)
    {
        if ($value)
        {
            $imageName=time().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('uploads/take_down_container/photos/'),$imageName);
           $this->attributes['photo']=$imageName;
        }
    }
}
