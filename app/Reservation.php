<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $fillable=['container_id','company_id','order_number', 'date_from',
        'date_to', 'total', 'status', 'payment_info', 'days','user_id','is_empty','baskets'];
    protected $table='reservations';
    
    
 public function companySeller()
    {
        return $this->belongsTo('App\CompanySeller','company_id');
    }



    public function OrderDetail()
    {
        return $this->hasMany('App\OrderDetail');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function wasteContainer()
    {
        return $this->belongsTo('App\WasteContainer','container_id');
    }
    
}
