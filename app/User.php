<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','phone', 
        'user_type', 'commercial_register', 'tax_record', 'city_id', 'verify_code',
        'user_status','status','jwt_token',
        'password','longitude','latitude','image','firebase_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token','created_at','updated_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setPasswordAttribute($password)
    {
        return $this->attributes['password'] = bcrypt($password);
    }
    
    public function setPhoneAttribute($phone)
    {
        return $this->attributes['phone'] = ltrim($phone, '0');
    }

    public function notifications()
    {
        return $this->hasMany('App\Notification');
    }
    public function city()
    {
        return $this->belongsTo('App\City');
    }
    
     public function suggestions()
    {
        return $this->hasMany('App\Suggestion');
    }
    
       public function setImageAttribute($value)
    {
        if ($value)
        {
            $imageName=time().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('uploads/user/profile/'),$imageName);
           $this->attributes['image']=$imageName;
        }
    }
    
       
      public function getImageAttribute($value)
    {
        if ($value) {
            return asset('uploads/user/profile/'.$value);
        } else {
            return asset('uploads/container/default.png');
        }
    }
    
       public function orders()
    {
        return $this->hasMany('App\OrderDetail');
    }

}
