<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AboutUs extends Model
{
    protected $table='about_us';
    protected $fillable=['icons', 'about_E','about_A'];
    
     public function getIconsAttribute($value)
    {
        if ($value) {
            return asset($value);
        } else {
            return asset('uploads/about/default.png');
        }
    }
}
